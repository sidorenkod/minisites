var hover_elem = true;

var edit_id = '';

var change_percent = false;

var style = {borderClass:'border-hover'};



/** ajax json update data */
function jsonFunc(data)
{
  var url = '/translator/save-post?temp_id='+$('#temp_id').val();
  $.post(url, data, function(json){console.log(json)},  'json');
}

/** remove span delete */
function delRemove()
{
  if(edit_id != $('.edit_block').attr('id'))
  {
    $('.delete').remove();
  }
  
}

function setPosition(e)
{
  $('#modal_block').css({'left':e.pageX,'top':e.pageY})
}

/** oopen/close colors block */
$('.open_close').click(function(){
  if($('.edit_template').hasClass('not_show'))
  {
    $('.edit_template').removeClass('not_show');
    $('.open_bl').addClass('hide');
    $('.close_bl').removeClass('hide');
  }
  else
  {
    $('.edit_template').addClass('not_show');
    $('.open_bl').removeClass('hide');
    $('.close_bl').addClass('hide');
  }
  
})
  
//style.borderClass = 'border-hover';

$('.edit_soc').click(function(e){
  
  edit_id = $(this).data('id');
  
  setPosition(e);
  
  $(this).addClass(style.borderClass);
  
  if(edit_id != '')
  {
    $('#'+edit_id).removeClass(style.borderClass);
  }
  
  console.log(edit_id);
  
  //edit_id = $(this).attr('id');
  
  delRemove();
  
  $('#soc_href').val($(this).attr('href'));
  
  $('#modal_block .blockedit').addClass('hide');
  $('#modal_block .edit_soc_block').removeClass('hide');
  $('#modal_block').removeClass('hide');

});

/** hover elements */
$('.edit_text, .edit_block').hover(function(event){
    
    $(this).addClass(style.borderClass); 
  
  },
  function(event){
    
  if($(this).attr('id')!=edit_id)
  {
    $(this).removeClass(style.borderClass);
  }
          
});

/** edit block */
$('.edit_block').click(function(){

  if(event.target.id==$(this).attr('id'))
  {
    $(this).addClass(style.borderClass);
  
    $('#close_form').click();
    
    edit_id = $(this).attr('id');
    
    delRemove();
    
    //var delet = '<span class="delete">X</span>';
    
    $(this).append('<span class="delete" title="delete block">X</span>');
  }
  
  
});

/** delete block */
$(document).on('click','.delete',function(){
  $('a[href=#twitter-carousel].twitter-right-control').click();
  
  $('#'+edit_id).remove();
  console.log('delete - '+edit_id);
  var data = edit_id+'='+0;
  jsonFunc(data);
});

/** edit text */
$('.edit_text').click(function(e){
   
   setPosition(e);
  
  $(this).addClass(style.borderClass);
  
  if(edit_id != '')
  {
    $('#'+edit_id).removeClass(style.borderClass);
  }
  
  edit_id = $(this).attr('id');
  
  delRemove();
  
  $('#m_text').val($(this).html());
  
  $('#modal_block .blockedit').addClass('hide');
  $('#modal_block .text_edit').removeClass('hide');
  $('#modal_block').removeClass('hide');
  
});

/** edit link */
$('.edit_link').click(function(event){
  
  setPosition(event);
  
  $(this).addClass(style.borderClass);
  
  if(edit_id != '')
  {
    $('#'+edit_id).removeClass(style.borderClass);
  }
  
  edit_id = $(this).data('id');
  
  delRemove();
  
  $('#link_text').val($(this).html());
  
  $('#modal_block .blockedit').addClass('hide');
  $('#modal_block .change_link').removeClass('hide');
  $('#modal_block').removeClass('hide');
  return false;
});

/** edit icon */
$('.edit_icon').click(function(event){
  
  setPosition(event);
  
  $(this).addClass(style.borderClass);
  
  if(edit_id != '')
  {
    $('#'+edit_id).removeClass(style.borderClass);
  }
  
  edit_id = $(this).attr('id');
  
  console.log(edit_id);
  
  delRemove();
  
  $('#modal_block .blockedit').addClass('hide');
  $('#modal_block .change_icon').removeClass('hide');
  $('#modal_block').removeClass('hide');
  
  return false;
});

/** edit link */
$('.edit_button').click(function(e){
  
  setPosition(e);
  
  $(this).addClass(style.borderClass);
  
  if(edit_id != '')
  {
    $('#'+edit_id).removeClass(style.borderClass);
  }
  
  edit_id = $(this).attr('id');
  
  delRemove();
  
  $('#button_text').val($(this).html());
  
  $('#modal_block .blockedit').addClass('hide');
  $('#modal_block .edit_bottom_block').removeClass('hide');
  $('#modal_block').removeClass('hide');
  
  return false;
  
});
/** change percent */
$('.change_lang_percent').click(function(){

  edit_id = $(this).attr('id');
  
  delRemove();
  
  change_percent = true;
  var percent = $(this).attr('aria-valuetransitiongoal');
  $('#m_text').val(percent);
  
  $('#modal_block .blockedit').addClass('hide');
  $('#modal_block .text_edit').removeClass('hide');
  $('#modal_block').removeClass('hide');
});

/** change_bg */
$('.change_bg').click(function(event){

    if(event.target.id==$(this).attr('id'))
    {
      edit_id = $(this).attr('id');
      delRemove();
      style.backgroundImage = $(this).css('background-image');
      
      setPosition(event);
      
      $('#modal_block .blockedit').addClass('hide');
      $('#modal_block .edit_bg').removeClass('hide');
      $('#modal_block').removeClass('hide');
      
    }   
});

/** change images */
$('.cnange_img').click(function(event){
  setPosition(event);
  edit_id = $(this).attr('id');
  
  delRemove();
  
  $('#modal_block .blockedit').addClass('hide');
  
  $('#modal_block .form_upload_file').removeClass('hide');
  $('#modal_block').removeClass('hide');
  
  return false;
});

/** submit form - upload file */
$('#form_upload').submit(function(){
  event.stopPropagation();
  event.preventDefault();
  
  var contactForm = $(this);

    formData = new FormData(contactForm.get(0));
  $.ajax({
    url: contactForm.attr('action'),
    type: contactForm.attr('method'),
    contentType: false,
    processData: false,
    data: formData,
    dataType: 'json',
    success: function(json){
      if(json.result == true)
      {
        $('.file_result').text('File added');
        var uimage = '<img src="'+json.file+'" width="100">';
        console.log(uimage);
        $('.users_files').append(uimage);
        $('.myf').click();
      }
      else
      {
        $('.file_result').text('error');
      }
      
    }
  });
});

$('#form_upload_bg').submit(function(){
  event.stopPropagation();
  event.preventDefault();
  
  var contactForm = $(this);

    formData = new FormData(contactForm.get(0));
  $.ajax({
    url: contactForm.attr('action'),
    type: contactForm.attr('method'),
    contentType: false,
    processData: false,
    data: formData,
    dataType: 'json',
    success: function(json){
      if(json.result == true)
      {
        $('.file2_result').text('File added');
        var uimage = '<img src="'+json.file+'" width="100">';
        console.log(uimage);
        $('.u_files').append(uimage);
        $('.myimg').click();
      }
      else
      {
        $('.file2_result').text('error');
      }
      
    }
  });
});

/** install bg for block */
$('.bloc_bg_edit').on('click','img',function(event){

  var href_img = $(this).attr('src');
  //console.log(href_img);
  var bgn = 'url('+href_img+')';
  
  
  
  $('#'+edit_id).css({'background-image':bgn});
  
  var data = edit_id+'=background-image:'+bgn;
  console.log(data);
  jsonFunc(data);
  
});

/** install bg for block */
$('.this_icon').on('click',function(event){

  var icon = $(this).data('icon');
  

  
  
  $('#'+edit_id).removeClass();
  $('#'+edit_id).addClass('fa');
  $('#'+edit_id).addClass('edit_icon');
  $('#'+edit_id).addClass(icon);
  
  var data = edit_id+'='+icon;
  //console.log(data);
  jsonFunc(data);
  
});

/** install images */
$('.users_files, .def_files').on('click','img',function(event){

  var href_img = $(this).attr('src');
  
  $('#'+edit_id).attr('src',href_img);
  
  var data = edit_id+'='+href_img;
  
  jsonFunc(data);
  
});

/** cancel install bg */
$('#m_cancel_bg').click(function(){
  
  $('#'+edit_id).css({'background-image':style.backgroundImage});
  
  var data = edit_id+'='+style.backgroundImage;
  
  jsonFunc(data);
});

$('#change_icon_save').click(function(){
  $('#close_form').click();
});

/** save changed text */
$('#m_save').click(function(){
  if(!change_percent)
  {
    $('#'+edit_id).html($('#m_text').val());
  }
  else
  {
    $('#'+edit_id).html($('#m_text').val()+'%');
    $('#'+edit_id).css({'width':$('#m_text').val()+'%'});
  }
  
  var data = edit_id+'='+$('#m_text').val();
  data += '&'+edit_id+'_style='+$('#'+edit_id).attr('style');
  //console.log(data);
  jsonFunc(data);
  $('#close_form').click();
});

/** */
$('#m_save_bg').click(function(){
  var data = edit_id+'='+$('#'+edit_id).attr('style');
  //jsonFunc(data);
  $('#close_form').click();
});

/** save changed text */
$('#soc_save').click(function(){

  $('#'+edit_id+'_link').attr('href',$('#soc_href').val());
  
  console.log(edit_id);
  
  var data = edit_id+'_link='+$('#soc_href').val();
  //console.log(data);
  jsonFunc(data);
  $('#close_form').click();
});
/** save changed text */
$('#butt_save').click(function(){

  $('#'+edit_id).html($('#button_text').val());
  
  
  var data = edit_id+'='+$('#button_text').val();
  data += '&'+edit_id+'_style='+$('#'+edit_id).attr('style');
  //console.log(data);
  jsonFunc(data);
  $('#close_form').click();
});

$('#link_save').click(function(){

  $('#'+edit_id).html($('#link_text').val());

  var data = edit_id+'='+$('#link_text').val();
  //console.log(data);
  jsonFunc(data);
  $('#close_form').click();
});

$('.delete_link').click(function(){
  $('.'+edit_id+'_block').remove();
  var data = edit_id + '_block=0';
  jsonFunc(data);
  $('#close_form').click();
});
$('#form_upload_file_save').click(function(){
  $('#close_form').click();
});
/** close modal form */
$('#close_form').click(function(){
  
  $('#modal_block').addClass('hide');
  $('#modal_block>div').addClass('hide');
  
  $('#'+edit_id).removeClass(style.borderClass);
  
  hover_elem = true;
  edit_id = '';
});

/** change color template */
$('.colors_edit').click(function(){
  
  var color_id = $(this).attr('id');
  
  var style = document.getElementById('css-preset');
  style.href = '/templates/template_2/css/colors/'+color_id+'.css';
  
  var data = 'css-preset='+color_id;
  
  jsonFunc(data);
});

$('.block_reset span').click(function(){
  $('.quest').removeClass('hide');
});

$('.quest span').click(function(){
  if($(this).hasClass('no'))
  {
    $('.quest').addClass('hide');
  }
  else
  {
    $.post('/translator/reset-post', {temp_id:$('#temp_id').val()}, function(json){ location.reload(true); },  'json');
  }
});

/** add style for text */
$('.add_style').click(function(){
  var $this = $(this);
  
  if($this.data('style')=='al_left')
  {
    if($this.hasClass('active'))
    {
      $('#'+edit_id).css({'text-align':''});
    }
    else
    {
      $('.tal').removeClass('active');
      $('#'+edit_id).css({'text-align':'left'});
    }
    
    $this.toggleClass('active');
  }
  else if($this.data('style')=='al_center')
  {
    if($this.hasClass('active'))
    {
      $('#'+edit_id).css({'text-align':''});
    }
    else
    {
      $('.tal').removeClass('active');
      $('#'+edit_id).css({'text-align':'center'});
    }
    
    $this.toggleClass('active');
  }
  else if($this.data('style')=='al_right')
  {
    if($this.hasClass('active'))
    {
      $('#'+edit_id).css({'text-align':''});
    }
    else
    {
      $('.tal').removeClass('active');
      $('#'+edit_id).css({'text-align':'right'});
    }
    
    $this.toggleClass('active');
  }
  
  
});

$('.colors_element').click(function(){
  var color = '#'+$(this).data('color');
  
  $('#'+edit_id).css({'background-image':'','background-color':color});
  
  var data = edit_id+'=background-color:'+color;
  
  jsonFunc(data);
  
});

$('.colors_text').click(function(){
  var color = '#'+$(this).data('color');
  
  $('#'+edit_id).css({'color':color});
  
  var data = edit_id+'_color'+'=color:'+color;
  
  jsonFunc(data);
  
});

/** switch blocks - images / form */
$('.but_s').click(function(){
  $('.but_s').removeClass('activ');
  $(this).addClass('activ');
  
  $('.bl_files').addClass('hide');

  $('.'+$(this).data('block')).removeClass('hide');
});

/** switch blocks - images / form */
$('.but_edit_bg').click(function(){
  $('.but_edit_bg').removeClass('activ');
  $(this).addClass('activ');
  
  $('.bloc_bg_edit').addClass('hide');

  $('.'+$(this).data('block')).removeClass('hide');
});
$('a, .navbar-collapse ul li a').click(function(){return false;});
