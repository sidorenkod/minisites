  <style>
    #modal_block{
      position: absolute;
      top: 50px;
      background: #fff;
      left: 50%;
      padding: 20px 10px;
      border: 1px solid #ccc;
      box-shadow: 2px 3px 6px rgba(85, 85, 85, 0.31);
      z-index: 9999;
      cursor: move;
    }
    #close_form{
      position: absolute;
      top: 0;
      right: 0;
      width: 20px;
      text-align: center;
      cursor: pointer;
      font-weight: bold;
    }
    .bloc_bg_edit img{cursor:pointer}
    .edit_text{cursor:pointer}
    .border-hover{border:1px solid #ccc}
    .add_style{
      cursor: pointer;
      font-size: 18px;
      border: 1px solid #eee;
      padding: 3px;
    }
    .add_style.active{
      border: 1px solid #aaa;
    }
    
    .edit_template{
      position: fixed;
      top: 50px;
      background: #fff;
      left: 0;
      padding: 20px 10px;
      border: 1px solid #ccc;
      box-shadow: 2px 3px 6px rgba(85, 85, 85, 0.31);
      z-index: 9999;
    }
    .not_show{left: -72px;}
    .open_close{
      position: absolute;
      right: -24px;
      width: 24px;
      height: 26px;
      top: 0px;
      padding-top: 1px;
      background: #fff;
      box-shadow: 5px 1px 6px rgba(85, 85, 85, 0.31);
      border-radius: 0 5px 5px 0;
      text-align: center;
      cursor: pointer;
      font-weight: bold;
    }
    .colors_edit{
      display:block;
      width:50px;
      height:25px;
      margin-bottom: 5px;
      cursor: pointer;
      border:1px solid #ccc;
    }
    .colors_element{
      display:inline-block;
      width:50px;
      height:25px;
      margin-bottom: 5px;
      cursor: pointer;
      border:1px solid #ccc;
    }
    .colors_text{
      display:inline-block;
      width:50px;
      height:25px;
      margin-bottom: 5px;
      cursor: pointer;
      border:1px solid #ccc;
    }
    .color_0{background: #ffffff;}
    .color_1{background: #028fcc;}
    .color_2{background: #f27935;}
    .color_3{background: #7FBA00;}
    .color_4{background: #0db3c7;}
    .color_5{background: #913d88;}
    .color_6{background: #f7ca18;}
    .users_files, .def_files{
      background: #ccc;
      margin-top: 5px;
    }
    .users_files img{cursor:pointer}
    .def_files img{cursor:pointer}
    .but_s,.but_edit_bg{
      cursor: pointer;
      display: inline-block;
      border: 1px solid #ccc;
      padding: 0 7px;
    }
    .but_s.activ, .but_edit_bg.activ{border-color:#777;}
    .change_bg{cursor:pointer;}
    .change_bg > div{cursor: default;}
    .change_bg > div.change_bg{cursor: pointer;}
    .edit_block{cursor: pointer;}
    .delete{
      position: absolute;
      top: 0;
      right: 0;
      background: #f00;
      width: 12px;
      text-align: center;
    }
    .cnange_img{cursor:pointer}
    .delete_link{cursor:pointer; color: #f00}
    .block_reset{
        position: fixed;
        top:0px;
        left:10px;
        z-index:9999;
    }
    .block_reset span{
        color: #f00;
        display: block;
        cursor: pointer;
        background: #fff;
        padding: 5px 10px;
        opacity: 0.5;
    }
    .block_reset span:hover{opacity: 1;}
    .quest{
        position: fixed;
        top: 50%;
        left: 50%;
        z-index: 9999;
        background: #fff;
        padding: 20px 20px;
        text-align: center;
        width: 326px;
        margin-left: -163px;
        margin-top: -53px;
        box-shadow: 1px 1px 9px #000;
    }
    .quest span{    
        display: inline-block;
        padding: 5px 20px;
        background: #eee;
        margin: 0 15px;
        cursor: pointer;
    }
    .quest span:hover{background: #ccc;}
    .quest p{}
    .this_icon{
      color: #555;
      font-size: 24px;
      cursor:pointer;
    }
  </style>
  
    <input type="hidden" id="temp_id" value="<?php echo $temp_id; ?>" />
    <?php if($editColorTheme){ ?>
    <div class="edit_template">
      
      <div class="open_close">
        <span class="open_bl hide">&gt;</span>
        <span class="close_bl">&lt;</span>
      </div>
      
      <div class="edit_template_bl">
        <span id="color_1" class="colors_edit color_1"></span>
        <span id="color_2" class="colors_edit color_2"></span>
        <span id="color_3" class="colors_edit color_3"></span>
        <span id="color_4" class="colors_edit color_4"></span>
        <span id="color_5" class="colors_edit color_5"></span>
        <span id="color_6" class="colors_edit color_6"></span>
      </div>
    </div>
    <?php } ?>
    
    <div class="block_reset">
        <span>Reset all changes</span>
    </div>
    
    <div class="quest hide">
        <p>Are you sure you want to reset all the changes?</p>
        <span class="yes">Yes</span>
        <span class="no">No</span>
    </div>
    
    <div id="modal_block" class="hide">
      
      <span id="close_form">X</span>
      
      <?php /** edit text */ ?>
      <div class="blockedit text_edit hide">
        <textarea id="m_text" style="width: 291px;height: 140px;"></textarea>
        <br />
        <span class="add_style tal glyphicon glyphicon-align-left" data-style="al_left"></span>
        <span class="add_style tal glyphicon glyphicon-align-center" data-style="al_center"></span>
        <span class="add_style tal glyphicon glyphicon-align-right" data-style="al_right"></span>
        <br>
        <span class="colors_text color_0" data-color="ffffff"></span>
        <span class="colors_text color_1" data-color="028fcc"></span>
        <span class="colors_text color_2" data-color="f27935"></span><br />
        <span class="colors_text color_3" data-color="7FBA00"></span>
        <span class="colors_text color_4" data-color="0db3c7"></span><br />
        <span class="colors_text color_5" data-color="913d88"></span>
        <span class="colors_text color_6" data-color="f7ca18"></span>
        <br>
        <input type="submit" id="m_save" value="Save" >
      </div>
      
      <?php /** edit icon */ ?>
      <div class="blockedit change_icon hide">
        
        <i class="fa fa-list this_icon" data-icon="fa-list"></i>
        <i class="fa fa-user-md this_icon" data-icon="fa-user-md"></i>
        <i class="fa fa-thumbs-up this_icon" data-icon="fa-thumbs-up"></i>
        <i class="fa fa-clipboard this_icon" data-icon="fa-clipboard"></i>
        <i class="fa fa-usd this_icon" data-icon="fa-usd"></i>
        <br>
        <i class="fa fa-flash this_icon" data-icon="fa-flash"></i>
        <i class="fa fa-check this_icon" data-icon="fa-check"></i>
        <i class="fa fa-plus this_icon" data-icon="fa-plus"></i>
        <i class="fa fa-cogs this_icon" data-icon="fa-cogs"></i>
        <i class="fa fa-sort this_icon" data-icon="fa-sort"></i>
        <br>
        <i class="fa fa-ils this_icon" data-icon="fa-ils"></i>
        <i class="fa fa-calculator this_icon" data-icon="fa-calculator"></i>
        <i class="fa fa-wifi this_icon" data-icon="fa-wifi"></i>
        <i class="fa fa-yelp this_icon" data-icon="fa-yelp"></i>
        <i class="fa fa-support this_icon" data-icon="fa-support"></i>
        <br>
        <i class="fa fa-cube this_icon" data-icon="fa-cube"></i>
        <i class="fa fa-language this_icon" data-icon="fa-language"></i>
        <i class="fa fa-fax this_icon" data-icon="fa-fax"></i>
        <i class="fa fa-building this_icon" data-icon="fa-building"></i>
        <i class="fa fa-bug this_icon" data-icon="fa-bug"></i>
        <br>
        <i class="fa fa-file this_icon" data-icon="fa-file"></i>
        <i class="fa fa-anchor this_icon" data-icon="fa-anchor"></i>
        <i class="fa fa-info this_icon" data-icon="fa-info"></i>
        <i class="fa fa-money this_icon" data-icon="fa-money"></i>
        <i class="fa fa-bars this_icon" data-icon="fa-bars"></i>
        
        <br />
        <input type="submit" id="change_icon_save" value="Save" >
      </div>
      
      <?php /** edit background */ ?>
      <div class="blockedit edit_bg hide">
        
        <div>
          <span class="but_edit_bg activ" data-block="images">Images</span>
          <span class="but_edit_bg" data-block="colors">Colors</span>
          <span class="but_edit_bg myimg" data-block="u_files">My images</span>
          <span class="but_edit_bg" data-block="f_files">Upload</span>
        </div>
        
        <div class="bloc_bg_edit images">
          <img src="/templates/template_2/images/1.jpg" width="100px" />
          <img src="/templates/template_2/images/2.jpg" width="100px" /><br /><br />
          <img src="/templates/template_2/images/3.jpg" width="100px" />
          <img src="/templates/template_2/images/4.jpg" width="100px" /><br /><br />
          <img src="/templates/template_2/images/5.jpg" width="100px" />
          <img src="/images/templ_img/bg1.jpg" width="100px" /><br /><br />
          <img src="/images/templ_img/bg2.jpg" width="100px" />
          <img src="/images/templ_img/bg3.jpg" width="100px" /><br /><br />
          <img src="/images/templ_img/bg4.jpg" width="100px" />
          <img src="/images/templ_img/bg5.jpg" width="100px" /><br /><br />
          <img src="/images/templ_img/bg6.jpg" width="100px" />
        </div>
        
        <div class="bloc_bg_edit colors hide">
          <span class="colors_element color_0" data-color="ffffff"></span>
          <span class="colors_element color_1" data-color="028fcc"></span>
          <span class="colors_element color_2" data-color="f27935"></span><br />
          <span class="colors_element color_3" data-color="7FBA00"></span>
          <span class="colors_element color_4" data-color="0db3c7"></span><br />
          <span class="colors_element color_5" data-color="913d88"></span>
          <span class="colors_element color_6" data-color="f7ca18"></span>
        </div>
        
        <div class="bloc_bg_edit u_files hide">
          <div class="file2_result"></div>
          <?=$templates->getDataImages('user_files_bg')?>
        </div>
        
        <form id="form_upload_bg" class="bloc_bg_edit f_files hide" method="post" action="/translator/upload-file?temp_id=<?php echo $temp_id; ?>&bg=1" enctype="multipart/form-data">
          <input type="file" id="fuser_file" name="fuser_file" />
          <br />
          <input type="submit" value="upload" />
          <span class="file_result"></span>
        </form>
        
        <br /><br />
        <input type="submit" id="m_save_bg" value="Save" > <input type="submit" id="m_cancel_bg" value="Cancel" >
      </div>
      
      <?php /** edit link */ ?>
      <div class="blockedit change_link">
        
        <div class="change_link_color">
          <span class="colors_text color_0" data-color="ffffff"></span>
        <span class="colors_text color_1" data-color="028fcc"></span>
        <span class="colors_text color_2" data-color="f27935"></span><br />
        <span class="colors_text color_3" data-color="7FBA00"></span>
        <span class="colors_text color_4" data-color="0db3c7"></span><br />
        <span class="colors_text color_5" data-color="913d88"></span>
        <span class="colors_text color_6" data-color="f7ca18"></span>
        </div>
        
        <textarea id="link_text" style="width: 291px;height: 140px;"></textarea>
        <br />
        <input type="submit" id="link_save" value="Save" >
        <br />
        <span class="delete_link">Delete</span>
      </div>
      
      <div class="blockedit edit_bottom_block">
        
        <div class="change_link_color">
        <p>color text</p>
        <span class="colors_text color_0" data-color="ffffff"></span>
        <span class="colors_text color_1" data-color="028fcc"></span>
        <span class="colors_text color_2" data-color="f27935"></span><br />
        <span class="colors_text color_3" data-color="7FBA00"></span>
        <span class="colors_text color_4" data-color="0db3c7"></span><br />
        <span class="colors_text color_5" data-color="913d88"></span>
        <span class="colors_text color_6" data-color="f7ca18"></span>
        </div>
        
        <div class="change_link_color">
        <p>background color</p>
        <span class="colors_element color_0" data-color="ffffff"></span>
          <span class="colors_element color_1" data-color="028fcc"></span>
          <span class="colors_element color_2" data-color="f27935"></span><br />
          <span class="colors_element color_3" data-color="7FBA00"></span>
          <span class="colors_element color_4" data-color="0db3c7"></span><br />
          <span class="colors_element color_5" data-color="913d88"></span>
          <span class="colors_element color_6" data-color="f7ca18"></span>
        </div>
        
        <textarea id="button_text" style="width: 291px;height: 140px;"></textarea>
        <br />
        <input type="submit" id="butt_save" value="Save" >
      </div>
      
      <div class="blockedit edit_soc_block hide">
        
        <textarea id="soc_href" style="width: 291px;height: 140px;"></textarea>
        <br />
        <input type="submit" id="soc_save" value="Save" >
        <br />
        <span class="delete_link">Delete</span>
      </div>
      
      <?php /** edit images */ ?>
      <div class="blockedit form_upload_file hide">
        <div>
          <span class="but_s def activ" data-block="def_files">Default</span>
          <span class="but_s myf" data-block="users_files">My files</span>
          <span class="but_s upl" data-block="form_files">Upload</span>
        </div>
        
        <div class="bl_files def_files">
          <img src="/templates/template_2/images/logo.png" width="148"><br />
          <img src="/templates/template_2/images/blog/1.jpg" width="148" >
          <img src="/templates/template_2/images/blog/2.jpg" width="148" ><br />
          <img src="/templates/template_2/images/blog/3.jpg" width="148" >
          <img src="/templates/template_2/images/affiliation/certifications.png" width="148" ><br />
          <img src="/templates/template_2/images/affiliation/affiliations.png" width="148" >
          <img src="/images/templ_img/img1.jpg" width="148" ><br />
          <img src="/images/templ_img/img2.jpg" width="148" >
        </div>
        
        <div class="bl_files users_files hide">
          <div class="file_result"></div>
          <?=$templates->getDataImages('user_files')?>
        </div>
        
        <form id="form_upload" class="bl_files form_files hide" method="post" action="/translator/upload-file?temp_id=<?php echo $temp_id; ?>" enctype="multipart/form-data">
          <input type="file" id="fuser_file" name="fuser_file" />
          <br />
          <input type="submit" value="upload" />
          <span class="file_result"></span>
        </form>
        <input type="submit" id="form_upload_file_save" value="Save" >
      </div>
      
    </div>
  
  <script type="text/javascript" src="/templates/edit_temp/js/edit.js"></script>
  <script type="text/javascript" src="/templates/edit_temp/js/drop.js"></script>
