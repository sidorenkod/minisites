<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo $templates->getMeta('title','Professional Translation Online | Smartlation.com') ?></title>
    <meta name="description" content="<?php echo $templates->getMeta('meta_desc') ?>">
    <meta name="keywords" content="<?php echo $templates->getMeta('meta_key') ?>">
    <meta name="author" content="Kompyle Technologies">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="<?=$tempPath?>/includes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- WOW JS Animation -->
    <link rel="stylesheet" href="<?=$tempPath?>/includes/wow-js/animations.css" />
    <!-- Litebox  -->
    <link rel="stylesheet" href="<?=$tempPath?>/includes/litebox/litebox.css" />
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?=$tempPath?>/includes/owl-carousel/owl.carousel.css" />
    <!-- custom css -->
    <link href="<?=$tempPath?>/css/style.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?=$tempPath?>/css/media.css" rel="stylesheet" type="text/css" media="screen">
    <!-- font awesome for icons -->
    <link href="<?=$tempPath?>/includes/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <!-- Favicons -->
    <link rel="icon" href="<?=$tempPath?>/favicon.ico">

    <!--[if lte IE 8]>
			<link rel="stylesheet" type="text/css" href="css/fucking-ie-8-and-lower.css" />
		<![endif]-->



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body onload="init()" id="body">

    <div id="preloader">
        <div class="spinner">
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
        </div>
    </div>
    <!--preloader end-->


    <section id="home">
        <div data-velocity="-.3" class="home-parallax"></div>
        <div class="home-overlay"></div>

        <div id="bg1" class="welcome-text change_bg" style="<?=$templates->getData('bg1')?>">
            <div class="container">
                <h2 class="wow animated fadeInDown">
                <span style="<?=$templates->getData('text1_style')?>" class="edit_text" id="text1" ><?=$templates->getData('text1')?></span>
                <span class="edit_text" id="text2" style="<?=$templates->getData('text2_style')?>"> <?=$templates->getData('text2')?></span></h2>
                <p class="wow animated fadeInUp edit_text" style="<?=$templates->getData('text333_style')?>" id="text333"><?=$templates->getData('text333')?></p>
                <br/>
                <a href="/upload" class="translate edit_button" style="<?=$templates->getData('butt1_style')?>" id="butt1"><?=$templates->getData('butt1')?></a>
                <br/>
                <br/>
                <br/>
                <a href="" class="translatetwo edit_button" style="<?=$templates->getData('butt2_style')?>" id="butt2"><?=$templates->getData('butt2')?></a>
                <br/>
            </div>
        </div>
    </section>
    <!--home section end-->

    <nav id="navigation">
        <div id="bg2" class="navbar navbar-default navbar-static-top change_bg" role="navigation" style="<?=$templates->getData('bg1')?>">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img class="cnange_img" id="logo" src="<?=$templates->getData('logo')?>" height="30px;" style="margin-top:-10px;" /></a>
                </div>
                <div class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right">
                    
                        <?php if($templates->chekData('link1_block')) { ?>
                        <li class="link1_block"><a href="/" style="<?=$templates->getData('link1_style')?>" id="link1" data-id="link1" class="edit_link"><?=$templates->getData('link1')?></a></li>
                        <?php } ?>
                        <?php if($templates->chekData('link11_block')) { ?>
                        <li class="link11_block">
                          <a href="/#about-us" style="<?=$templates->getData('link11_style')?>" id="link11" data-id="link11" class="edit_link" ><?=$templates->getData('link11')?></a>
                        </li>
                        <?php } ?>
                        <?php if($templates->chekData('link13_block')) { ?>
                        <li class="link13_block">
                          <a href="/#services" id="link13" style="<?=$templates->getData('link13_style')?>" data-id="link13" class="edit_link" ><?=$templates->getData('link13')?></a>
                        </li>
                        <?php } ?>
                        <?php if($templates->chekData('link10_block')) { ?>
                        <li class="link10"><a href="/#contact" id="link10" style="<?=$templates->getData('link10_style')?>" data-id="link10" class="edit_link"><?=$templates->getData('link10')?></a></li>
                        <?php } ?>
                    </ul>

                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container -->
        </div>
        <!--navbar-default-->
    </nav>
    <!--navigation section end here-->

    <section id="about-us" class="change_bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow animated fadeInUp">
                    <h2 class="page-title edit_text" id="text3" style="<?=$templates->getData('text3_style')?>"><?=$templates->getData('text3')?></h2>
                    <p class="edit_text" style="<?=$templates->getData('text4_style')?>" id="text4"><?=$templates->getData('text4')?></p>

                    <p>
                      <a href="/upload" style="<?=$templates->getData('btnnn2_style')?>" class="btn btn-filled edit_button" id="btnnn2"><?=$templates->getData('btnnn2')?></a> 
                      <a href="" style="<?=$templates->getData('btnnn3_style')?>" class="btn btn-bordered edit_button" id="btnnn3"><?=$templates->getData('btnnn3')?></a>
                    </p>
                </div>

                <div class="col-md-6 wow animated fadeInUp">
                    <img class="img-responsive cnange_img" id="img2" src="<?=$templates->getData('img2')?>" alt="" />
                </div>
            </div>
        </div>
    </section>

    <section id="services" class="section-padding">
        <div id="bg3" data-velocity="-.1" class="features-bg change_bg"> </div>
        <div class="container">
            <div class="row wow animated fadeInDown">
                <div class="col-md-12 text-center hero-title">
                    <i style="margin-top:-30px; background-color:white; color:#57bddb" class="fa fa-list"></i>
                    <h1 style="<?=$templates->getData('text5_style')?>" class="edit_text" id="text5"><?=$templates->getData('text5')?></h1>
                </div>
                <!--hero title end-->
            </div>
            <div class="row">
                <?php if($templates->chekData('block5')) { ?>
                <div id="block5" class="edit_block col-md-4 col-sm-6 wow animated fadeInLeft" data-wow-duration="700ms" data-wow-delay="100ms">
                    <div class="features-item">
                        <i class="fa fa-user-md"></i>
                        <h2 class="edit_text" style="<?=$templates->getData('text6_style')?>" id="text6"><?=$templates->getData('text6')?></h2>
                        <p class="edit_text" style="<?=$templates->getData('text7_style')?>" id="text7"><?=$templates->getData('text7')?></p>
                    </div>
                </div>
                <?php } ?>
                <?php if($templates->chekData('block6')) { ?>
                <!--feature col end-->
                <div id="block6" class="edit_block col-md-4 col-sm-6 wow animated fadeInLeft" data-wow-duration="700ms" data-wow-delay="200ms">
                    <div class="features-item">
                        <i class="fa fa-thumbs-up"></i>
                        <h2 class="edit_text" style="<?=$templates->getData('text8_style')?>" id="text8"><?=$templates->getData('text8')?></h2>
                        <p class="edit_text" style="<?=$templates->getData('text9_style')?>" id="text9"><?=$templates->getData('text9')?></p>
                    </div>
                </div>
                <?php } ?>
                <?php if($templates->chekData('block7')) { ?>
                <!--feature col end-->
                <div id="block7" class="edit_block col-md-4 col-sm-6 wow animated fadeInLeft" data-wow-duration="700ms" data-wow-delay="300ms">
                    <div class="features-item">
                        <i class="fa fa-clipboard"></i>
                        <h2 class="edit_text" style="<?=$templates->getData('text10_style')?>" id="text10"><?=$templates->getData('text10')?></h2>
                        <p class="edit_text" style="<?=$templates->getData('text11_style')?>" id="text11"><?=$templates->getData('text11')?></p>
                    </div>
                </div>
                <?php } ?>
                <?php if($templates->chekData('block8')) { ?>
                <!--feature col end-->
                <div id="block8" class="edit_block col-md-4 col-sm-6 wow animated fadeInLeft" data-wow-duration="700ms" data-wow-delay="400ms">
                    <div class="features-item">
                        <i class="fa fa-usd"></i>
                        <h2 class="edit_text" style="<?=$templates->getData('text12_style')?>" id="text12"><?=$templates->getData('text12')?></h2>
                        <p class="edit_text" style="<?=$templates->getData('text13_style')?>" id="text13"><?=$templates->getData('text13')?></p>
                    </div>
                </div>
                <?php } ?>
                <?php if($templates->chekData('block9')) { ?>
                <!--feature col end-->
                <div id="block9" class="edit_block col-md-4 col-sm-6 wow animated fadeInLeft" data-wow-duration="700ms" data-wow-delay="500ms">
                    <div class="features-item">
                        <i class="fa fa-flash"></i>
                        <h2 class="edit_text" id="text14" style="<?=$templates->getData('text14_style')?>"><?=$templates->getData('text14')?></h2>
                        <p class="edit_text" id="text15" style="<?=$templates->getData('text15_style')?>"><?=$templates->getData('text15')?></p>
                    </div>
                </div>
                <?php } ?>
                <?php if($templates->chekData('block10')) { ?>
                <!--feature col end-->
                <div id="block10" class="edit_block col-md-4 col-sm-6 wow animated fadeInLeft" data-wow-duration="700ms" data-wow-delay="600ms">
                    <div class="features-item">
                        <i class="fa fa-check"></i>
                        <h2 class="edit_text" id="text16" style="<?=$templates->getData('text16_style')?>"><?=$templates->getData('text16')?></h2>
                        <p class="edit_text" id="text17" style="<?=$templates->getData('text17_style')?>"><?=$templates->getData('text17')?></p>
                    </div>

                </div>
                <?php } ?>
                <!--feature col end-->
            </div>
            <div class="row wow animated fadeInDown">
                <div class="col-md-12 text-center hero-title">
                    <h4  class="edit_text" id="text18" style="<?=$templates->getData('text18_style')?>"><?=$templates->getData('text18')?></h4>
                </div>
                <!--hero title end-->
            </div>

        </div>
    </section>
    <section id="call-to-action" class="change_bg">
        <div class="container">
            <div class="row wow animated fadeInDown">
                <div class="col-md-12">
                    <div class="cta-content">
                        <a style="<?=$templates->getData('text19_style')?>" href=""  class="edit_text" id="text19"><?php echo $templates->getData('text19', 'Join Us');?></a>
                        <a style="<?=$templates->getData('text20_style')?>" href="translate.html"  class="edit_text" id="text20"><?php echo $templates->getData('text20', 'Translate Now');?></a>
                        <a style="<?=$templates->getData('text21_style')?>" href="" class="edit_text" id="text21"><?php echo $templates->getData('text21', 'Contact');?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="contact" class="call-to-action">
        <div class="container">
            <div class="row wow animated fadeInDown">
                <div class="col-md-12">

 <section class="contact-me">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4"></div>

            <div class="col-md-4"></div>
    </div>
      
    <div class="row">
        <center>
              <div class="col-md-2"></div>
      <div class="col-md-8">
        <h4>CONCTAC  US  </h4>
          <br>
        <div class="form-box clfx">
        <form method="post" action="/en/contact/sending" id="contact-form" class="clfx">

                    <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" id="full_name" name="full_name" value="" placeholder="Full name">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control " id="email_or_phone" name="email_or_phone" value="" placeholder="Email">
              </div>
            </div>
          </div>
            
              <div class="row">

            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control " id="email_or_phone" name="email_or_phone" value="" placeholder="Phone">
              </div>
            </div>
                  
            <div class="col-md-6">
              <div class="form-group">
                <label for="file_form" id="l_sf"  class="form-control add-file">Add attachment</label>
                <p id="text_sf" class="ds-n">Add attachment</p>
                <input type="file" id="file_form" class="ds-n" name="file_form">
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-12">
              <textarea name="message" id="message" class=" textarea-form form-control " placeholder="MESSAGE" rows="3"></textarea>
            </div>
          </div>
          <button type="submit" class="btn btn-filled">CONCTAC ME </button>
          <div class="input-group">
            <div class="loader_progress_cus mg-n hide"></div>
          </div>
        </form>
          </div>
      </div>
      <div class="col-md-2"></div>
    </div>
        
        </center>

  </div>
</section>
                    
                    
                    
                </div>
            </div>
        </div>
    </section>


    <section id="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                   
                   <?php if($templates->chekData('block1')) { ?>
                   <div id="block1" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="<?=$templates->getData('text37_style')?>" class="title-widget edit_text" id="text37"><?=$templates->getData('text37')?></h1>

                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text22_style')?>" class="edit_text" id="text22"><?=$templates->getData('text22')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text23_style')?>" class="edit_text" id="text23"><?=$templates->getData('text23')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text24_style')?>" class="edit_text" id="text24"><?=$templates->getData('text24')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text25_style')?>" class="edit_text" id="text25"><?=$templates->getData('text25')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <?php } ?>
                    <?php if($templates->chekData('block2')) { ?>
                    <!-- widgets column left end -->
                     <div id="block2" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="<?=$templates->getData('text38_style')?>" class="title-widget edit_text" id="text38"><?=$templates->getData('text38')?></h1>

                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text26_style')?>" class="edit_text" id="text26"><?=$templates->getData('text26')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text27_style')?>" class="edit_text" id="text27"><?=$templates->getData('text27')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text28_style')?>" class="edit_text" id="text28"><?=$templates->getData('text28')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text29_style')?>" class="edit_text" id="text29"><?=$templates->getData('text29')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <?php } ?>
                    <?php if($templates->chekData('block3')) { ?>
                    <!-- widgets column left end -->
                     <div id="block3" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="<?=$templates->getData('text39_style')?>" class="title-widget edit_text" id="text39"><?=$templates->getData('text39')?></h1>

                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text30_style')?>" class="edit_text" id="text30"><?=$templates->getData('text30')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text31_style')?>" class="edit_text" id="text31"><?=$templates->getData('text31')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span style="<?=$templates->getData('text32_style')?>" class="edit_text" id="text32"><?=$templates->getData('text32')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <!-- widgets column left end -->
                    <?php } ?>
                    <?php if($templates->chekData('block4')) { ?>
                    <div id="block4" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="<?=$templates->getData('text40_style')?>" class="title-widget edit_text" id="text40"><?=$templates->getData('text40')?></h1>

                                <ul>
                                    <li><a href="#"  data-toggle="modal" data-target="#myModal"><i class="fa fa-file"></i> <span style="<?=$templates->getData('text33_style')?>" class="edit_text" id="text33"><?=$templates->getData('text33')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-play"></i> <span style="<?=$templates->getData('text34_style')?>" class="edit_text" id="text34"><?=$templates->getData('text34')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-comment"></i> <span style="<?=$templates->getData('text35_style')?>" class="edit_text" id="text35"><?=$templates->getData('text35')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-pencil"></i> <span style="<?=$templates->getData('text36_style')?>" class="edit_text" id="text36"><?=$templates->getData('text36')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <?php } ?>
                    <!-- widgets column left end -->
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                    
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Supported File Formats</h4>
                          </div>
                          <div class="modal-body">
                            <p><span style="color:#57bddb; font-weight:700;">Text Files:</span> *.txt, *.doc, *.docx, *.pdf, *.xls, *.xlsx, *.ppt, *.pptx</p>
                            <p><span style="color:#57bddb; font-weight:700;">Images Files:</span> *.jpg, *.png, *.gif, *.tif, *.tiff</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                    
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <footer style="margin-top:10px;">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="footer-copyright">
                        <p>
                            <a href=""><img src="<?=$tempPath?>/img/logoB&W.png" height="17px;"></a> &copy; 2015 | <a href="" >Terms & Conditions</a> | <a href="">Privacy Policy</a></p>
                    </div>
                </div>
                <!--copyright col end-->

                <div class="col-md-3 col-sm-3">
                    <div class="footer_mid pull-right">
                        <ul class="social-contact list-inline">
                            <?php if($templates->chekData('soc1')) { ?>
                            <li id="soc1"> 
                                <a id="soc1_link" data-id="soc1" href="<?=$templates->getData('soc1_link')?>" class="edit_soc"><i class="fa fa-facebook"></i></a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc2')) { ?>
                            <li id="soc2"> 
                                <a id="soc2_link" data-id="soc2" href="<?=$templates->getData('soc2_link')?>" class="edit_soc"><i class="fa fa-twitter"></i></a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc3')) { ?>
                            <li id="soc3"> 
                                <a id="soc3_link" data-id="soc3" href="<?=$templates->getData('soc3_link')?>" class="edit_soc"><i class="fa fa-google-plus"></i> </a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc4')) { ?>
                            <li id="soc4">
                                <a id="soc4_link" data-id="soc4" href="<?=$templates->getData('soc4_link')?>" class="edit_soc"> <i class="fa fa-linkedin"></i></a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc5')) { ?>
                            <li id="soc5">
                                <a id="soc5_link" data-id="soc5" href="<?=$templates->getData('soc5_link')?>" class="edit_soc"> <i class="fa fa-pinterest"></i></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!--blogname col end-->
            </div>
        </div>
    </footer>

    <!--scripts and plugins -->
    <!--must need plugin jquery-->
    <script src="<?=$tempPath?>/js/jquery.min.js"></script>
    <!--bootstrap js plugin-->
    <script src="<?=$tempPath?>/includes/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Google Maps API -->
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- Custom Google Map -->
    <script type="text/javascript">
        var maps;
			var geocoder;

			function init() {
				maps = [];
				geocoder = new google.maps.Geocoder();

				var styles = {
				  'Red': [
					{
					  featureType: 'all',
					  stylers: [{saturation:-100},{gamma:0.50}]
					}
				  ]
				};

				for (var s in styles) {
				  var opt = {
					mapTypeControlOptions: {
					  mapTypeIds: [s]
					},
					disableDefaultUI: true,
					navigationControl: true,
					scrollwheel: false,
					center: new google.maps.LatLng(42.415109, -96.842471),
					zoom: 8,
					mapTypeId: s
				  };

				  var div = document.getElementById('map');
				  var map = new google.maps.Map(div, opt);
				  var styledMapType = new google.maps.StyledMapType(styles[s], {name: s});
				  map.mapTypes.set(s, styledMapType);
				  
				  var goldStar = 'templates/template_1/img/marker.png';

				  var marker = new google.maps.Marker({
					position: map.getCenter(),
					icon: goldStar,
					map: map
				  });				  
				}
			}
    </script>
    <!--easying scroll effect-->
    <script src="<?=$tempPath?>/js/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?=$tempPath?>/js/jquery.sticky.js" type="text/javascript"></script>
    <script src="<?=$tempPath?>/js/jquery.nav.js" type="text/javascript"></script>
    <!--owl carousel-->
    <script src="<?=$tempPath?>/includes/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
    <!--wow animation-->
    <script src="<?=$tempPath?>/includes/wow-js/wow.min.js" type="text/javascript"></script>

    <!--Activating WOW Animation only for modern browser-->
    <!--[if !IE]><!-->
    <script type="text/javascript">
        new WOW().init();
    </script>
    <!--<![endif]-->

    <!--Oh Yes, IE 9+ Supports animation, lets activate for IE 9+-->
    <!--[if gte IE 9]>
			<script type="text/javascript">new WOW().init();</script>
		<![endif]-->

    <!--Opacity & Other IE fix for older browser-->
    <!--[if lte IE 8]>
			<script type="text/javascript" src="js/ie-opacity-polyfill.js"></script>
		<![endif]-->


    <!--parallax-->
    <script src="<?=$tempPath?>/js/jquery.scrolly.js" type="text/javascript"></script>
    <!--portfolio-filter-->
    <script src="<?=$tempPath?>/js/isotope.pkgd.min.js" type="text/javascript"></script>
    <!--lightbox-->
    <script src="<?=$tempPath?>/includes/litebox/images-loaded.min.js" type="text/javascript"></script>
    <script src="<?=$tempPath?>/includes/litebox/litebox.min.js" type="text/javascript"></script>
    <!--project count-->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="<?=$tempPath?>/js/jquery.counterup.min.js" type="text/javascript"></script>
    <!--custom scrollbar-->
    <script src="<?=$tempPath?>/js/jquery.nicescroll.min.js" type="text/javascript"></script>

    <!--customizable plugin edit according to your needs-->
    <script src="<?=$tempPath?>/js/custom.js" type="text/javascript"></script>
    
    <?php
    if($templates->edit())
    {
      $editColorTheme = false;
      $patch_edit = $path = 'templates/edit_temp/edit.php';
      include $patch_edit;
    }
  ?>
    
</body>
</html>
