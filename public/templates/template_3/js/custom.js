(function ($) {
	"use strict";
  
  var intervalID_s;
  
  function interval_s()
  {
    $.post( 
      "/checkout/checkprice/", 
      { order_id: $('#order_id').val()}, 

      function(data)
      {
          if(data.result !== false)
          {
              clearInterval(intervalID_s);

              $('.wait').addClass('hide');
              $('.form_payment').removeClass('hide');

              $('.price_usd').text(data.price);
              $('.price_sek').text(data.priceSek);
              $('.deadlinedays').text(data.days);
              
              if(data.days > 1)
              {
                $('.daytext').addClass('hide');
                $('.daystext').removeClass('hide');
              }
              else
              {
                $('.daytext').removeClass('hide');
                $('.daystext').addClass('hide');
              }
          }
      }     
      , 
      "json");

  }
  
  $(document).ready(function(){
    if($('#process').val() == 1)
    {
      intervalID_s = setInterval(interval_s, 3000);
    }
  });


	$(document).ready(function() {

		// jQuery DemoPanel
		$("#panel_open").click(function(){
			$(".setting_panel").animate({left:"-226px"});
			$(this).hide();
			$("#panel_close").css("display","block");
		});	

		$("#panel_close").click(function(){
			$(".setting_panel").animate({left:"0px"});
			$(this).hide();
			$("#panel_open").css("display","block");
		});	

		// jQuery Custom scrollbar
		$("body").niceScroll({
			cursorcolor: "#57BDDB",
			cursorborderradius: "0px",
			cursorwidth: "10px",
			cursorminheight: 100,
			cursorborder: "0px solid #fff",
			zindex: 9999,
			autohidemode: true,
			horizrailenabled:false
		});

		// jQuery Stick menu		
		$(".navbar").sticky({topSpacing: 0});
		
		// jQuery One page nav
		$(".nav").onePageNav();
		
		// jQuery Portfolio filter
		$("#portfolio_list").isotope({
		  // options
		  itemSelector: '.pitem',
		  layoutMode: 'fitRows'
		});	
		
		
		// jQuery Owl Carousel
		$(".partner-list").owlCarousel({
			pagination : false,
			navigation : true,
			navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
		
		
		// jQuery counter
		$('.counter').counterUp({
			delay: 10,
			time: 1000
		});
		
		// jQuery Lightbox
		$(".litebox").liteBox();	
		
		// jQuery Parallax
		$('.home-parallax, .features-bg, .parallax-bg').scrolly({bgParallax: true});
		
		
		
		// jQuery Contact form validation
		/*$('form').validatr({
			location:"bottom"
		}); */
		
		$("#form-messages").hover(function() {
			$(this).delay(350).fadeOut('slow');
		});
		
		

		
	});





	/* ==============================================
	 Smooth Scroll To Anchor
	 =============================================== */

	//jQuery for page scrolling feature - requires jQuery Easing plugin
	/*$(function() {
		$('.navbar a,.btn,.to-top').bind('click', function(event) {
			var $anchor = $(this);
			$('html').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top - 50
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});
	});*/




	$(window).load(function(){

		
		
		// Preloader & Option Panel
		$('.spinner').fadeOut();
		$('.setting_panel').show();
		
		$('#preloader').delay(350).fadeOut('slow');
		
		$('body').delay(350);
		

		// Portfolio Items
		var $container = $('#portfolio_list');
		$container.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
	 
		$('.portfolio-filters li').click(function(){
			$('.portfolio-filters .active').removeClass('active');
			$(this).addClass('active');
	 
			var selector = $(this).attr('data-filter');
			$container.isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			 });
			 return false;
		});
	});
  
  var chekFile_id;
  var order_id;
  function chekFile()
  {
    var redirect = true;
    
    $('.file-preview-frame').each(
        function()
        {
          if($(this).hasClass('file-preview-error'))
          {
            clearInterval(chekFile_id);
            redirect = false;
            console.log('interval clear');
          }
          else if(!$(this).hasClass('file-preview-success'))
          {
            redirect = false;
            console.log('redirect false');
          }
        }
      );
      
     if(redirect)
     {
       window.location = '/checkout?id='+order_id;
     }
  }
  
  $('.process').on('click',function(e) {
    var error = false;
    var data = '';

    if($('#ffrom_lang').val() == '')
    {
      error = true;
    }
    else
    {
      data = 'from_lang='+$('#ffrom_lang').val();
    }
    
    if($('#fdest_lang').val() == '')
    {
      error = true;
    }
    else
    {
      data += '&fdest_lang='+$('#fdest_lang').val();
    }
    
    if($('#fexpertise').val() == '')
    {
      error = true;
    }
    else
    {
      data += '&fexpertise='+$('#fexpertise').val();
    }
    
    if($('.file-input').hasClass('file-input-ajax-new'))
    {
      console.log($('.file-input'));
      error = true;
      $('.error_file').removeClass('hide');
    }
    
    if(!error)
    {
      data += '&note='+$('#note').val();
    
      $.post('/upload', data, function(json){
      
        document.getElementById("upload_file_a").click();
        
        //console.log(json);
        
        order_id = json.order_id;
        
        chekFile_id = setInterval(chekFile, 1000);
        
        },  'json');
    }
    
    return false;
  });
  

}(jQuery));	