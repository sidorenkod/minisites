<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo $templates->getMeta('title','Professional Translation Online | Smartlation.com') ?></title>
    <meta name="description" content="<?php echo $templates->getMeta('meta_desc') ?>">
    <meta name="keywords" content="<?php echo $templates->getMeta('meta_key') ?>">
    <meta name="author" content="Kompyle Technologies">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Color Pickr -->
   
    <!-- Bootstrap -->
    <link href="<?=$tempPath?>/includes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- WOW JS Animation -->
    <link rel="stylesheet" href="<?=$tempPath?>/includes/wow-js/animations.css" />
    <!-- Litebox  -->
    <link rel="stylesheet" href="<?=$tempPath?>/includes/litebox/litebox.css" />
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="<?=$tempPath?>/includes/owl-carousel/owl.carousel.css" />
    <!-- custom css -->
    <link href="<?=$tempPath?>/css/style.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?=$tempPath?>/css/media.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?=$tempPath?>/css/fileinput.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?=$tempPath?>/css/fileinput.min.css" rel="stylesheet" type="text/css" media="screen">
    <!-- font awesome for icons -->
    <link href="<?=$tempPath?>/includes/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Web Fonts -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300' rel='stylesheet' type='text/css'>
    <!-- Favicons -->
    <link rel="icon" href="/favicon.ico">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="<?=$tempPath?>/js/fileinput.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>

    <!--[if lte IE 8]>
			<link rel="stylesheet" type="text/css" href="css/fucking-ie-8-and-lower.css" />
		<![endif]-->



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body id="body">

    <div id="preloader">
        <div class="spinner">
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
            <div class="cube"></div>
        </div>
    </div>
    <!--preloader end-->

    <nav id="navigation">
        <div id="bg2nav" class="navbar navbar-default navbar-static-top change_bg" role="navigation" style="<?=$templates->getData('bg2nav')?>">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img class="cnange_img" id="logo" src="<?=$templates->getData('logo')?>" height="30px;"" style="margin-top:-10px;" /></a>
                </div>
                <div class="navbar-collapse collapse">

                    <ul class="nav navbar-nav navbar-right">
                    
                        <?php if($templates->chekData('link1_block')) { ?>
                        <li class="link1_block"><a href="/" style="<?=$templates->getData('link1_style')?>" id="link1" data-id="link1" class="edit_link"><?=$templates->getData('link1')?></a></li>
                        <?php } ?>
                        <?php if($templates->chekData('link11_block')) { ?>
                        <li class="link11_block">
                          <a href="/#about-us" style="<?=$templates->getData('link11_style')?>" id="link11" data-id="link11" class="edit_link" ><?=$templates->getData('link11')?></a>
                        </li>
                        <?php } ?>
                        <?php if($templates->chekData('link13_block')) { ?>
                        <li class="link13_block">
                          <a href="/#services" id="link13" style="<?=$templates->getData('link13_style')?>" data-id="link13" class="edit_link" ><?=$templates->getData('link13')?></a>
                        </li>
                        <?php } ?>
                        <?php if($templates->chekData('link10_block')) { ?>
                        <li class="link10">
                          <a href="/#contact" id="link10" style="<?=$templates->getData('link10_style')?>" data-id="link10" class="edit_link"><?=$templates->getData('link10')?></a>
                        </li>
                        <?php } ?>
                    </ul>

                </div>
                <!--/.nav-collapse -->
            </div>
            <!--/.container -->
        </div>
        <!--navbar-default-->
    </nav>
    <!--navigation section end here-->

    <section id="about" class="change_bg" style="<?=$templates->getData('about')?>">
        <div class="container">
            <div class="row">
                <div class="col-md-6 wow animated fadeInUp">
                    <img class="cnange_img" id="img2" src="<?=$templates->getData('img2')?>">
                    <h2 class="page-title edit_text" id="text1" style="margin-top:30px;" ><?=$templates->getData('text1')?></h2>
                    <p><i class="fa fa-check-square-o"></i> <span class="edit_text" id="text2"><?=$templates->getData('text2')?></span></p>
                    <p><i class="fa fa-upload"></i> <span class="edit_text" id="text3"><?=$templates->getData('text3')?></span></p>
                    <p><i class="fa fa-search"></i> <span class="edit_text" id="text4"><?=$templates->getData('text4')?></span></p>

                    <p id="formats" style="background-color:#999966">TXT</p>
                    <p id="formats" style="background-color:cornflowerblue">DOC</p>
                    <p id="formats" style="background-color:#006699">DOX</p>
                    <p id="formats" style="background-color:#e6b800">PDF</p>
                    <p id="formats" style="background-color:#00cc66">XLS</p>
                    <p id="formats" style="background-color:#009933">XLSX</p>
                    <p id="formats" style="background-color:#cc0000">PPT</p>
                    <p id="formats" style="background-color:#993333">PPTX</p>
                    <p id="formats" style="background-color:#000000">JPG</p>
                    <p id="formats" style="background-color:#66ccff">PNG</p>
                    <p id="formats" style="background-color:#99cc00">GIF</p>
                    <p id="formats" style="background-color:#993366">TIF</p>
                </div>

                <div class="col-md-6 wow animated fadeInUp" style="background-color: rgba(255, 255, 255, 0.5);">
                    <h4 class="edit_text" id="text41"><?=$templates->getData('text41')?></h4>
                    <select name="ffrom_lang" type="text" value="" required class="form-control">
                        <option value="0">Source Language</option>
                        <optgroup label="Popular Languages">
                            <option value="159">Arabic</option>
                            <option value="901">Chinese</option>
                            <option value="902">English</option>
                            <option value="903">French</option>
                            <option value="904">German</option>
                            <option value="74">Hebrew</option>
                            <option value="79">Italian</option>
                            <option value="81">Japanese</option>
                            <option value="108">Russian</option>
                            <option value="905">Spanish</option>
                        </optgroup>
                        <optgroup label="Additional Languages">
                            <option value="1">Afrikaans</option>
                            <option value="2">Albanian</option>
                            <option value="3">Amharic</option>
                            <option value="159">Arabic</option>
                            <option value="4">Arabic - Algeria</option>
                            <option value="5">Arabic - Bahrain</option>
                            <option value="6">Arabic - Egypt</option>
                            <option value="7">Arabic - Iraq</option>
                            <option value="8">Arabic - Jordan</option>
                            <option value="9">Arabic - Kuwait</option>
                            <option value="10">Arabic - Lebanon</option>
                            <option value="11">Arabic - Libya</option>
                            <option value="12">Arabic - Morocco</option>
                            <option value="13">Arabic - Oman</option>
                            <option value="14">Arabic - Qatar</option>
                            <option value="15">Arabic - Saudi Arabia</option>
                            <option value="16">Arabic - Syria</option>
                            <option value="17">Arabic - Tunisia</option>
                            <option value="18">Arabic - United Arab Emirates</option>
                            <option value="19">Arabic - Yemen</option>
                            <option value="20">Armenian</option>
                            <option value="21">Assamese</option>
                            <option value="22">Azeri - Cyrillic (Arzebaijan)</option>
                            <option value="23">Basque</option>
                            <option value="24">Belarusian</option>
                            <option value="25">Bengali</option>
                            <option value="26">Bosnian</option>
                            <option value="27">Bulgarian</option>
                            <option value="28">Burmese</option>
                            <option value="29">Catalan</option>
                            <option value="901">Chinese</option>
                            <option value="30">Chinese - China</option>
                            <option value="31">Chinese - Hong Kong SAR</option>
                            <option value="32">Chinese - Macau SAR</option>
                            <option value="33">Chinese - Singapore</option>
                            <option value="34">Chinese - Taiwan</option>
                            <option value="35">Croatian</option>
                            <option value="36">Czech</option>
                            <option value="37">Danish</option>
                            <option value="908">Dari</option>
                            <option value="38">Divehi - Dhivehi - Maldivian</option>
                            <option value="39">Dutch - Belgium</option>
                            <option value="40">Dutch - Netherlands</option>
                            <option value="902">English</option>
                            <option value="41">English - Australia</option>
                            <option value="42">English - Belize</option>
                            <option value="43">English - Canada</option>
                            <option value="44">English - Caribbean</option>
                            <option value="45">English - Great Britain</option>
                            <option value="46">English - India</option>
                            <option value="47">English - Ireland</option>
                            <option value="48">English - Jamaica</option>
                            <option value="49">English - New Zealand</option>
                            <option value="50">English - Phillippines</option>
                            <option value="51">English - Southern Africa</option>
                            <option value="52">English - Trinidad</option>
                            <option value="53">English - United States</option>
                            <option value="54">Estonian</option>
                            <option value="55">Faroese</option>
                            <option value="56">Farsi - Persian</option>
                            <option value="57">Finnish</option>
                            <option value="903">French</option>
                            <option value="58">French - Belgium</option>
                            <option value="59">French - Canada</option>
                            <option value="60">French - France</option>
                            <option value="61">French - Luxembourg</option>
                            <option value="62">French - Switzerland</option>
                            <option value="64">Gaelic - Ireland</option>
                            <option value="65">Gaelic - Scotland</option>
                            <option value="170">Georgian</option>
                            <option value="904">German</option>
                            <option value="66">German - Austria</option>
                            <option value="67">German - Germany</option>
                            <option value="68">German - Liechtenstein</option>
                            <option value="69">German - Luxembourg</option>
                            <option value="70">German - Switzerland</option>
                            <option value="71">Greek</option>
                            <option value="72">Guarani - Paraguay</option>
                            <option value="73">Gujarati</option>
                            <option value="173">Hausa</option>
                            <option value="74">Hebrew</option>
                            <option value="75">Hindi</option>
                            <option value="76">Hungarian</option>
                            <option value="77">Icelandic</option>
                            <option value="172">Igbo</option>
                            <option value="78">Indonesian</option>
                            <option value="79">Italian - Italy</option>
                            <option value="80">Italian - Switzerland</option>
                            <option value="81">Japanese</option>
                            <option value="82">Kannada</option>
                            <option value="83">Kashmiri</option>
                            <option value="84">Kazakh</option>
                            <option value="85">Khmer</option>
                            <option value="86">Korean</option>
                            <option value="158">Kurdish</option>
                            <option value="87">Lao</option>
                            <option value="88">Latin</option>
                            <option value="89">Latvian</option>
                            <option value="90">Lithuanian</option>
                            <option value="63">Macedonian</option>
                            <option value="91">Malay - Brunei</option>
                            <option value="92">Malay - Malaysia</option>
                            <option value="93">Malayalam</option>
                            <option value="94">Maltese</option>
                            <option value="95">Maori</option>
                            <option value="96">Marathi</option>
                            <option value="97">Mongolian</option>
                            <option value="98">Nepali</option>
                            <option value="99">Norwegian - Bokml</option>
                            <option value="100">Oriya</option>
                            <option value="907">Pashto</option>
                            <option value="101">Polish</option>
                            <option value="102">Portuguese - Brazil</option>
                            <option value="103">Portuguese - Portugal</option>
                            <option value="104">Punjabi</option>
                            <option value="105">Raeto-Romance</option>
                            <option value="106">Romanian - Moldova</option>
                            <option value="107">Romanian - Romania</option>
                            <option value="108">Russian</option>
                            <option value="906">Samoan</option>
                            <option value="110">Sanskrit</option>
                            <option value="111">Serbian - Cyrillic</option>
                            <option value="112">Setsuana</option>
                            <option value="113">Sindhi</option>
                            <option value="114">Sinhala; Sinhalese</option>
                            <option value="115">Slovak</option>
                            <option value="116">Slovenian</option>
                            <option value="117">Somali</option>
                            <option value="118">Sorbian</option>
                            <option value="905">Spanish</option>
                            <option value="119">Spanish - Argentina</option>
                            <option value="120">Spanish - Bolivia</option>
                            <option value="121">Spanish - Chile</option>
                            <option value="122">Spanish - Colombia</option>
                            <option value="123">Spanish - Costa Rica</option>
                            <option value="124">Spanish - Dominican Republic</option>
                            <option value="125">Spanish - Ecuador</option>
                            <option value="126">Spanish - El Salvador</option>
                            <option value="127">Spanish - Guatemala</option>
                            <option value="128">Spanish - Honduras</option>
                            <option value="129">Spanish - Mexico</option>
                            <option value="130">Spanish - Nicaragua</option>
                            <option value="131">Spanish - Panama</option>
                            <option value="132">Spanish - Paraguay</option>
                            <option value="133">Spanish - Peru</option>
                            <option value="134">Spanish - Puerto Rico</option>
                            <option value="135">Spanish - Spain (Traditional)</option>
                            <option value="136">Spanish - Uruguay</option>
                            <option value="137">Spanish - Venezuela</option>
                            <option value="138">Swahili</option>
                            <option value="139">Swedish - Finland</option>
                            <option value="140">Swedish - Sweden</option>
                            <option value="171">Tagalog</option>
                            <option value="141">Tajik</option>
                            <option value="142">Tamil</option>
                            <option value="143">Tatar</option>
                            <option value="144">Telugu</option>
                            <option value="145">Thai</option>
                            <option value="146">Tibetan</option>
                            <option value="147">Tsonga</option>
                            <option value="148">Turkish</option>
                            <option value="149">Turkmen</option>
                            <option value="150">Ukrainian</option>
                            <option value="151">Urdu</option>
                            <option value="152">Uzbek - Cyrillic</option>
                            <option value="153">Vietnamese</option>
                            <option value="154">Welsh</option>
                            <option value="155">Xhosa</option>
                            <option value="156">Yiddish</option>
                            <option value="169">Yoruba</option>
                            <option value="157">Zulu</option>
                        </optgroup>
                    </select>

                    <h4 class="edit_text" id="text42"><?=$templates->getData('text42')?></h4>

                    <select name="ffrom_lang" type="text" value="" required class="form-control">
                        <option value="0">Destination Language</option>
                        <optgroup label="Popular Languages">
                            <option value="159">Arabic</option>
                            <option value="901">Chinese</option>
                            <option value="902">English</option>
                            <option value="903">French</option>
                            <option value="904">German</option>
                            <option value="74">Hebrew</option>
                            <option value="79">Italian</option>
                            <option value="81">Japanese</option>
                            <option value="108">Russian</option>
                            <option value="905">Spanish</option>
                        </optgroup>
                        <optgroup label="Additional Languages">
                            <option value="1">Afrikaans</option>
                            <option value="2">Albanian</option>
                            <option value="3">Amharic</option>
                            <option value="159">Arabic</option>
                            <option value="4">Arabic - Algeria</option>
                            <option value="5">Arabic - Bahrain</option>
                            <option value="6">Arabic - Egypt</option>
                            <option value="7">Arabic - Iraq</option>
                            <option value="8">Arabic - Jordan</option>
                            <option value="9">Arabic - Kuwait</option>
                            <option value="10">Arabic - Lebanon</option>
                            <option value="11">Arabic - Libya</option>
                            <option value="12">Arabic - Morocco</option>
                            <option value="13">Arabic - Oman</option>
                            <option value="14">Arabic - Qatar</option>
                            <option value="15">Arabic - Saudi Arabia</option>
                            <option value="16">Arabic - Syria</option>
                            <option value="17">Arabic - Tunisia</option>
                            <option value="18">Arabic - United Arab Emirates</option>
                            <option value="19">Arabic - Yemen</option>
                            <option value="20">Armenian</option>
                            <option value="21">Assamese</option>
                            <option value="22">Azeri - Cyrillic (Arzebaijan)</option>
                            <option value="23">Basque</option>
                            <option value="24">Belarusian</option>
                            <option value="25">Bengali</option>
                            <option value="26">Bosnian</option>
                            <option value="27">Bulgarian</option>
                            <option value="28">Burmese</option>
                            <option value="29">Catalan</option>
                            <option value="901">Chinese</option>
                            <option value="30">Chinese - China</option>
                            <option value="31">Chinese - Hong Kong SAR</option>
                            <option value="32">Chinese - Macau SAR</option>
                            <option value="33">Chinese - Singapore</option>
                            <option value="34">Chinese - Taiwan</option>
                            <option value="35">Croatian</option>
                            <option value="36">Czech</option>
                            <option value="37">Danish</option>
                            <option value="908">Dari</option>
                            <option value="38">Divehi - Dhivehi - Maldivian</option>
                            <option value="39">Dutch - Belgium</option>
                            <option value="40">Dutch - Netherlands</option>
                            <option value="902">English</option>
                            <option value="41">English - Australia</option>
                            <option value="42">English - Belize</option>
                            <option value="43">English - Canada</option>
                            <option value="44">English - Caribbean</option>
                            <option value="45">English - Great Britain</option>
                            <option value="46">English - India</option>
                            <option value="47">English - Ireland</option>
                            <option value="48">English - Jamaica</option>
                            <option value="49">English - New Zealand</option>
                            <option value="50">English - Phillippines</option>
                            <option value="51">English - Southern Africa</option>
                            <option value="52">English - Trinidad</option>
                            <option value="53">English - United States</option>
                            <option value="54">Estonian</option>
                            <option value="55">Faroese</option>
                            <option value="56">Farsi - Persian</option>
                            <option value="57">Finnish</option>
                            <option value="903">French</option>
                            <option value="58">French - Belgium</option>
                            <option value="59">French - Canada</option>
                            <option value="60">French - France</option>
                            <option value="61">French - Luxembourg</option>
                            <option value="62">French - Switzerland</option>
                            <option value="64">Gaelic - Ireland</option>
                            <option value="65">Gaelic - Scotland</option>
                            <option value="170">Georgian</option>
                            <option value="904">German</option>
                            <option value="66">German - Austria</option>
                            <option value="67">German - Germany</option>
                            <option value="68">German - Liechtenstein</option>
                            <option value="69">German - Luxembourg</option>
                            <option value="70">German - Switzerland</option>
                            <option value="71">Greek</option>
                            <option value="72">Guarani - Paraguay</option>
                            <option value="73">Gujarati</option>
                            <option value="173">Hausa</option>
                            <option value="74">Hebrew</option>
                            <option value="75">Hindi</option>
                            <option value="76">Hungarian</option>
                            <option value="77">Icelandic</option>
                            <option value="172">Igbo</option>
                            <option value="78">Indonesian</option>
                            <option value="79">Italian - Italy</option>
                            <option value="80">Italian - Switzerland</option>
                            <option value="81">Japanese</option>
                            <option value="82">Kannada</option>
                            <option value="83">Kashmiri</option>
                            <option value="84">Kazakh</option>
                            <option value="85">Khmer</option>
                            <option value="86">Korean</option>
                            <option value="158">Kurdish</option>
                            <option value="87">Lao</option>
                            <option value="88">Latin</option>
                            <option value="89">Latvian</option>
                            <option value="90">Lithuanian</option>
                            <option value="63">Macedonian</option>
                            <option value="91">Malay - Brunei</option>
                            <option value="92">Malay - Malaysia</option>
                            <option value="93">Malayalam</option>
                            <option value="94">Maltese</option>
                            <option value="95">Maori</option>
                            <option value="96">Marathi</option>
                            <option value="97">Mongolian</option>
                            <option value="98">Nepali</option>
                            <option value="99">Norwegian - Bokml</option>
                            <option value="100">Oriya</option>
                            <option value="907">Pashto</option>
                            <option value="101">Polish</option>
                            <option value="102">Portuguese - Brazil</option>
                            <option value="103">Portuguese - Portugal</option>
                            <option value="104">Punjabi</option>
                            <option value="105">Raeto-Romance</option>
                            <option value="106">Romanian - Moldova</option>
                            <option value="107">Romanian - Romania</option>
                            <option value="108">Russian</option>
                            <option value="906">Samoan</option>
                            <option value="110">Sanskrit</option>
                            <option value="111">Serbian - Cyrillic</option>
                            <option value="112">Setsuana</option>
                            <option value="113">Sindhi</option>
                            <option value="114">Sinhala; Sinhalese</option>
                            <option value="115">Slovak</option>
                            <option value="116">Slovenian</option>
                            <option value="117">Somali</option>
                            <option value="118">Sorbian</option>
                            <option value="905">Spanish</option>
                            <option value="119">Spanish - Argentina</option>
                            <option value="120">Spanish - Bolivia</option>
                            <option value="121">Spanish - Chile</option>
                            <option value="122">Spanish - Colombia</option>
                            <option value="123">Spanish - Costa Rica</option>
                            <option value="124">Spanish - Dominican Republic</option>
                            <option value="125">Spanish - Ecuador</option>
                            <option value="126">Spanish - El Salvador</option>
                            <option value="127">Spanish - Guatemala</option>
                            <option value="128">Spanish - Honduras</option>
                            <option value="129">Spanish - Mexico</option>
                            <option value="130">Spanish - Nicaragua</option>
                            <option value="131">Spanish - Panama</option>
                            <option value="132">Spanish - Paraguay</option>
                            <option value="133">Spanish - Peru</option>
                            <option value="134">Spanish - Puerto Rico</option>
                            <option value="135">Spanish - Spain (Traditional)</option>
                            <option value="136">Spanish - Uruguay</option>
                            <option value="137">Spanish - Venezuela</option>
                            <option value="138">Swahili</option>
                            <option value="139">Swedish - Finland</option>
                            <option value="140">Swedish - Sweden</option>
                            <option value="171">Tagalog</option>
                            <option value="141">Tajik</option>
                            <option value="142">Tamil</option>
                            <option value="143">Tatar</option>
                            <option value="144">Telugu</option>
                            <option value="145">Thai</option>
                            <option value="146">Tibetan</option>
                            <option value="147">Tsonga</option>
                            <option value="148">Turkish</option>
                            <option value="149">Turkmen</option>
                            <option value="150">Ukrainian</option>
                            <option value="151">Urdu</option>
                            <option value="152">Uzbek - Cyrillic</option>
                            <option value="153">Vietnamese</option>
                            <option value="154">Welsh</option>
                            <option value="155">Xhosa</option>
                            <option value="156">Yiddish</option>
                            <option value="169">Yoruba</option>
                            <option value="157">Zulu</option>
                        </optgroup>
                    </select>

                    <h4 class="edit_text" id="text43"><?=$templates->getData('text43')?></h4>

                    <select name="fexpertise" type="text" value="" required class="form-control">
                        <option value="0">Field of expertise</option>
                        <option value="10">Academy, Science</option>
                        <option value="11">All students needs</option>
                        <option value="19">Art, Music</option>
                        <option value="17">Aviation, cars, Maritime</option>
                        <option value="13">Certificate, Cv's</option>
                        <option value="21">Electronics, Computers</option>
                        <option value="22">Energy Gas, Oil</option>
                        <option value="9">Engineering, Construction, Manufacturing, Architecture</option>
                        <option value="12">Finance, Accounting, Currency</option>
                        <option value="8">Games</option>
                        <option value="6">General</option>
                        <option value="23">Insurance</option>
                        <option value="24">Legal Documents</option>
                        <option value="20">Literature TV, Cinema</option>
                        <option value="15">Marketing, Advertising, Business, E-Commerce</option>
                        <option value="14">Medical, Pharmaceutical</option>
                        <option value="18">Military</option>
                        <option value="16">Software, Internet, Mobile application</option>
                        <option value="25">Telecommunication</option>
                        <option value="7">Tourism</option>
                    </select>

                    <div id="upload" style="margin-top:40px;">
                        <input id="input-ficons-1" name="inputficons1[]" multiple type="file" class="file-loading">
                        <script>
                            $("#input-ficons-1").fileinput({
        uploadUrl: "/uploads",
        uploadAsync: true,
        previewFileIcon: '<i class="fa fa-file"></i>',
        allowedPreviewTypes: null , // set to empty, null or false to disable preview for all types
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        }
    });
                        </script>
                        <textarea class="form-control" rows="2" style="resize:none; margin-top:30px;" placeholder="Add notes to the translator (optional)"></textarea>
                        <br/>

                        <input type="checkbox" id="blankCheckbox" value="option1"> I agree to the <a href="">Terms and Conditions</a> and <a href="">Privacy Policy</a>
                        <br>
                        </br>

                        <button id="btn1" type="submit" class="btn btn-success edit_button" style="width:100%;<?=$templates->getData('btn1_style')?>"><?=$templates->getData('btn1')?></button>
                    </div>
                </div>



            </div>
        </div>


    </section>




    <section id="footer" class="change_bg" style="<?=$templates->getData('footer')?>">
        <div class="footer_top" style="max-width: 1190px; margin: auto;">
            <div class="container">
                <div class="row">
                    <div id="block1" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="margin-top:50px;" class="title-widget edit_text" id="text37"><?=$templates->getData('text37')?></h1>

                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text22"><?=$templates->getData('text22')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text23"><?=$templates->getData('text23')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text24"><?=$templates->getData('text24')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text25"><?=$templates->getData('text25')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <!-- widgets column left end -->
                     <div id="block2" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="margin-top:50px;" class="title-widget edit_text" id="text38"><?=$templates->getData('text38')?></h1>

                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text26"><?=$templates->getData('text26')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text27"><?=$templates->getData('text27')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text28"><?=$templates->getData('text28')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text29"><?=$templates->getData('text29')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <!-- widgets column left end -->
                     <div id="block3" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="margin-top:50px;" class="title-widget edit_text" id="text39"><?=$templates->getData('text39')?></h1>

                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text30"><?=$templates->getData('text30')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text31"><?=$templates->getData('text31')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i> <span  class="edit_text" id="text32"><?=$templates->getData('text32')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <!-- widgets column left end -->

                  <div id="block4" class="col-lg-3 col-md-3 edit_block">
                        <!-- widgets column left -->
                        <ul class="list-unstyled clear-margins">
                            <!-- widgets -->

                            <li class="widget-container widget_nav_menu">
                                <!-- widgets list -->

                                <h1 style="margin-top:50px;" class="title-widget edit_text" id="text40"><?=$templates->getData('text40')?></h1>

                                <ul>
                                    <li><a href="#"  data-toggle="modal" data-target="#myModal"><i class="fa fa-file"></i> <span  class="edit_text" id="text33"><?=$templates->getData('text33')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-play"></i> <span  class="edit_text" id="text34"><?=$templates->getData('text34')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-comment"></i> <span  class="edit_text" id="text35"><?=$templates->getData('text35')?></span></a></li>
                                    <li><a href="#"><i class="fa fa-pencil"></i> <span  class="edit_text" id="text36"><?=$templates->getData('text36')?></span></a></li>
                                </ul>

                            </li>

                        </ul>


                    </div>
                    <!-- widgets column left end -->
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                    
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Supported File Formats</h4>
                          </div>
                          <div class="modal-body">
                            <p><span style="color:#57bddb; font-weight:700;">Text Files:</span> *.txt, *.doc, *.docx, *.pdf, *.xls, *.xlsx, *.ppt, *.pptx</p>
                            <p><span style="color:#57bddb; font-weight:700;">Images Files:</span> *.jpg, *.png, *.gif, *.tif, *.tiff</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                    
                      </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <footer id="bg23" class="change_bg" style="margin-top:10px;<?=$templates->getData('bg23')?>">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="footer-copyright">
                        <p>
                            <a href=""><img src="<?=$tempPath?>/img/logoB&W.png" height="17px;"></a> &copy; 2015 | <a href="">Terms & Conditions</a>                            | <a href="">Privacy Policy</a></p>
                    </div>
                </div>
                <!--copyright col end-->

                <div class="col-md-3 col-sm-3">
                    <div class="footer_mid pull-right">
                        <ul class="social-contact list-inline">
                            <?php if($templates->chekData('soc1')) { ?>
                            <li id="soc1"> 
                                <a id="soc1_link" data-id="soc1" href="<?=$templates->getData('soc1_link')?>" class="edit_soc"><i class="fa fa-facebook"></i></a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc2')) { ?>
                            <li id="soc2"> 
                                <a id="soc2_link" data-id="soc2" href="<?=$templates->getData('soc2_link')?>" class="edit_soc"><i class="fa fa-twitter"></i></a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc3')) { ?>
                            <li id="soc3"> 
                                <a id="soc3_link" data-id="soc3" href="<?=$templates->getData('soc3_link')?>" class="edit_soc"><i class="fa fa-google-plus"></i> </a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc4')) { ?>
                            <li id="soc4">
                                <a id="soc4_link" data-id="soc4" href="<?=$templates->getData('soc4_link')?>" class="edit_soc"> <i class="fa fa-linkedin"></i></a>
                            </li>
                            <?php } ?>
                            <?php if($templates->chekData('soc5')) { ?>
                            <li id="soc5">
                                <a id="soc5_link" data-id="soc5" href="<?=$templates->getData('soc5_link')?>" class="edit_soc"> <i class="fa fa-pinterest"></i></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!--blogname col end-->
            </div>
        </div>
    </footer>

    <!--scripts and plugins -->
    <!--must need plugin jquery-->
    <script src="<?=$tempPath?>/js/jquery.min.js"></script>
    <!--bootstrap js plugin-->
    <script src="<?=$tempPath?>/includes/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Google Maps API -->
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!-- Custom Google Map -->
    <script type="text/javascript">
       /* var maps;
			var geocoder;

			function init() {
				maps = [];
				geocoder = new google.maps.Geocoder();

				var styles = {
				  'Red': [
					{
					  featureType: 'all',
					  stylers: [{saturation:-100},{gamma:0.50}]
					}
				  ]
				};

				for (var s in styles) {
				  var opt = {
					mapTypeControlOptions: {
					  mapTypeIds: [s]
					},
					disableDefaultUI: true,
					navigationControl: true,
					scrollwheel: false,
					center: new google.maps.LatLng(42.415109, -96.842471),
					zoom: 8,
					mapTypeId: s
				  };

				  var div = document.getElementById('map');
				  var map = new google.maps.Map(div, opt);
				  var styledMapType = new google.maps.StyledMapType(styles[s], {name: s});
				  map.mapTypes.set(s, styledMapType);
				  
				  var goldStar = '<?=$tempPath?>/img/marker.png';

				  var marker = new google.maps.Marker({
					position: map.getCenter(),
					icon: goldStar,
					map: map
				  });				  
				}
			}*/
    </script>
    <!--easying scroll effect-->
    <script src="<?=$tempPath?>/js/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?=$tempPath?>/js/jquery.sticky.js" type="text/javascript"></script>
    <script src="<?=$tempPath?>/js/jquery.nav.js" type="text/javascript"></script>
    <!--owl carousel-->
    <script src="<?=$tempPath?>/includes/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
    <!--wow animation-->
    <script src="<?=$tempPath?>/includes/wow-js/wow.min.js" type="text/javascript"></script>

    <!--Activating WOW Animation only for modern browser-->
    <!--[if !IE]><!-->
    <script type="text/javascript">
        new WOW().init();
    </script>
    <!--<![endif]-->

    <!--Oh Yes, IE 9+ Supports animation, lets activate for IE 9+-->
    <!--[if gte IE 9]>
			<script type="text/javascript">new WOW().init();</script>
		<![endif]-->

    <!--Opacity & Other IE fix for older browser-->
    <!--[if lte IE 8]>
			<script type="text/javascript" src="js/ie-opacity-polyfill.js"></script>
		<![endif]-->


    <!--parallax-->
    <script src="<?=$tempPath?>/js/jquery.scrolly.js" type="text/javascript"></script>
    <!--portfolio-filter-->
    <script src="<?=$tempPath?>/js/isotope.pkgd.min.js" type="text/javascript"></script>
    <!--lightbox-->
    <script src="<?=$tempPath?>/includes/litebox/images-loaded.min.js" type="text/javascript"></script>
    <script src="<?=$tempPath?>/includes/litebox/litebox.min.js" type="text/javascript"></script>
    <!--project count-->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
    <script src="<?=$tempPath?>/js/jquery.counterup.min.js" type="text/javascript"></script>
    <!--custom scrollbar-->
    <script src="<?=$tempPath?>/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <!--Contact Form-->

    <!--customizable plugin edit according to your needs-->
    <script src="<?=$tempPath?>/js/custom.js" type="text/javascript"></script>
    
    <?php
    if($templates->edit())
    {
      $editColorTheme = false;
      $patch_edit = $path = $_SERVER['DOCUMENT_ROOT'] . '/templates/edit_temp/edit.php';
      include $patch_edit;
    }
  ?>
    
</body>

</html>