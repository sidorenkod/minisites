 $(document).ready(function()
{
              $('.login').click(function () {
              $( ".register-table" ).hide();
              $( ".login-table" ).show();
              });     
     
             $('.register').click(function () {
              $( ".login-table" ).hide();
               $( ".register-table" ).show();
              });
   
  $('.chek_field').focus(function(){
    var id = $(this).data('id');
    $('#'+id+' .error_mess').remove();
  }); 
     
  $('#form_agensy').submit(function(){
    
    var error = false;
    var textError = '';
    var re = /^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,6}$/i;
  
    $('.chek_field').each(function(){
      
      var this_error = false;
      
      var id = $(this).data('id');
      
      var text = $(this).val();
      
      var min = Number($(this).data('min'));
      var max = Number($(this).data('max'));
      
      var textLen = Number(text.length);
      
      if(text == '' && $(this).data('check') == 'required')
      {
        this_error = true;
        
        textError = 'Fill in the field';
      }
      else if($(this).data('type') == 'email')
      {
        if(!re.test(text))
        {
          this_error = true;
        
          textError = 'Wrong email format. <br>Example: email@gmail.com';
        }
      }
      else if(min > textLen && text != '')
      {
        this_error = true;
        
        textError = 'Minimum length of - '+$(this).data('min')+' characters. Now - '+$(this).val().length+' characters';
      }
      else if(max < textLen && text != '')
      {
        this_error = true;
        
        textError = 'Maximum length of - '+$(this).data('max')+' characters. Now - '+$(this).val().length+' characters';
      }
      
      if(this_error)
      {
        var elem = '<span class="error_mess">'+textError+'</span>';
      
        $('#'+id).append(elem);
        
        error = true;
      }
    });
    
    if(error == true)
    {
      $('body').animate({scrollTop: 0},500);
      return false;
    }
  
});

$('#fileexcel').change(function(){
        if($(this).val() != '') {
            $('.labelblock').text($(this).val().replace(/.+[\\\/]/, ""));
        }
        else
        {
            $('.labelblock').text($('.text_label').text());
        }
    });
     
});