 $(document).ready(function()
{
              $('.agency-btn').click(function () {
              $( ".my-agency" ).show();
              $( ".my-plan" ).hide();
              $( ".my-website" ).hide();
              $( ".order-plan" ).hide();
               $( ".my-website-step-2" ).hide()
              $( ".basic-plan" ).hide();
              $( ".welcome" ).hide();
              $( ".account" ).hide();
              });
              
              $('.plan-btn').click(function () {
              $( ".my-plan" ).show();
              $( ".my-agency" ).hide();
              $( ".my-website" ).hide();
              $( ".order-plan" ).hide();
              $( ".my-website-step-2" ).hide()
              $( ".basic-plan" ).hide();
              $( ".welcome" ).hide();
              $( ".account" ).hide();

              }); 
              
              $('.website-btn').click(function () {
              $( ".my-website" ).show();
              $( ".my-agency" ).hide();
              $( ".my-plan" ).hide();
              $( ".order-plan" ).hide();
              $( ".my-website-step-2" ).hide()
              $( ".account" ).hide();
              $( ".basic-plan" ).hide();
                                $( ".welcome" ).hide();

              });    
             
             $('.my-domain').click(function () {
              $( ".my-website" ).hide();
              $( ".my-agency" ).hide();
              $( ".my-plan" ).hide();
              $( ".order-plan" ).show();
              $( ".my-website-step-2" ).hide();
              $( ".basic-plan" ).hide();
             $( ".welcome" ).hide();
              $( ".account" ).hide();



              });       
        
            $('.send-plan').click(function () {
              $( ".my-website" ).hide();
              $( ".my-agency" ).hide();
              $( ".my-plan" ).hide();
              $( ".basic-plan" ).show();
              $( ".order-plan" ).hide();
              $( ".my-website-step-2" ).hide();
              $( ".welcome" ).hide();
              $( ".account" ).hide();

              });                
              
             $('.close-form').click(function () {
              $( ".my-website" ).hide();
              $( ".my-agency" ).hide();
              $( ".my-plan" ).hide();
              $( ".basic-plan" ).hide();
              $( ".order-plan" ).hide();
              $( ".my-website-step-2" ).hide();
              $( ".account" ).hide();
              $( ".welcome" ).show();
              });                
              
              $('.btn-account').click(function () {
              $( ".my-website" ).hide();
              $( ".my-agency" ).hide();
              $( ".my-plan" ).hide();
              $( ".basic-plan" ).hide();
              $( ".order-plan" ).hide();
              $( ".my-website-step-2" ).hide();
              $( ".account" ).show();
              $( ".welcome" ).hide();
              });                
     
       
              
      
     
});

$(function() {
  $("#uploadFile").on("change", function() {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    if (/^image/.test(files[0].type)) { // only image file
      var reader = new FileReader(); // instance of the FileReader
      reader.readAsDataURL(files[0]); // read the local file

      reader.onloadend = function() { // set image data as background of div
        $("#imagePreview").css("background-image", "url(" + this.result + ")");
         
      }
    }
  });
  $('#imagePreview').click(function() {
    $('#uploadFile').trigger('click');

  });
});




$(function(){
    $('#slider div:gt(0)').hide();
    setInterval(function(){
      $('#slider div:first-child').fadeOut(0)
         .next('div').fadeIn(3000)
         .end().appendTo('#slider');}, 6000);
});


/*Domains*/
 $(document).on('ready', function(){

        $("#type").on('change', function(){
            var type = $("#type option:selected").val();
            $(this).attr('disabled', true);
            $("#set_type").val(type); 

            if(type == "sub"){
                $("#cont_own").remove(); 
                $("#cont_rent").remove(); 
                $("#cont_sub").show();
            }else if(type =="own"){
                $("#cont_sub").remove(); 
                $("#cont_rent").remove(); 
                $("#cont_own").show();
            }else if(type =="rent"){
                $("#cont_sub").remove(); 
                $("#cont_own").remove(); 
                $("#cont_rent").show();
            }
        });

        $("#dom_rent").on('change', function(){
            var dom = $("#dom_rent option:selected").val();
            var domt = $("#dom_rent option:selected").text();

            $(".txt_email").each(function(){
                $(this).text('@'+domt);
            });
        });

        $("#bt_add_email").on('click', function(){
            var domt = $("#dom_rent option:selected").text();
            var html = "";
            html+= "<div class='row'>";
            html+= "    <div class='col-md-5'>";
            html+= "        <input type='text' name='ema_rent[]' required minlength='2' maxlength='50' placeholder='Example: thenameofmyagency' class='form-control'>";
            html+= "    </div>";
            html+= "    <div class='col-md-5'>";
            html+= "        <span class='txt_email'>@"+domt+"</span>";
            html+= "    </div>";
            html+= "    <div class='col-md-2'>";
            html+= "       <input type='button' class='btn btn-danger bt_remove' value='Remove'>";
            html+= "    </div>";
            html+= "</div><hr>";
            $("#cont_email_rented").prepend(html);
        });

        $(document).on('click', '.bt_remove', function(){
            $(this).parent().parent().remove();
        });
    });

/*end domains*/

/*Basic Plan*/

$(document).on('ready', function(){

        $("#bt_selec").on('click', function(){

            var len1 = $("#len1 option:selected").val().trim();
            var lent1 = $("#len1 option:selected").text().trim();

            if(len1 == ""){
                alert("You have to select one language.");
                return;
            }

            var len2 = $("#len2 option:selected").val().trim();
            var lent2 = $("#len2 option:selected").text().trim();

            if(len2 == ""){
                alert("You have to select one language.");
                return;
            }

            if(len1 == len2){
                alert("You have to choose different langueges.");
                return;
            }
            
            var html = "<tr class='row_selected'>";
            html+= "<td >" + lent1 + "</td>";
            html+= "<td style='text-align:center'><i class='fa fa-mail-forward'></i></td>";
            html+= "<td>" + lent2 + "</td>";
            html+= "<td><input type='button' class='btn btn-danger bt_delete' value=' X '>";
            html+= "<input type='hidden' class='id1' name='len1[]' value='"+len1+"'>";
            html+= "<input type='hidden' class='id2' name='len2[]' value='"+len2+"'></td>";
            html+= "</tr>";

            var total = $("#combinations").val();
            var n = $(".row_selected").length;

            if(n < total){
                var value = true;
                $(".row_selected").each(function(){
                    var id1 = $(this).find("input.id1").val();
                    var id2 = $(this).find("input.id2").val();
                    if(len1 == id1 && len2 == id2){
                        alert("This combination already exists.");
                        value = false;
                    }
                });
                if(value){
                    $("#txt").text((n+1)+"/"+total);
                    $("#combinations_selected").append(html);
                 }
            }else{
                alert("You have only "+total+" combinations.");
                return;
            }
        });

        $(document).on('click', '.bt_delete', function(){
            if(confirm('Are you sure?')){
                $(this).parent().parent().remove();
                var total = $("#combinations").val();
                var n = $(".row_selected").length;
                $("#txt").text(n+"/"+total);
            }
        });


        $("#bt_selec_expertise").on('click', function() {
            var exp = $("#expertise option:selected").val().trim();
            var expt = $("#expertise option:selected").text().trim();

            if(exp == ""){
                alert("You have to select one expertise.");
                return;
            }

            var html = "<tr class='row_selected_expertise' id='row_"+exp+"'>";
            html+= "<td >" + expt + "</td>";
            html+= "<td><input type='button' class='btn btn-danger bt_delete_expertise' value=' X '>";
            html+= "<input type='hidden' class='' name='expe[]' value='"+exp+"'>";
            html+= "</tr>";

            var total = $("#expertises").val();
            var n = $(".row_selected_expertise").length;

            if(n < total){
                if($("#row_"+exp).length == 1){
                    alert("This expertise already exists");
                    return;
                }
                $("#txt_expertises").text((n+1)+"/"+total);
                $("#expertises_selected").append(html);
            }else{
                alert("You have only "+total+" expertises.");
                return;
            }
        });

        $(document).on('click', '.bt_delete_expertise', function(){
            if(confirm('Are you sure?')){
                $(this).parent().parent().remove();
                var total = $("#expertises").val();
                var n = $(".row_selected_expertise").length;
                $("#txt_expertises").text(n+"/"+total);
            }
        });
    });
/*End basic plan*/



