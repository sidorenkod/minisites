<?php
class MyPlugin extends Zend_Controller_Plugin_Abstract
{
  public function preDispatch(Zend_Controller_Request_Abstract $request)
  {
    $request->setModuleName(strtolower($request->getModuleName()));
    $request->setControllerName(strtolower($request->getControllerName()));
    $request->setActionName(strtolower($request->getActionName()));
  }
}

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initDb()
    {
      $resource = $this->getPluginResource('db');
      $db = $resource->getDbAdapter();
      Zend_Db_Table_Abstract::setDefaultAdapter($db);
      return $db;
    }
    
    protected function _initView()
    {
      $view = new Zend_View();
      $auto = Zend_Loader_Autoloader::getInstance();
      $auto->registerNamespace("models_");
      //$auto->registerNamespace("models_DBTable_");
  
      $front = Zend_Controller_Front::getInstance();
      $front->registerPlugin(new MyPlugin());
  
      return $view;
    }

   /* protected function _initFrontControllerOutput() {

        $this->bootstrap('FrontController');
        $frontController = $this->getResource('FrontController');

        $response = new Zend_Controller_Response_Http;
        $response->setHeader('Content-Type', 'text/html; charset=UTF-8', true);
        $response->setHeader('Accept-Encoding', 'gzip, deflate', true);
        $response->setHeader('X-Compression', 'gzip');
        //$response->setHeader('Content-Encoding', 'gzip');
        $response->setHeader('Content-type', 'text/html');
        $response->setHeader('Cache-Control', 'must-revalidate');
        $frontController->setResponse($response);

        $this->_frontController = $frontController;

        return $frontController;

    }*/

	protected function _initViewHelpers() {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();
        $view->setEncoding('UTF-8');

        $view->headTitle()->setSeparator(' » ');
	}
	public function _initAuth(){
	    $auth = Zend_Auth::getInstance();
	    $data = $auth->getStorage()->read();
	}

    protected function _initLayoutHelper() {

        Zend_Controller_Action_HelperBroker::addPath(APPLICATION_PATH .'/controllers/helpers');
    }

    /*public function _initRoute()
    {

        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $dispatcher = $front->getDispatcher();

        $router = $front->getRouter();
        $router->removeDefaultRoutes();

        $defaultRoute = new Zend_Controller_Router_Route(
            ':controller/:action/*',
            array(
                'controller'    => 'index',
                'action'        => 'index'
            )
        );

        $enRoute = new Zend_Controller_Router_Route(
            'en',
            array(
                'controller'    => 'index',
                'action'        => 'index',
                'language'      => 'en'
            )
        );

        $heRoute = new Zend_Controller_Router_Route(
            'he',
            array(
                'controller'    => 'index',
                'action'        => 'index',
                'language'      => 'he'
            )
        );

        $router->addRoutes(array(
            'default'   => $defaultRoute,
            'en'        => $enRoute->chain($defaultRoute),
            'he'        => $heRoute->chain($defaultRoute)
        ));

        Zend_Controller_Front::getInstance()->registerPlugin(new Application_Plugin_PluginLanguage());
    }*/

}