<?php

class TranslatorController extends Zend_Controller_Action
{
  public function init()
  {
    $this->_helper->layout()->setLayout('translator');
    
    $this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    
    $auth = Zend_Auth::getInstance();
    
    if(!$auth->hasIdentity() || $auth->getIdentity()->role != 4)
    {
      $this->_helper->redirector('', '');
      exit;
    }
  }
  
  public function indexAction()
  { 
    $this->postAgensy();
    
    $objAgen = new models_Agencys();
    $this->view->agens = $objAgen->getAgencys();
    
    $objTrans = new models_Translator();
    $this->view->tran = $objTrans->getTrEmail(Zend_Auth::getInstance()->getIdentity()->email);
    
    $objCount = new models_Country();
    $this->view->counts = $objCount->getCountry();
  }
  
  public function logoutAction()
  {
    $this->_helper->layout->disableLayout();

    Zend_Auth::getInstance()->clearIdentity();

    unset($_SESSION);

    $this->_helper->redirector('', '');
  }
  
  private function postAgensy()
  {
    if($this->getRequest()->isPost())
    {
      if(isset($_POST['btg'])){
        $data['name_agen'] = $this->test_input($_POST['nam']);
        $data['addr_agen'] = $this->test_input($_POST['add']);
        $data['phon_agen'] = $this->test_input($_POST['ph1']);
        $data['pho2_agen'] = $this->test_input($_POST['ph2']);
        $data['id_scan_agen'] = $this->test_input($_POST['sca']); 
        $data['tran_cert_agen'] = $this->test_input($_POST['cer']); 
        $data['way_pay_agen'] = $this->test_input($_POST['pay']); 
        $data['email_pay_agen'] = $this->test_input($_POST['ema']);

        $error = false;
        /*if(empty($data['nam'])){
            $error = true;
            $errorName = 'Name is empty';
        }else if(empty($data['add'])){
            $error = true;
            $errorName = 'Address is empty';
        }else if(empty($ph1)){
            $error = true;
            $errorName = 'Phone 1 number is empty';
        }else if(empty($sca)){
            $error = true;
            $errorName = 'Scan id number is empty';
        }else if(empty($cer)){
            $error = true;
            $errorName = 'Certificate is empty';
        }else if(empty($pay)){
            $error = true;
            $errorName = 'Way to payment is empty';
        }else if(empty($ema)){
            $error = true;
            $errorName = 'Email is empty';
        }*/
        if(!$error){
          
          $data['stat_agen'] = 'ACTIVATED';
          $data['email_r'] = Zend_Auth::getInstance()->getIdentity()->email;
          
          $objAgen = new models_Agencys();
          $objAgen->insertAgen($data);
        }

      }else if(isset($_POST['btc'])){
        //$data['agency_id'] = $orm->test_input($_POST['ida']);
        $data['name_agen'] = $this->test_input($_POST['nam']);
        $data['addr_agen'] = $this->test_input($_POST['add']);
        $data['phon_agen'] = $this->test_input($_POST['ph1']);
        $data['pho2_agen'] = $this->test_input($_POST['ph2']);
        $data['id_scan_agen'] = $this->test_input($_POST['sca']); 
        $data['tran_cert_agen'] = $this->test_input($_POST['cer']); 
        $data['way_pay_agen'] = $this->test_input($_POST['pay']); 
        $data['email_pay_agen'] = $this->test_input($_POST['ema']);

        $error = false;
        /*if(empty($nam)){
            $error = true;
            $errorName = 'Name is empty';
        }else if(empty($add)){
            $error = true;
            $errorName = 'Address is empty';
        }else if(empty($ph1)){
            $error = true;
            $errorName = 'Phone 1 number is empty';
        }else if(empty($sca)){
            $error = true;
            $errorName = 'Scan id number is empty';
        }else if(empty($cer)){
            $error = true;
            $errorName = 'Certificate is empty';
        }else if(empty($pay)){
            $error = true;
            $errorName = 'Way to payment is empty';
        }else if(empty($ema)){
            $error = true;
            $errorName = 'Email is empty';
        }*/
        if($error){
            //header("location: index_translator.php?op=myagency&rsp=r_ed_myagency&msj=$errorName");
        }
        else
        {
          $email_r = Zend_Auth::getInstance()->getIdentity()->email;
          
          $objAgen = new models_Agencys();
          $objAgen->updateAgen($email_r, $data);
        }

      }
      
      $data = array('country_id'=>(int)$_POST['country_id']);
      
      $objTran = new models_Translator();
      $objTran->updateTrans(Zend_Auth::getInstance()->getIdentity()->id, $data);
    }
  }
  
  public function uploadFaviconAction()
  {
    if(!empty($_FILES))
    {
      //print_r($_FILES);exit;
      $tempFile = $_FILES['file']['tmp_name'];
      
      $user_id = Zend_Auth::getInstance()->getIdentity()->id;
      
      $urlp = '/templates/templates_users/'.$user_id;
      
      $storeFolder = realpath(APPLICATION_PATH . "/../public/").$urlp;

      if (!is_dir($storeFolder)){ mkdir($storeFolder,0777); }
      
      //print_r($storeFolder);exit;
      $file_ext = trim(strtolower(strrchr($_FILES['file']['name'], '.')),'.');
      
      $file_name = 'favicon.'.$file_ext;
      
      $pathFile = $storeFolder .'/'.$file_name;
      //echo $pathFile;exit;
      if (move_uploaded_file($tempFile, $pathFile))
      {
        echo json_encode(array('result'=>true));
      }
    }
    exit;
  }
  
  public function uploadLogoAction()
  {
    if(!empty($_FILES))
    {
      //print_r($_FILES);exit;
      $tempFile = $_FILES['file']['tmp_name'];
      
      $user_id = Zend_Auth::getInstance()->getIdentity()->id;
      
      $urlp = '/templates/templates_users/'.$user_id;
      
      $storeFolder = realpath(APPLICATION_PATH . "/../public/").$urlp;

      if (!is_dir($storeFolder)){ mkdir($storeFolder,0777); }
      
      //print_r($storeFolder);exit;
      $file_ext = trim(strtolower(strrchr($_FILES['file']['name'], '.')),'.');
      
      $file_name = 'logo.'.$file_ext;
      
      $pathFile = $storeFolder .'/'.$file_name;
      //echo $pathFile;exit;
      if (move_uploaded_file($tempFile, $pathFile))
      {
        $trans = new models_Translator();
        
        $transData = $trans->getTrEmail(null,$user_id);
        
        $json = json_decode($transData['meta_data'], true);
        
        $json['logo'] = $urlp.'/'.$file_name;
        
        $trans->updateTrans($user_id, array('logo'=>$file_name, 'meta_data'=>json_encode($json)));
        
        /*$templates = new models_Templates();
        
        $templs = $templates->getTemplate($user_id,null,true);
        
        foreach ($templs as $temp)
        {
          $jsonData = json_decode($temp['json_data'],true);
          
          $jsonData['logo'] = $urlp.'/'.$file_name;
          
          $templates->updateTempl($user_id,array('json_data'=>json_encode($jsonData)),$temp['templ_id']);
        }*/
      }
    }
    exit;
  }
  
  public function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }
  
  public function myplanAction()
  {
    
    if(isset($_POST['btgp'])){

        $idp = $_POST['idp'];

        $error = false;
        if(empty($idp)){
            $error = true;
            $errorName = 'The plan id is empty';
        }

        if($error){
            header("location: /translator/myplan?step=1&rsp=err_show_plan&msj=$errorName");
        }

        $sql = "SELECT * FROM plans WHERE plan_id='$idp';";

        //$r1 = $orm->consultaPersonalizada($sql);
        
        $model = new models_Model();
        $row = $model->sqlQuery2($sql, true);

        if($row){
            $email = Zend_Auth::getInstance()->getIdentity()->email;
            $sql  = "INSERT INTO order_plans VALUES(null, '$email', '".$row['plan_id']."', '".$row['name_plan']."', '".$row['quan_plan']."', '".$row['price_plan']."', '".$row['expe_plan']."', '".date('Y-m-d H:i:s')."');";
            $model->sqlQuery($sql, 'order_plans');
            $r2 = $model->getAdapter()->lastInsertId('order_plans');
            
            //var_dump($r2);exit;
            //$r2 = $orm->insertarPersonalizado($sql);

            if($r2){
                $len1 = $_POST['len1'];
                $len2 = $_POST['len2'];
                $pric = $_POST['pric'];
                
                $expe = $_POST['expe'];

                $idi = $r2; 
                $nl = 0;

                if(count($len1) > $row['quan_plan']){
                    $nl = $row['quan_plan'];
                }else{
                    $nl = count($len1);
                }

                $ne = 0;
                if(count($expe) > $row['expe_plan']){
                    $ne = $row['expe_plan'];
                }else{
                    $ne = count($expe);

                }

                for($i = 0; $i < $nl; $i++){
                    $sql = "INSERT INTO details_language_order_plan VALUES(null, '$idi', '".$len1[$i]."', '".$len2[$i]."', '".$pric[$i]."');";
                    $model->sqlQuery($sql, 'details_language_order_plan');
                    $r3 = $model->getAdapter()->lastInsertId('details_language_order_plan');
                    //$r3  = $orm->insertarPersonalizado($sql);
                }

                for($i = 0; $i < $ne; $i++){
                    $sql = "INSERT INTO details_expertise_order_plan VALUES(null, '$idi', '".$expe[$i]."')";
                    $model->sqlQuery($sql, 'details_expertise_order_plan');
                    $r3 = $model->getAdapter()->lastInsertId('details_language_order_plan');
                    //$r3  = $orm->insertarPersonalizado($sql);
                }
                
                //exit('work');

                header("location: /translator/myplan?step=1&rsp=re_ok_plan");
            }else{
                header("location: /translator/myplan?step=1&rsp=email_exists");
            }
        }else{
            header("location: /translator/myplan?step=1&rsp=no_exi_plan_id");
        }


    }
    
    if(isset($_POST['step1'])){
        $idp = $_POST['idp'];

        $error = false;
        if(empty($idp)){
            $error = true;
            $errorName = 'The plan id is empty';
        }

        if($error){
            header("location: /translator/myplan?step=1&rsp=err_show_plan&msj=$errorName");
        }

        //$sql = "SELECT * FROM plans WHERE plan_id='$idp';";
        $model = new models_Model();
        $f = $model->sqlQuery2("SELECT * FROM plans WHERE plan_id='$idp';", true);

        //$r = $orm->consultaPersonalizada($sql);

        if($f){
            $this->view->PLAN = $f;
        }else{
            header("location: /translator/myplan?rsp=no_exi_plan_id");
        }
    }
    
    $objPlans = new models_Plans();
    $this->view->PLANS = $objPlans->getPlans();
    
    $objOrPlans = new models_OrderPlans();
    $this->view->EXISTS = $objOrPlans->getPlans(Zend_Auth::getInstance()->getIdentity()->email);
    
    $objLang = new models_Languages();
    $this->view->langs = $objLang->getLang();
    
    $objExpert = new models_Expertises();
    $this->view->experts = $objExpert->getExpert();
    
    if($this->view->EXISTS)
    {
      $objModel = new models_Model();
      $this->view->order_plan_id = $objModel->getData($this->view->EXISTS['order_plan_id']);
      $this->view->order_plan_id2 = $objModel->getData2($this->view->EXISTS['order_plan_id']);
    }
    
  }
  
  public function mywebsiteAction()
  {
    $objTrans = new models_Translator();
    
    if(isset($_POST['save_lang']))
    {
      $data['site_lang_id'] = (int)$_POST['lang_id'];
      
      if($data['site_lang_id'] != 0)
      {
        $objTrans->updateTrans(Zend_Auth::getInstance()->getIdentity()->id, $data);
      }
    }
    
    $templates = new models_Templates();
    
    $this->view->temp_id = $templates->getTemId();
    
    $objLang = new models_Languages();
    $this->view->langs = $objLang->getLang2();
    $this->view->trans = $objTrans->getTrEmail(Zend_Auth::getInstance()->getIdentity()->email);
    
    $this->view->metaData = json_decode($this->view->trans['meta_data'],true);
    //print_r($this->view->metaData);exit;
  }
  
  public function saveLightboxAction()
  {
    if(!empty($_POST))
    {
      $objTrans = new models_Translator();
    
      $transData = $objTrans->getTrEmail(Zend_Auth::getInstance()->getIdentity()->email);
      
      $metaData = json_decode($transData['meta_data'],true);
      
      foreach($_POST as $k=>$v)
      {
        $metaData[$k] = $v;
      }
      
      $objTrans->updateTrans(Zend_Auth::getInstance()->getIdentity()->id, array('meta_data' => json_encode($metaData)));
    }
    
    header("location: /translator/mywebsite");
    exit;
  }
  
  public function lookSiteAction()
  {
    $templates = new models_Templates();
    
    $temp_id = (int)$_GET['temp_id'];
    
    if($temp_id == 0)
    {
      $temp_id = $templates->getTemId();
    }
    
    $this->view->templId = $templates->getTemId();
    
    $this->_helper->layout()->setLayout('templates/template-'.$temp_id);

    //exit;
  }
  
  public function orderdomainAction()
  {
    $this->postDomains();
    
    $objDom = new models_Domains();
    
    $this->view->domains = $objDom->getDomain();

    $email = Zend_Auth::getInstance()->getIdentity()->email;

    $objOrdDomains = new models_OrderDomains();

    $this->view->ORDER_DOMAINS = $objOrdDomains->getOrdDomain($email);
  }
  
  private function postDomains()
  {
    if(isset($_POST['btg'])){
      
      $email = Zend_Auth::getInstance()->getIdentity()->email;
        $type = $this->test_input($_POST['type']);
        //print_r($_POST);
        if($type == "sub"){
            $dom = $this->test_input($_POST['dom_sub']); 
            $ema = $this->test_input($_POST['ema_sub']);
            $error = false;
            if(empty($dom)){
                $error = true;
                $nameError = "The Subdomain was empty.";
            }else if(empty($ema)){
                $nameError = "The Email was empty.";
            }
            if($error){
                //header("location: index_translator.php?op=orderdomain&rsp=$nameError");
            }
            $id_dom = 'null';
        }else if($type == "own"){
            $dom = $this->test_input($_POST['dom_own']); 
            $ema = $this->test_input($_POST['ema_own']);
            $id_dom = 'null';
            $error = false;
            if(empty($dom)){
                $error = true;
                $nameError = "Your own domain was empty.";
            }else if(empty($ema)){
                $nameError = "The Email was empty.";
            }
            if($error){
                //header("location: index_translator.php?op=orderdomain&rsp=$nameError");
                header("location: /translator/orderdomain?op=orderdomain&rsp=$nameError");
            }
        }else if($type == "rent"){
            $dom = $this->test_input($_POST['dom_rent']); 
            $ema = 'null';
            $emac = $_POST['ema_rent'];
            $error = false;
            if(empty($dom)){
                $error = true;
                $nameError = "The Subdomain was empty.";
            }
            if($error){
                //header("location: index_translator.php?op=orderdomain&rsp=$nameError");
                header("location: /translator/orderdomain?rsp=$nameError");
            }
            
            $objDom = new models_Domains();
    
            $domain = $objDom->getDomain($dom);

            /*$sqlq = "SELECT * FROM domains WHERE domain_id='$dom' AND stat_dom='AVAILABLE';";
            $rq = $orm->consultaPersonalizada($sqlq);*/
            
            if(!empty($domain))
            {
              $id_dom = $domain['domain_id'];
              $dom = $domain['name_dom'];
            }else{
               header("location: /translator/orderdomain?rsp=domain_no_exists");
            }
            
        }
        
        $objModel = new models_Model();
        
        $date = date('Y-m-d');
        //$sql = "INSERT INTO order_domains VALUES(null, '$email', '$dom', '$date', '$type', $id_dom, $ema);";
        //$r1 = $orm->insertarPersonalizado($sql);
        
        //print_r($sql);exit;
        
        $data['email_r'] = $email;
        $data['domain'] = $dom;
        $data['date_order'] = $date;
        $data['type_order'] = $type;
        $data['domain_id_r'] = $id_dom;
        $data['email_order'] = $ema;
        
        $objOrDom = new models_OrderDomains();
        
        $ido = $objOrDom->insertOrDom($data);
        
        //$ido = $objModel->sqlQuery($sql, 'order_domains');
        
        if($ido)
        {
            //$ido = $r1->insert_id;

            if($type == "rent")
            {
                for($i = 0; $i < count($emac); $i++)
                {
                  $data2 = array();
                  $data2['order_domain_id'] = $ido;
                  $data2['email_order'] = $this->test_input($emac[$i]."@".$dom);
                  
                  $sql = "INSERT INTO emails_order_domain VALUES(null, '$ido', '".$this->test_input($emac[$i]."@".$dom)."');";
                  //print_r($sql);exit;
                  $objModel->sqlQuery($sql, 'emails_order_domain');
                  
                    
                    //$orm->insertarPersonalizado($sql);
                }
            }
            
            //header("location: index_translator.php?op=orderdomain&rsp=ok_re_order");
            header("location: /translator/orderdomain?rsp=ok_re_order");
        }else{
            //header("location: index_translator.php?op=orderdomain&rsp=no_re_order");
            header("location: /translator/orderdomain?rsp=no_re_order");
        }
    }
  }
  
  public function mydomainsAction()
  {
    $email = Zend_Auth::getInstance()->getIdentity()->email;
    
    $objOrdDomains = new models_OrderDomains();
    
    $this->view->ORDER_DOMAINS = $objOrdDomains->getOrdDomain($email);
    
    
  }
  
  public function changeTemplAction()
  {
    $templ_id = (int)$_POST['temp'];
    
    if($templ_id != 0)
    {
      $objTrans = new models_Translator();
      $objTrans->updateTrans(Zend_Auth::getInstance()->getIdentity()->id, array('templ_id'=>$templ_id));
    }

    exit;
  }
  
  /** method - change meta data */
  public function changeMetaAction()
  {
    $id = Zend_Auth::getInstance()->getIdentity()->id;
    
    $objTrans = new models_Translator();
    
    $transData = $objTrans->getTrEmail(null,$id);
    
    $meta = json_decode($transData['meta_data'],true);
    
    print_r($meta);
    
    foreach($_POST as $k=>$v)
    {
      $meta[$k]=$v;
    }
    
    $data['meta_data'] = json_encode($meta);
    
    $objTrans->updateTrans($id, $data);
    
    echo json_encode(array('result'=>true));
    
    exit;
  }
  
  public function savePostAction()
  {
    $templates = new models_Templates();
    $templates->savePost();
    exit;
  }
  
  public function uploadFileAction()
  {
    $templates = new models_Templates();
    $templates->uploadFile();
    exit;
  }
  
  public function resetPostAction()
  {
    $templates = new models_Templates();
    $templates->resetTemp();
    exit;
  }
  
  public function orderAction()
  {
    $objOrder = new models_Order();
    
    $id = (int)$this->getRequest()->getParam('id',0);
    
    $trans_id = Zend_Auth::getInstance()->getIdentity()->id;
    
    $this->view->orders = $objOrder->getOrder($id, $trans_id);
    
    if($id != 0)
    {
      $objFiles = new models_File();
      
      $this->view->files = $objFiles->getFiles($id);
      
      //print_r($id);exit;
      
      $this->view->trans_id = $trans_id;
      
      $this->_helper->viewRenderer('order-id');
    }
  }
  
  public function domainEmailEditAction()
  {
    $objCorpEmail = new models_EmailsOrderDomain();
    
    if(!empty($_POST))
    {
      $id = (int)$_POST['id'];
      $email = $_POST['email'];
      
      $objCorpEmail->updateCEmail($id, array('email_order'=>$email));
      header('Location: /translator/orderdomain'); exit;
    }
    
    $id = (int)$_GET['id'];
    
    
    $this->view->email = $objCorpEmail->getCorporateEmail($id);
  }
  
  public function emailAction()
  {
    $emailTrans = models_Model::getTransEmail();
    
    $objTrans = new models_Translator();
    
    $transData = $objTrans->getTrEmail($emailTrans);
    
    if(!empty($_FILES))
    {
      //print_r($_FILES);exit;
      $objCust = new models_Customers();
      
      $this->view->resultAdd = $objCust->getFileExel();
    }
    elseif(!empty($_POST))
    {
      $objCust = new models_Customers();
      
      $data = array('agency_id_r'=>$transData['id']);
      
      $data['name_cus'] = $_POST['name_cus'];
      $data['email_cus'] = $_POST['email_cus'];
      $data['email_cus_sec'] = $_POST['email_cus_sec'];
      
      $cust = $objCust->getCust($data['email_cus'],0,$transData['id']);
      
      //print_r($cust);exit;
      
      if(!$cust)
      {
        $this->view->result = $objCust->insertCust($data);
      }
      else
      {
        $this->view->error = 'The customer with the e-mail ('.$data['email_cus'].') is already';
      }
    }
    
    $this->view->trans = $transData;
  }
  
  public function emailListAction()
  {
    $objCust = new models_Customers();
    
    $transID =  Zend_Auth::getInstance()->getIdentity()->id;
    
    $this->view->customers = $objCust->getCust(null,0,$transID);
  }
  
  public function settingsAction()
  {
    if(!empty($_POST))
    {
      $password = md5($_POST['pswd']);
      
      $newPass = $_POST['newpswd'];
      $confPass = $_POST['conwpswd'];
      
      if($newPass != $confPass)
      {
        $this->view->error = 'Passwords do not match';
      }
      else if(Zend_Auth::getInstance()->getIdentity()->pass_tran != $password)
      {
        $this->view->error = 'Incorrect password';
      }
      else
      {
        $objTrans = new models_Translator();
        
        $objTrans->updateTrans(Zend_Auth::getInstance()->getIdentity()->id, array('pass_tran'=>md5($newPass)));
        
        $this->view->res = 'Password changed';
      }
    }
  }
}