<?php

class UserController extends Zend_Controller_Action
{
  private $agensyID = false;
  private $custID = null;
  
  public function init()
  {
    $this->_helper->layout()->setLayout('templates/user');
    if (session_id()=='') session_start();
  }

  public function uploadtranslatedfileAction(){
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();

    $req = $this->getRequest();
    $sess_id =  $req->getParam("id", null);       // smartlation order num
    $file_urls =  $req->getParam("files", null);  // translated file urls

    if (!is_null($sess_id) && !is_null($file_urls))
    {
      $objO = new models_Order();
      if ($objO->uploadTranslatedFiles($sess_id, $file_urls)){
        $res = array( "status" => true );
      }else{
        $res = array( "status" => false, "error" => "files were not uploaded!");
      }
    }else{
      $res = array( "status" => false, "error" => "id or files parameter was missed!");
    }

    echo json_encode($res);
  }

  public function uploaddonepercentAction(){
    $this->_helper->viewRenderer->setNoRender(true);
    $this->_helper->layout->disableLayout();

    $req = $this->getRequest();
    $sess_id =  $req->getParam("id", null);       // smartaltion order num
    $perc =  $req->getParam("work_status", null); // work_status

    if (!is_null($sess_id) && !is_null($perc))
    {
      $objO = new models_Order();
      if ($objO->updatePercent($sess_id, $perc))
      {
        $res = array( "status" => true );
      }else{
        $res = array( "status" => false, "error" => "percent was not updated!");
      }
    }else{
      $res = array( "status" => false, "error" => "id or percent parameter was missed!");
    }

    echo json_encode($res);
  }
  
  public function checkLogin()
  {
    $auth = Zend_Auth::getInstance();
    
    if(!$auth->hasIdentity() || $auth->getIdentity()->role != 1)
    {
      $this->_helper->redirector('login', 'user');
      exit;
    }
    else
    {
      $this->custID = $auth->getIdentity()->cus_id;
      $this->view->menu = true;
      
      $objOrders = new models_Order();
      
      $orderData = $objOrders->getCountOrder($this->custID);
      
      //
      
      $this->view->ordersCount = $orderData['count'];
      //print_r($orderData);exit;
    }
  }

  public function checkDomain()
  { 
    $_SESSION['email'] = '';
    
    $subDomain = models_Model::getSubdomain();

    // let's try to search by host name first
    $objOrdDom = new models_OrderDomains();
    list($host,)=explode(':',$_SERVER['HTTP_HOST']);
    
    //$host = 'www.allasprak.se';
    
    $data = $objOrdDom->getEmailOrdDomain($host);
    //
    //var_dump($data);exit;
    
    if(isset($data['email_r']))
    {
      
    }
    else if($subDomain)
    {
      $data = $objOrdDom->getEmailOrdDomain($subDomain);
    }
    else if(!$data)
    {
      echo 'Site does not exist';
      exit;
    }
    
    $_GET['upload'] = true;
    
    
    $_SESSION['email'] = $data['email_r'];
    
    //var_dump($data['email_r']);exit;

    $templates = new models_Templates();
    
    if($templates->checkTemplate())
    {
      $temp_id = $templates->getTemId();
      
      $objTempl = new models_TemplOther();
      
      //print_r($trans);exit;
      
      $objAgensy = new models_Agencys();
      
      $agensy = $objAgensy->getAgencys($data['email_r']);
      
      $this->agensyID = $agensy['agency_id'];
      
      $objTrans = new models_Translator();
      
      $trans = $objTrans->getTrEmail($data['email_r']);
      
      //var_dump($data);exit;
      
      $otherTempl = $objTempl->getTemp($trans['site_lang_id']);
      
      $this->view->siteLang = $trans['site_lang_id'];
      
      $this->view->data = (!$otherTempl) ? false : json_decode($otherTempl['data'], true);
      
      //print_r($this->view->data);exit;
      
      $this->view->templId = $trans['templ_id'];
      
      $objLang = new models_Languages();
        
      $this->view->siteLangCode = $objLang->getLang($trans['site_lang_id']);
    
    }
    else
    {
      echo 'Site does not exist';
      exit;
    }
    
  }
  
  public function indexAction()
  {
    $this->checkLogin();
    
    $objCust = new models_Customers();
    
    $this->view->custData = $objCust->getCust(null, $this->custID);
  }
  
  public function changepasswordAction()
  {
    $this->checkLogin();
  }
  
  public function ordersAction()
  {
    $this->checkLogin();
    
    $id = (int)$this->getRequest()->getParam('id');
    
    $objOrder = new models_Order();
    
    if($id !== 0)
    {
      $this->view->order = $objOrder->getOrderCust($this->custID, $id);
      
      if($this->view->order)
      {
        $this->_helper->viewRenderer('order-id');
        
        $objFiels = new models_File();
      
        $this->view->files = $objFiels->getFiles($id);
      }
      else
      {
        $this->_helper->viewRenderer('order-not');
      }
    }
    
    $this->view->orders = $objOrder->getOrderCust($this->custID);
  }
  
  public function loginAction()
  {
    if(Zend_Auth::getInstance()->hasIdentity() && Zend_Auth::getInstance()->getIdentity()->role == 1)
    {
      header('Location: /user');
    }
    
    if($this->getRequest()->isPost())
    {
      //print_r($_POST);exit;
      $auth = Zend_Auth::getInstance();
      
      $email = $this->getRequest()->getPost('login');
      $password = $this->getRequest()->getPost('password');
      
      $objCust = new models_Customers();
      
      $cust = $objCust->getCustLogin($email, $password, $this->agensyID);
      //
      if($cust !== false)
      {
        if($cust->role == 1)
        {
          // �������� ������ � ��������� ������ Zend
          $authStorage = $auth->getStorage();
          
          // �������� ���� ���������� � ������������,
          // ����� ����� � ��� ������ ��� ���������������� Acl
          $authStorage->write($cust);
  
          //$this->redirectUrl($identity->role);
          header('Location: /user');
          exit;
        }
        else
        {
          $this->view->error = true;
        }
      }
      else
      {
        $this->view->error = true;
      }
    }
  }
  
  /** method logout */
  public function logoutAction()
  {
    $this->_helper->layout->disableLayout();

    Zend_Auth::getInstance()->clearIdentity();

    header('Location: /user/login');
    exit;
  }
}