<?php

class CheckoutController extends Zend_Controller_Action
{
  public function init()
  {
    $this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
  }

  public function indexAction()
  {
    $_SESSION['email'] = '';
    
    $subDomain = models_Model::getSubdomain();

    // let's try to search by host name first
    $objOrdDom = new models_OrderDomains();
    list($host,)=explode(':',$_SERVER['HTTP_HOST']);
    
    $data = $objOrdDom->getEmailOrdDomain($host);
    //print_r($host);exit;
    if ($data){
      $_SESSION['email'] = $data['email_r'];
    }else if($subDomain)
    {
      $data = $objOrdDom->getEmailOrdDomain($subDomain);
      //print_r($data);exit;
      $_SESSION['email'] = $data['email_r'];
    }
    
    $_GET['temp_id'] = 3;
    
    //print_r($data);exit;
    
    $_SESSION['email'] = $data['email_r'];

    $templates = new models_Templates();
    
    if($templates->checkTemplate())
    { 
      $this->_helper->layout()->setLayout('templates/checkout');
      
      $this->priceOrder($data['email_r'], $host);
      
      $this->view->domain = 'http://'.$subDomain.'.translated.co';
      
      $objTrans = new models_Translator();
      
      $trans = $objTrans->getTrEmail($data['email_r']);
      
      //print_r($trans);exit;
      
      $objTempl = new models_TemplOther();
      
      //print_r($trans);exit;
      
      $otherTempl = $objTempl->getTemp($trans['site_lang_id']);
      
      $this->view->siteLang = $trans['site_lang_id'];
      
      $objLang = new models_Languages();
        
      $this->view->siteLangCode = $objLang->getLang($trans['site_lang_id']);
      
      $this->view->data = (!$otherTempl) ? false : json_decode($otherTempl['data'], true);
      
      //print_r($this->view->data);exit;
    }
    else
    {
      echo 'Site does not exist';
      exit;
    }
    
  }
  
  private function priceOrder($email, $host)
  {
    $order_id = (int)$this->getRequest()->getParam('id');
    if($order_id == 0)
    {
      header('Location: /upload');
      exit;
    }
    $objOrder = new models_Order();
    $orderData = $objOrder->getOrder($order_id);
    
    $objOrPlan = new models_OrderPlans();
    $priceCount = $objOrPlan->getPrice($email, $orderData['lang_id'],$orderData['dest_lang_id']);

    $send_to_smartaltion = false;
    if(!$priceCount)
    {
      $objGrLan = new models_GroupLang();
      $priceCount = $objGrLan->getPrice($orderData['lang_id'],$orderData['dest_lang_id']);
      $send_to_smartaltion = true;
    }

    $price = $priceCount['price'];
    
    $objFile = new models_File();
    $files = $objFile->getFiles($order_id);
    //print_r($files);exit;
    $total = 0;
    $len=0;
    
    foreach($files as $file)
    {
      if($file['name_file'] != 0)
      {
        $total += $file['len']*$price;
        $len += $file['len'];
      }
      else
      {
        $this->view->pocesing = true;
      }
    }
    
    $objLang = new models_Languages();
    
    $lang = $objLang->getLang($orderData['lang_id']);
    
    $this->view->sourse = $lang['name_leng'];
    
    $lang = $objLang->getLang($orderData['dest_lang_id']);
    
    $this->view->dest = $lang['name_leng'];
    
    $this->view->files = $files;
    $this->view->price = $total;
    
    $this->view->days = ($len > 0) ? ceil($len/2000) : 1; 
    
    //print_r($host);exit;
    
    if($host == 'www.allasprak.se')
    {
      $this->view->sek = models_Model::convert($total);
    }
    
    $this->view->order_id = $order_id;
    
    $req = $this->getRequest();
      
    if (!is_null($req->getPost("submitbtn", null)))
    {
      
      $femail = $req->getPost("femail", null);
      $femail2 = $req->getPost("femail2", null);

      if ($femail == $femail2 && !is_null($femail))
      {
        // let's try to process credit card payment
        $fname = $req->getPost("ffirstname","");
        $lname = $req->getPost("flastname","");
        $ccno = $req->getPost("ccno","");
        $expmonth = $req->getPost("expmonth","");
        $expyear = $req->getPost("expyear","");
        $mycvv = $req->getPost("mycvv","");
        $sum = $this->view->price;
        
        $objAgensy = new models_Agencys();
        $agensData = $objAgensy->getAgencys($email);
        
        $objCust = new models_Customers();
        $custData = $objCust->getCust($femail,0,$agensData['agency_id']);

        if(!$custData)
        {
          $dataCustInsert = array();
          
          $password = models_Model::passwGen();
          
          $dataCustInsert['email_cus'] = $femail;
          $dataCustInsert['password'] = md5($password);
          $dataCustInsert['name_cus'] = $fname;
          $dataCustInsert['last_name_cus'] = $lname;
          $dataCustInsert['agency_id_r'] = $agensData['agency_id'];
          
          $custId = $objCust->insertCust($dataCustInsert);
        }
        else
        {
          $custId = $custData['cus_id'];
        }
        
        $objOrder->updateOrder($order_id, array('customer_id'=>$custId));

        if($femail == 'evgeniy.starcev.88@gmail.com' || strpos($femail, 'test123'))
        {
          $res[0]=true;
        }
        else
        {
          $res = models_Tranzila::charge($sum, $ccno, $expmonth, $expyear, $mycvv, $fname, $lname, $femail);
        }
        
        if (!$res[0])
        {
          $objTR = new models_TranzilaResponse();
          $result = $objTR->getCode($res[1]);
          $this->view->error = $result['string'].' (code - '.$res[1].')';
        }
        else
        {
          $objOrder->updateOrder($order_id, array('status'=>'1','price'=>$total));
          
          $orderData = $objOrder->getOrder($order_id);
          $objTrans = new models_Translator();
          
          $transData = $objTrans->getTrEmail($email);
          
          $objLang = new models_Languages();
          $langs = $objLang->getLangMacros($transData['site_lang_id']);
          $langArr = json_decode($langs['data'],true);
          
          if(!isset($langArr['text35']))
          {
            $langs = $objLang->getLangMacros(902);
            $langArr = json_decode($langs['data'],true);
          }
          
          $agensyName = (isset($agensData['name_agen'])) ? $agensData['name_agen'] : $host;
          $message = models_Model::getString($langArr['text35'],array($fname,$agensyName));
          
          $mailer = new models_Mailer();

          // message to customer if customer was missed
          if(isset($password))
          {
            $messAcount = 'Welcome '.$fname.', you can manage your orders in our Customer Panel.<br><br>
            Customer panel access: <a href="http://'.$host.'/user/login">http://'.$host.'/user/login/</a><br>
            Your login: '.$femail.'<br>
            Your password: '.$password.'<br><br>
            Sincerely,<br>Translated.co team';
            
            $mailer->doMail($femail,'Your customer panel access',$messAcount, $email, null, $agensyName);
          }

          // customer notification about new order
          $mailer->doMail($femail,'You have placed new order at translated.co!',$message, $email, null, $agensyName);

          $sek = '';
          if(isset($this->view->sek))
          {
            $sek = ' ('.$this->view->sek.' SEK)';
          }

          if (!$send_to_smartaltion)
          {
            // message to the agency
            $message = "Hello $agensyName, we just got new order at your agency site. Don't miss it out!<br><br>
            Order details:<br>
            Order price: $sum $ $sek<br>
            Source language: {$orderData['sourse']}<br>
            Destination language: {$orderData['dest']}<br>
            Customer email: $femail<br>
            Customer name: $fname<br><br>Sincerely,<br>Translated.co team";
            $mailer->doMail($email,"Don't miss out new order at translated.co!",$message);
          }else{
            // Smartlation will do the job!, send order to Smartlation API here and also don't forget to update
            // flag that order is processed by Smartlation
            $smartaltion_host = 'http://smartlation.com';

            // prepare request
            foreach($files as $file){
              $file_urls[] = "files[]=".urlencode("http://translated.co/uploads/".$transData['id']."/".$file['name_file']);
            }

            $err = '';
            $url = $smartaltion_host.'/api/prepareorder/?flang='.$orderData['lang_id'].'&'.implode('&', $file_urls);
            $res = file_get_contents($url);
            if ($res)
            {
              $res_json = json_decode($res);
              if ($res_json){
                if ($res_json->status)
                {
                  $id = $res_json->id;

                  // wait for files to be processed
                  $not_processed = true;
                  while($not_processed)
                  {
                    $url = $smartaltion_host.'/api/getstatus/?id='.$id;
                    $res = file_get_contents($url);
                    if ($res)
                    {
                      $res_json = json_decode($res);
                      if ($res_json){
                        if ($res_json->status)
                        {
                          $files = $res_json->files;

                          $not_processed = false;
                          foreach($files as $file){
                            if (is_null($file->words) || $file->words==''){
                              $not_processed = true;
                              break;
                            }
                          }
                        }else{
                          $err = $res_json->error;
                          break;
                        }
                      }else{
                        $err = "Invalid JSON response in getstatus";
                        break;
                      }
                    }else{
                      $err = "HTTP request of getstatus was failed";
                      break;
                    }
                  }

                  if ($err=='')
                  {
                    $src_lang_id = $orderData['lang_id'];
                    $dst_lang_id = $orderData['dest_lang_id'];
                    $exp = $orderData['expertise'];
                    $price = $total;

                    $url = $smartaltion_host."/api/placeorder/?id=$id&flang=$src_lang_id&tlang=$dst_lang_id&exp=$exp&price=$price";
                    $res = file_get_contents($url);

                    if ($res)
                    {
                      $res_json = json_decode($res);
                      if ($res_json){
                        if ($res_json->status)
                        {
                          $objOrder->updateOrder($order_id, array('smartlation_order_num'=>$id));
                        }else{
                          $err = $res_json->error;
                        }
                      }else{
                        $err = "Invalid JSON response in placeorder";
                      }
                    }else{
                      $err = "HTTP request of placeorder was failed";
                    }
                  }
                }else{
                  $err = $res_json->error;
                }
              }else{
                $err = "Invalid JSON response in prepareorder";
              }
            }else{
              $err = "HTTP request of prepareorder was failed";
            }

            if ($err!='')
            {
              $objM = new models_Mailer();
              $objM->doMail("sidorenkod@gmail.com", "DEBUG MESSAGE: translated.co=>smartlation API Integration", $err."<br>".$url."<br>".json_encode($res_json));
            }else{

            }
          }

          header("Location: /page/?id={$order_id}");
          exit;
        }
      }else{
        $this->view->error = 'not email';
      }
    }
  }
  
  public function checkpriceAction()
  {
    $subDomain = models_Model::getSubdomain();

    // let's try to search by host name first
    $objOrdDom = new models_OrderDomains();
    list($host,)=explode(':',$_SERVER['HTTP_HOST']);
    $data = $objOrdDom->getEmailOrdDomain($host);
    //print_r($host);exit;
    if ($data){
      $_SESSION['email'] = $data['email_r'];
    }else if($subDomain)
    {
      $data = $objOrdDom->getEmailOrdDomain($subDomain);
      //print_r($data);exit;
      $_SESSION['email'] = $data['email_r'];
    }
    
    $order_id = (int)$this->getRequest()->getPost('order_id');
    
    $objOrder = new models_Order();
    $orderData = $objOrder->getOrder($order_id);
    
    //print_r($orderData);exit;
    
    $objOrPlan = new models_OrderPlans();
    $priceCount = $objOrPlan->getPrice($data['email_r'], $orderData['lang_id'],$orderData['dest_lang_id']);
    //print_r($orderData);exit;
    if(!$priceCount)
    {
      $objGrLan = new models_GroupLang();
      
      $priceCount = $objGrLan->getPrice($orderData['lang_id'],$orderData['dest_lang_id']);
    }

    $price = $priceCount['price'];
    
    $objFile = new models_File();
    $files = $objFile->getFiles($order_id);
    //print_r($files);exit;
    $total = 0;
    $len=0;
    
    $result = true;
    
    foreach($files as $file)
    {
      if($file['name_file'] != 0)
      {
        $total += $file['len']*$price;
        $len += $file['len'];
      }
      else
      {
        $result = false;
      }
    }
    
    $days = ($len > 0) ? ceil($len/2000) : 1; 
    
    $data = array(
      'result'=>$result,
      'price'=>$total,
      'days'=>$days, 
      'priceSek'=>models_Model::convert($total)
    );
    
    echo json_encode($data);
    
    exit;
  }
  
  public function resultAction()
  {
      $this->_helper->layout()->setLayout('empty');
      $pst = $_POST;

      //$settings = Zend_Registry::get('settings');
      $paypalhost = 'www.sandbox.paypal.com';

      $verify_url = "https://$paypalhost/cgi-bin/webscr?cmd=_notify-validate&" . http_build_query( $_POST );
      $ch = curl_init($verify_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_USERAGENT, "IGMI");
      $res = '';
      if( ($res = curl_exec($ch) ) === false)
      {
          $err = curl_error($ch);

          // testing only
          //$objMail = new models_MailNotifications();
          //$objMail->doMail("sidorenkod@gmail.com", "Invoice payment error", "Error details: $err");
      }
      curl_close($ch);

      //testing only
      //$objMail = new models_MailNotifications();
      //$objMail->doMail("sidorenkod@gmail.com", "Payment result", "$res, ".http_build_query( $pst ));

      if (!strcasecmp($res, "VERIFIED"))
      {
        if(!strcasecmp($pst['payment_status'],'completed') || !strcasecmp($pst['payment_status'],'pending')) {
          $objOrder = new models_Order();
          $objOrder->updateOrder($pst['invoice'],array('status'=>1));
        }
      }
  }
}