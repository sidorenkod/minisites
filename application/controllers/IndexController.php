<?php

class IndexController extends Zend_Controller_Action
{

  public function init()
  {
    $this->_helper->layout()->setLayout('index');
    $this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    if (session_id()=='') session_start();
  }

  public function indexAction()
  {
    $_SESSION['email'] = '';
    
    $subDomain = models_Model::getSubdomain();

    // let's try to search by host name first
    $objOrdDom = new models_OrderDomains();
    list($host,)=explode(':',$_SERVER['HTTP_HOST']);
    $data = $objOrdDom->getEmailOrdDomain($host);
    //print_r($host);exit;
    if ($data){
      $_SESSION['email'] = $data['email_r'];
    }else if($subDomain)
    {
      $data = $objOrdDom->getEmailOrdDomain($subDomain);
      //print_r($data);exit;
      $_SESSION['email'] = $data['email_r'];
    }

    if ($_SESSION['email']!='')
    {
      $templates = new models_Templates();
      
      if($templates->checkTemplate())
      {
        $temp_id = $templates->getTemId();
        
        $objTempl = new models_TemplOther();
        
        $objTrans = new models_Translator();
      
        $trans = $objTrans->getTrEmail($data['email_r']);
        
        $otherTempl = $objTempl->getTemp($trans['site_lang_id']);
        
        $objLang = new models_Languages();
        
        $this->view->siteLangCode = $objLang->getLang($trans['site_lang_id']);
        
        //print_r($this->view->siteLang);exit;
        
        //print_r(json_decode($otherTempl['data'], true));exit;
        
        $this->view->data = (!$otherTempl) ? false : json_decode($otherTempl['data'], true);
        $this->view->siteLang = $trans['site_lang_id'];

        $this->_helper->layout()->setLayout('templates/template-'.$temp_id);
      }
      else
      {
        echo 'Site does not exist';
         exit;
      }
      
      $this->_helper->viewRenderer('empty');
    }
    else
    {
      $this->login();
    }
  }
  
  public function testAction()
  {
    /*$objTempl = new models_Templates();
    
    $templ = $objTempl->getTemplate(13,null,true);
    $data = json_decode($templ[1]['json_data'], true);
    print_r($data);
    exit;*/
    
    
    
    $newData = Array
(
    '1_txt1' => 'Alla språK ',
    '1_txt1_style' => 'font-size: 40px; text-align: right;',
    '1_txt2' => '',

    '1_txt2_style' => 'text-align: center;',
    '1_txt3' => 'Ladda upp - Jämför - Klart!',
    '1_txt3_style' => '',
    '1_txt4' => 'Allaspråk.se är ett enkelt sätt att hitta kvalitativ översättning. Genom att enkelt ladda upp dina filer får du direkt offert för din översättning.
Vi har över 4000 översättare i vårt nätverk som kan översätta till över 140 språk  ',
    '1_txt4_style' => '',
    '1_bg1' => 'background-image: url("/templates/template_2/images/2.jpg");',
    '1_bg2' => '',
    '1_txt5' => 'TJÄNSTER',
    '1_txt5_style' => 'color:white !important; text-align:center;',
    '1_txt6' => 'ÖVERSÄTTNING',
    '1_txt6_style' => '',
    '1_txt7' => 'AllaSpråk ger dig tillgång till över 4000 översättare jorden runt ',
    '1_txt7_style' => '',
    '1_txt8' => 'APPÖVERSÄTTNING',
    '1_txt8_style' => '',
    '1_txt9' => 'Med AllaSpråk kan du även översätta dina Android och iOS appar',
    '1_txt9_style' => '',
    '1_txt10' => 'KORREKTURLÄSNING',
    '1_txt10_style' => '',
    '1_txt11' => 'Vi korrekturläser dina projekt',
    '1_txt11_style' => '',
    '1_txt12' => 'Cost Effectiveness',
    '1_txt12_style' => '',
    '1_txt13' => 'All prices quoted through SMARTLATION.COM are based on the source language text. When a project is uploaded, our system provides an accurate word count and generates a quote. By providing the quote upon upload, the client knows the final price in advance.',
    '1_txt13_style' => '',
    '1_txt14' => 'Speed',
    '1_txt14_style' => '',
    '1_txt15' => 'The list displays the estimated time of delivery for each vendor. The system also tracks the translator’s progress. If there is a delay, the client is notified immediately. This ensures that the job is finished on time.',
    '1_txt15_style' => '',
    '1_txt16' => 'Ease of Mind',
    '1_txt16_style' => '',
    '1_txt17' => 'All stages of a project are done through SMARTLATION.COM. This saves both client and vendor from having to use different methods of communication for each stage of the job. SMARTLATION.COM makes the whole translation process fast, easy, and simple.',
    '1_txt17_style' => '',
    '1_txt18' => 'Med våran platform får du kontroll över alla stadier av din översättning. ',
    '1_txt18_style' => 'color:white; text-align:center; margin-top:20px; margin-bottom:-30px; z-index:99999;',
    '1_txt19' => 'BLI ÖVERSÄTTARE',
    '1_txt19_style' => 'margin:0px 5px;',
    '1_txt20' => 'ÖVERSÄTT NU',
    '1_txt20_style' => 'margin:0px 5px;',
    '1_txt21' => 'KONTAKTA OSS',
    '1_txt21_style' => 'margin:0px 5px;',
    '1_txt22' => 'Våra fördelar',
    '1_txt22_style' => '',
    '1_txt23' => 'Hur FUNKAR det?',
    '1_txt23_style' => '',
    '1_txt24' => 'Vad vi gör',
    '1_txt24_style' => '',
    '1_txt25' => 'Nyheter',
    '1_txt25_style' => '',
    '1_txt26' => 'Bli Översättare',
    '1_txt26_style' => '',
    '1_txt27' => 'ProfessionelL ÖVERSÄTTNING',
    '1_txt27_style' => '',
    '1_txt28' => 'Korrekturläsning',
    '1_txt28_style' => '',
    '1_txt29' => 'PROJEKTLEDNING',
    '1_txt29_style' => '',
    '1_txt30' => 'Frågor och svar Översättare',
    '1_txt30_style' => '',
    '1_txt31' => 'Frågor och svar KUnder',
    '1_txt31_style' => '',
    '1_txt32' => 'KONTAKTA OSS',
    '1_txt32_style' => '',
    '1_txt33' => 'FILER VI STÖDJER',
    '1_txt33_style' => '',
    '1_txt34' => 'HUR FUNGERAR SMARTLATION?',
    '1_txt34_style' => '',
    '1_txt35' => 'ÖVERSÄTTARFORUM',
    '1_txt35_style' => '',
    '1_txt36' => 'Smartlation Blog',
    '1_txt36_style' => '',
    '1_txt37' => 'OM OSS',
    '1_txt37_style' => 'margin-top:50px;',
    '1_txt38' => 'TJÄNSTER',
    '1_txt38_style' => 'margin-top:50px;',
    '1_txt39' => 'FRÅGOR',
    '1_txt39_style' => 'margin-top:50px;',
    '1_txt40' => 'HJÄLP',
    '1_txt40_style' => 'margin-top:50px;',
    '1_txt333' => 'Professionell översättning',
    '1_txt333_style' => 'visibility: visible; animation-name: fadeInUp;',
    '1_txt41' => 'KONTAKTA OSS',
    '1_txt41_style' => '',
    '1_btn4' => 'SKICKA',
    '1_btn4_style' => '',
    '1_btn1' => 'Översätt här',
    '1_btn1_style' => 'color: rgb(255, 255, 255); background-color: rgb(2, 143, 204);',
    '1_butt2' => '<i class="fa fa-play-circle"></i> Hur funkar det?',
    '1_butt2_style' => '',
    '1_btnnn2' => 'Översätt nu',
    '1_btnnn2_style' => '',
    '1_btnnn3' => 'Våra språk',
    '1_btnnn3_style' => '',
    '1_blc1' => '1',
    '1_blc2' => '1',
    'block3' => '1',
    'block4' => '1',
    'block5' => '1',
    'block6' => '1',
    'block7' => '1',
    'block8' => '0',
    'block9' => '0',
    'block10' => '0',
    'link1' => 'HEM',
    'link1_block' => '1',
    'link1_style' => '',
    'link2' => 'How does it works?',
    'link2_block' => '1',
    'link2_style' => '',
    'link3' => 'What we do?',
    'link3_block' => '1',
    'link3_style' => '',
    'link4' => 'New & Updates',
    'link4_block' => '1',
    'link4_style' => '',
    'link5' => 'Become Our Translator',
    'link5_block' => '1',
    'link5_style' => '',
    'link6' => 'Professional Translation',
    'link6_block' => '1',
    'link6_style' => '',
    'link7' => 'Proof Reading',
    'link7_block' => '1',
    'link7_style' => '',
    'link8' => 'Project Manager',
    'link8_block' => '1',
    'link8_style' => '',
    'link9' => 'Transciption',
    'link9_block' => '1',
    'link9_style' => '',
    'link10' => 'KONTAKTA OSS',
    'link10_block' => '1',
    'link10_style' => '',
    'link11' => 'OM OSS<b class="caret"></b>',
    'link11_block' => '1',
    'link11_style' => '',
    'link12' => 'Our Advantages',
    'link12_block' => '1',
    'link12_style' => '',
    'link13' => 'TJÄNSTER<b class="caret"></b>',
    'link13_block' => '1',
    'link13_style' => '',
    'logo' => '/templates/templates_users/13/logo.png',
    '1_img2' => '/templates/template_1/img/about.png',
    '1_soc1' => '1',
    '1_soc1_link' => 'https://www.facebook.com/smartlation',
    '1_soc2' => '1',
    '1_soc2_link' => 'https://twitter.com/smartlation',
    '1_soc3' => '1',
    '1_soc3_link' => 'https://plus.google.com/+SmartlationMarketplace',
    '1_soc4' => '1',
    '1_soc4_link' => 'https://www.linkedin.com/company/smartlation',
    '1_soc5' => '1',
    '1_soc5_link' => 'https://www.pinterest.com/smartlation/',
    
    
    '3_txt1' => 'EASY TRANSLATION JOB',
  '3_txt1_style' => 'font-size:40px;',
  '3_txt2' => 'Choose Language',
  '3_txt2_style' =>  '',
  '3_txt3' =>  'Upload Your Files',
  '3_txt4' =>  'Get Instant Offers',
   
  '3_txt41' =>  'Translate From',
  '3_txt42' =>  'Translate From',
  '3_txt43' =>  'Expertise',

  '3_img2' =>  '/templates/template_3/img/top.png',
  '3_bg2nav' =>  'background-color:#eef3f7',
  '3_about' =>  '',
  '3_btn1' =>  'FIND YOUR TRANSLATOR NOW!',
  '3_btn1_style' =>  '',
  'footer' =>  '',
  '3_bg23' =>  '',
    
    
    'css-preset' => 'color_1 ',

    'logobot' => '/templates/templates_users/13/logo.png',
    'slogan_p' => 'SNABB OCH PROFESSIONELL ÖVERSÄTTNING',
    'slogan_p_color' => '',
    'id_h1' => 'Välkommen till <span>AllaSpråk</span>',
    'id_h1_color' => '',
    'text_h2_1' => 'Varför välja oss?',
    'text_h2_1_color' => '',
    'text_p_1' => 'Jämför enkelt den bästa översättare för just dig. Du sparar både tid och pengar.',
    'text_p_1_color' => '',
    'text_h3_1' => 'Ladda upp filerna som ska översättas',
    'text_h3_1_color' => '',
    'text_p_2' => '',
    'text_p_2_color' => '',
    'text_h3_2' => 'Välj din översättare enligt pris och kvalité',
    'text_h3_2_color' => '',
    'text_p_3' => '',
    'text_p_3__color' => '',
    'text_h3_3' => 'Din översättning kommer inom kort',
    'text_h3_3_color' => '',
    'text_p_4' => '',
    'text_p_4_color' => '',
    'bg_1' => 'background-image: url(/templates/template_2/images/header.jpg)',
    'text_h2_2' => 'Om oss',
    'text_h2_2_color' => '',
    'text_p_5' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.Ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    'text_p_5_color' => '',
    'text_p_6' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    'text_p_6_color' => '',
    'text_h2_3' => 'Professional Credibility & Experience',
    'text_h2_3_color' => '',
    'text_p_7' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam',
    'text_p_7_color' => '',
    'text_p_8' => 'Experience Title-1',
    'text_p_8_color' => '',
    'text_p_9' => '2011 - 2013',
    'text_p_9_color' => '',
    'text_p_10' => 'Name of Company',
    'text_p_10_color' => '',
    'text_p_11' => 'Lorem ipsum dolor sit amet, consectetur adipiscingVivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.',
    'text_p_11_color' => '',
    'text_p_12' => 'Experience Title-2',
    'text_p_12_color' => '',
    'text_p_13' => '2010 - 2010',
    'text_p_13_color' => '',
    'text_p_14' => 'Name of Company',
    'text_p_15' => 'Lorem ipsum dolor sit amet, consectetur adipiscingVivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.',
    'text_h3_4' => 'Cerification-1',
    'text_h4_1' => 'Name of Institute',
    'text_p_16' => 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    'text_h3_5' => 'Cerification-2',
    'text_h4_2' => 'Name of Institute',
    'text_p_17' => 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    'text_h3_6' => 'Affiliation-1',
    'text_h4_3' => 'Name of Affiliate',
    'text_p_18' => 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    'text_h3_7' => 'Affiliation-2',
    'text_h4_4' => 'Name of Affiliate',
    'text_p_19' => 'Consectetur adipisicing elit, sed do eiusmod tempor incididunt',
    'text_h3_8' => '150',
    'text_p_20' => 'Completed Order',
    'text_h3_9' => '65',
    'text_p_21' => 'Available Languages',
    'text_h3_10' => '93%',
    'text_p_22' => 'Positive Ratings',
    'text_h3_11' => '44',
    'text_p_23' => 'Clients',
    'text_h2_4' => 'Hire Us',
    'text_p_24' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam',
    'text_h4_5' => 'Client References',
    'text_p_25' => 'Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
    'text_p_26' => 'Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
    'text_p_27' => 'Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.',
    'text_h4_6' => 'Client One',
    'text_h4_7' => 'Client Two',
    'text_h4_8' => 'Client Three',
    'text_h2_5' => 'Blog Posts',
    'text_p_28' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam',
    'text_h3_12' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
    'text_p_29' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    'text_h3_13' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
    'text_p_30' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    'text_h3_14' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit',
    'text_p_31' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    'text_p_32' => 'June 26, 2016',
    'text_p_33' => 'June 26, 2016',
    'text_p_34' => 'June 26, 2016',
    'text_h2_6' => 'Contact Us',
    'text_p_35' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam',
    'text_p_36' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.',
    'text_p_37' => 'Test Location',
    'text_p_38' => '+123 456 7890',
    'text_p_39' => '+123 456 7890',
    'text_p_40' => 'Skype/test-user',
    'text_p_41' => '&copy;2016 | All Rights Reserved',
    'text_p_42' => 'www.test-user.com',
    'text_p_14_color' => '',
    'text_p_15_color' => '',
    'text_h3_4_color' => '',
    'text_h4_1_color' => '',
    'text_p_16_color' => '',
    'text_h3_5_color' => '',
    'text_h4_2_color' => '',
    'text_p_17_color' => '',
    'text_h3_6_color' => '',
    'text_h4_3_color' => '',
    'text_p_18_color' => '',
    'text_h3_7_color' => '',
    'text_h4_4_color' => '',
    'text_p_19_color' => '',
    'text_h3_8_color' => '',
    'text_p_20_color' => '',
    'text_h3_9_color' => '',
    'text_p_21_color' => '',
    'text_h3_10_color' => '',
    'text_p_22_color' => '',
    'text_h3_11_color' => '',
    'text_p_23_color' => '',
    'text_h2_4_color' => '',
    'text_p_24_color' => '',
    'text_h4_5_color' => '',
    'text_p_25_color' => '',
    'text_p_26_color' => '',
    'text_p_27_color' => '',
    'text_h4_6_color' => '',
    'text_h4_7_color' => '',
    'text_h4_8_color' => '',
    'text_h2_5_color' => '',
    'text_p_28_color' => '',
    'text_h3_12_color' => '',
    'text_p_29_color' => '',
    'text_p_30_color' => '',
    'text_h3_14_color' => '',
    'text_p_31_color' => '',
    'text_p_32_color' => '',
    'text_p_33_color' => '',
    'text_p_34_color' => '',
    'text_h2_6_color' => '',
    'text_p_35_color' => '',
    'text_p_36_color' => '',
    'text_p_37_color' => '',
    'text_p_38_color' => '',
    'text_p_39_color' => '',
    'text_p_40_color' => '',
    'text_p_41_color' => '',
    'text_p_42_color' => '',
    'features' => '',
    'about-us' => '',
    'twitter' => '',
    'contact-us' => '',
    'lang1' => 'Language-1',
    'lang2' => 'Language-2',
    'lang3' => '0%',
    'lang4' => '0',
    'langp1' => '95',
    'langp2' => '0',
    'langp3' => '0',
    'langp4' => '0',
    'lang1_color' => '',
    'lang2_color' => '',
    'lang3_color' => '',
    'lang4_color' => '',
    'langp1_color' => '',
    'langp2_color' => '',
    'langp3_color' => '',
    'langp4_color' => '',
    'user_files' => Array
        (
        ),

    'user_files_bg' => Array
        (
        ),

    'block1' => '0',
    'block2' => '0',
    'block3' => '1',
    'block4' => '1',
    'comt1' => '1',
    'comt2' => '1',
    'comt3' => '1',
    'img1' => '/templates/template_2/images/blog/1.jpg',
    'img2' => '/templates/template_2/images/blog/2.jpg',
    'img3' => '/templates/template_2/images/blog/3.jpg',
    'img4' => '/templates/template_2/images/affiliation/certifications.png',
    'img5' => '/templates/template_2/images/affiliation/certifications.png',
    'img6' => '/templates/template_2/images/affiliation/affiliations.png',
    'img7' => '/templates/template_2/images/affiliation/affiliations.png',
    'nav' => '',
    'services' => '',
    'team' => '',
    'hire' => '',
    'blog' => '',
    'footer' => '',
    'footer_bot' => '',
    'text9' => '',
    'text10' => '',
    'undefined_block' => '0',
    'id_h1_style' => '',
    'slogan_p_style' => '',
    'text9_style' => '',
    'text10_style' => '',
    'undefined' => 'Översätt',
    'text_h2_1_style' => '',
    'text_h2_2_style' => '',
    'text_h3_1_style' => '',
    'text_h3_2_style' => '',
    'text_h3_3_style' => '',
    'text_p_1_style' => 'text-align: center;',
    'text_p_2_style' => '',
    'text_p_3_style' => '',
    'text_p_4_style' => '',
    'langp2_style' => 'width: 0%;',
    'lang3_style' => '',
    'langp4_style' => 'width: 0%;',
    'langp3_style' => 'width: 0%;',
    'lang4_style' => 'width: 0%;',
    
);

print_r(json_encode($newData));exit;
  }
  
  public function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }
  
  private function login()
  {
    if($this->getRequest()->isPost())
    {
      if (isset($_POST['login']))
      {
        $this->view->login = true;
        $auth = Zend_Auth::getInstance();
      
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
        
        $authAdapter->setTableName('translators')
              ->setIdentityColumn('email')
              ->setCredentialColumn('pass_tran');
              
        $email = $this->getRequest()->getPost('email');
        $password = $this->getRequest()->getPost('pass');
        
        $authAdapter->setIdentity($email)
                ->setCredential(md5($password));
                
        $result = $auth->authenticate($authAdapter);
        
        if ($result->isValid())
        {
          // используем адаптер для извлечения оставшихся данных о пользователе
          $identity = $authAdapter->getResultRowObject();
          
          if($identity->role == 4)
          {
            // получаем доступ к хранилищу данных Zend
            $authStorage = $auth->getStorage();
    
            // помещаем туда информацию о пользователе,
            // чтобы иметь к ним доступ при конфигурировании Acl
            $authStorage->write($identity);
    
            //$this->redirectUrl($identity->role);
            $this->redirector->gotoUrl($this->view->url(array("controller"=>"translator", "action"=>"index")));
            exit;
          }
          else
          {
            $this->view->error = true;
          }
          
        }
        else
        {
          $this->view->error = true;
        }
      }
      else if(isset($_POST['registration']))
      {
        $this->registration();
      }
    }
  }
  
  public function registration()
  {
    $this->view->registration = true;
    
    $error = false;
    
    $data = array();
    
    $data['email'] = $this->test_input($_POST['email']);
    $data['name_tran'] = $this->test_input($_POST['name_tran']);
    $data['last_tran'] = $this->test_input($_POST['last_tran']);
    $pass_tran = $this->test_input($_POST['pass_tran']);
    $confPass = $this->test_input($_POST['confpass']);
    $data['phon_tran'] = $this->test_input($_POST['phon_tran']);
    
    $code = $this->test_input($_POST['code']);

    if(empty($data['email'])){
        $error = true;
        $errorName['email'] = "The email was empty";
    }
    if(empty($data['name_tran'])){
        $error = true;
        $errorName['name_tran'] = "The name was empty";
    }
    if(empty($data['last_tran'])){
        $error = true;
        $errorName['last_tran'] = "The las was empty";
    }
    if(empty($pass_tran)){
        $error = true;
        $errorName['pass_tran'] = "The password name was empty";
    }
    if(empty($confPass)){
        $error = true;
        $errorName['confpass'] = "The confirm password was empty";
    }
    if(empty($data['phon_tran'])){
        $error = true;
        $errorName['phon_tran'] = "The phone number was empty";
    }
    if($pass_tran != $confPass){
        $error = true;
        $errorName['conf_i_pass'] = "Passwords are not the same";
    }
    
    if(empty($code))
    {
      $error = true;
      $errorName['code'] = "Enter the code for registration";
    }
    else
    {
      $objCode = new models_Code();

      if(!$objCode->getCode(0,$code))
      {
        $error = true;
        $errorName['code'] = "Wrong code";
      }
    }
          
    if($error){
        $this->view->error = true;
        $this->view->nameError = $errorName;
        $this->view->data = $data;
    }
    else
    {
      $model = new models_Translator();
    
      if(!$model->getTrEmail($data['email']))
      {
        $data['meta_data'] = json_encode(models_Model::templateData()) ;
        $data['pass_tran'] = md5($pass_tran);
        $data['way_tran'] = '';
        
        $model->insertTrans($data);
        
        $objCode->updateCode($code, array('status'=>0));
        
        $mess = 'Welcome '.$data['name_tran'].' '.$data['last_tran'].'!,<br><br>You have registered successfully as an agency at translated.co.<br><br>Your login: '.$data['email'].'<br>Your password: '.$pass_tran.'<br><br>Sincerely,<br>Translated.co team';
      
        $objMail = new models_Mailer();
        $objMail->doMail($data['email'], 'Registration for the service',$mess);
        
        $this->view->success = true;
      }
      else
      {
        $this->view->nameError = array('email_exist'=>"The Email already exists") ;
        $this->view->error = true;
        $this->view->data = $data;
      }
    }
    
    
  }
  
}

