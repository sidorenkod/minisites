<?php

class LoginController extends Zend_Controller_Action
{

  public function init()
  {
    $this->_helper->layout()->setLayout('empty');
    $this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    
    if(Zend_Auth::getInstance()->hasIdentity() && Zend_Auth::getInstance()->getIdentity()->role == 10)
    {
      header('Location: /admin');
    }
  }

  public function indexAction()
  {
    $this->login();
  }
  
  private function login()
  {
    if($this->getRequest()->isPost())
    {
      //print_r($_POST);exit;
      $auth = Zend_Auth::getInstance();
      
      $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter());
      
      $authAdapter->setTableName('admins')
            ->setIdentityColumn('user_adm')
            ->setCredentialColumn('pass_adm');
            
      $email = $this->getRequest()->getPost('user');
      $password = $this->getRequest()->getPost('pass');
      
      $authAdapter->setIdentity($email)
              ->setCredential(md5($password));
              
      $result = $auth->authenticate($authAdapter);
      
      if ($result->isValid())
      {
        // ���������� ������� ��� ���������� ���������� ������ � ������������
        $identity = $authAdapter->getResultRowObject();
        
        if($identity->role == 10)
        {
          // �������� ������ � ��������� ������ Zend
          $authStorage = $auth->getStorage();
  
          // �������� ���� ���������� � ������������,
          // ����� ����� � ��� ������ ��� ���������������� Acl
          $authStorage->write($identity);
  
          //$this->redirectUrl($identity->role);
          header('Location: /admin');
          exit;
        }
        else
        {
          $this->view->error = true;
        }
      }
      else
      {
        $this->view->error = true;
      }
    }
  }
}