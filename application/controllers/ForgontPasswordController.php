<?php

class ForgontPasswordController extends Zend_Controller_Action
{

  public function init()
  {
    $this->_helper->layout()->setLayout('index');
    $this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    if (session_id()=='') session_start();
  }

  public function indexAction()
  {
    if(!empty($_POST))
    {
      $email = $this->getRequest()->getPost('email');
      
      $objTran = new models_Translator();
      
      $trans = $objTran->getTrEmail($email);
      
      if($trans)
      {
        $mess = 'Welcome '.$trans['name_tran'].'!<br><br>To reset your password please <a href="http://translated.co/forgont-password/reset?g='.$trans['id'].'_'.$trans['pass_tran'].'" target="_blank">click here</a><br><br>Sincerely,<br>Translated.co team';
      
        $objMail = new models_Mailer();
        $objMail->doMail($email, 'Password restore request',$mess);
      }
      
      $this->view->result = true;
    }
  }
  
  public function resetAction()
  {
    if(isset($_GET['g']))
    {
      $objTran = new models_Translator();
      
      $arr = explode('_',$_GET['g']);
      
      $trans = $objTran->transIdPass($arr[0], $arr[1]);
      
      if(!empty($_POST))
      {
        $id = $this->getRequest()->getPost('id');
        
        $pass = $this->getRequest()->getPost('pass');
        $confPass = $this->getRequest()->getPost('confpass');
        
        if($pass == $confPass)
        {
          $objTran->updateTrans($id, array('pass_tran'=>md5($pass)));
          $this->view->text = 'Your password has been changed';
        }
        else
        {
          $this->view->text = 'Passwords do not match';
        }
      }
      
      
      
      if($trans != false)
      {
        $this->view->result = true;
        $this->view->id = $trans['id'];
      }
      else
      {
        $this->view->result = false;
      }
      
    }
    else
    {
      $this->view->result = false;
    }
  }
}