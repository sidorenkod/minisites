<?php

class UploadController extends Zend_Controller_Action
{
  public function init()
  {
    
  }

  public function indexAction()
  { 
    $_SESSION['email'] = '';
    
    $subDomain = models_Model::getSubdomain();

    // let's try to search by host name first
    $objOrdDom = new models_OrderDomains();
    list($host,)=explode(':',$_SERVER['HTTP_HOST']);
    $data = $objOrdDom->getEmailOrdDomain($host);
    //print_r($host);exit;
    if ($data){
      $_SESSION['email'] = $data['email_r'];
    }else if($subDomain)
    {
      $data = $objOrdDom->getEmailOrdDomain($subDomain);
      //print_r($data);exit;
      $_SESSION['email'] = $data['email_r'];
    }
    
    $_GET['upload'] = true;
    
    
    //var_dump($subDomain);exit;
    
    $_SESSION['email'] = $data['email_r'];

    $templates = new models_Templates();
    
    if($templates->checkTemplate())
    {
      $temp_id = $templates->getTemId();
      //print_r($temp_id);exit;
      $this->_helper->layout()->setLayout('templates/template-3');
      
      $objTempl = new models_TemplOther();
      
      //print_r($trans);exit;
      
      $objTrans = new models_Translator();
      
      $trans = $objTrans->getTrEmail($data['email_r']);
      
      $otherTempl = $objTempl->getTemp($trans['site_lang_id']);
      
      //print_r(json_decode($otherTempl['data'], true));exit;
      
      $this->view->siteLang = $trans['site_lang_id'];
      
      $this->view->data = (!$otherTempl) ? false : json_decode($otherTempl['data'], true);
      
      $this->view->templId = $trans['templ_id'];
      
      $objLang = new models_Languages();
        
      $this->view->siteLangCode = $objLang->getLang($trans['site_lang_id']);
      
      $objExp = new models_Expertises();
      
      $exp = $objExp->getExpert(0, $trans['site_lang_id']);
      
      //print_r($exp);exit;
      
      if(empty($exp))
      {
        $exp = $objExp->getExpert(0, 902);
      }
      
      $this->view->exp = $exp;
      //print_r($this->view->data );exit;
      
      if(!empty($_FILES))
      {
        $this->upload($data['email_r']);
      }
      
      if(!empty($_POST))
      {
        $this->addorder($data['email_r']);
      }
    }
    else
    {
      echo 'Site does not exist';
      exit;
    }
    
  }
  
  private function upload($email)
  { 
    $objFile = new models_File();
    
    $res = $objFile->uploadFile($email);
    
    echo json_encode(array('reuslt'=>$res,'order_id'=>$_SESSION['order_id']));
    
    exit;
  }
  
  private function addorder($email)
  { 
    $objOrder = new models_Order();
    
    $objTrans = new models_Translator();
    
    $trans = $objTrans->getTrEmail($email);
    
    $data['trans_id'] = $trans['id'];
    
    $data['lang_id'] = $this->getRequest()->getPost('from_lang');
    $data['dest_lang_id'] = $this->getRequest()->getPost('fdest_lang');
    $data['expertise'] = $this->getRequest()->getPost('fexpertise');
    $data['note'] = $this->getRequest()->getPost('note');
    $data['date_order'] = date("Y-m-d H:i:s");
    
    $_SESSION['order_id'] = $objOrder->insertOrder($data);
    
    echo json_encode(array('reuslt'=>true,'order_id'=>$_SESSION['order_id']));
    
    exit;
  }
}