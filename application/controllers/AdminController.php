<?php

class AdminController extends Zend_Controller_Action
{

  public function init()
  {
    $this->_helper->layout()->setLayout('admin');
    //$this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    
    if(!Zend_Auth::getInstance()->hasIdentity() || Zend_Auth::getInstance()->getIdentity()->role != 10)
    {
      header('Location: /login');
    }
  }
  
  /** method logout */
  public function logoutAction()
  {
    $this->_helper->layout->disableLayout();

    Zend_Auth::getInstance()->clearIdentity();

    header('Location: /login');
    exit;
  }
 
/** method Index */
  public function indexAction()
  {
    $model = new models_Model();
    
    $this->view->nt = $model->sqlQueryCount('SELECT email from translators;');
    
    $this->view->na = $model->sqlQueryCount('SELECT agency_id from agencys;');
    
    $this->view->np = $model->sqlQueryCount('SELECT plan_id from plans;');
    
    $this->view->ne = $model->sqlQueryCount('SELECT expertise_id from expertises;');
    
    $this->view->nd = $model->sqlQueryCount('SELECT domain_id from domains;');
    
    $this->view->nl = $model->sqlQueryCount('SELECT lenguage_id from languages;');
    
    $this->view->no = $model->sqlQueryCount('SELECT order_plan_id from order_plans;');
  }
  
  public function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
  }
  
  /** method translator - view all, one, delete, add, update*/
  public function translatorsAction()
  {
    $model = new models_Model();
    
    if(!empty($_POST))
    {
      $data['email'] = $this->test_input($_POST['ema']);
      $data['name_tran'] = $this->test_input($_POST['nam']);
      $data['last_tran'] = $this->test_input($_POST['las']);
      $data['phon_tran'] = $this->test_input($_POST['pho']);
      
      if(empty($data['email']))
      {
          $errorName = 'Email is empty';
      }
      else if(empty($data['name_tran']))
      {
          $errorName = 'Price is empty';
      }
      else if(empty($data['last_tran']))
      {
          $errorName = 'Last name is empty';
      }
      else if(empty($data['phon_tran']))
      {
          $errorName = 'Phone Number is empty';
      }

      if(isset($errorName)){
          header("location: /admin/translators?rsp=re_er_translator&msj=$errorName");
          exit;
      }      
      
      $data['way_tran'] = '';
      
      $pas = $this->test_input($_POST['pas']);
        
      if($pas != '')
      {
        $data['pass_tran'] = md5($pas);
      }
      
      if(isset($_POST['btg']))
      {
        $objTrans = new models_Translator();
        
        if(!$objTrans->getTrEmail($data['email']))
        {
          $cod = $objTrans->insertTrans($data);
          header("location: /admin/translators?form&ver=$cod&rsp=re_ok_translator");
        }
        else
        {
          header("location: /admin/translators?rsp=translator_exist");
        }
      }
      else if(isset($_POST['btc']))
      {
        $id = (int)$_POST['id'];
        
        if($id != 0)
        {
          $objTrans = new models_Translator();
          $objTrans->updateTrans($id, $data);
          
          header("location: /admin/translators&?form&ver=$id&rsp=ed_ok_translator");
          
        }
        
        header("location: /admin/translators&?form&ver=$id&rsp=no_ed_translator");
      }
    }
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->tran = $model->sqlQuery2('SELECT * FROM translators WHERE id='.$cod.';');
      
    }
    else if(isset($_GET['ele']))
    {
      $cod = (int)$_GET['ele'];
      
      if($cod != 0)
      {
        $model->sqlQuery('DELETE FROM `translators` WHERE  `id`='.$cod.';');
      }
     
      header("location: /admin/translators");
     
    }
    else
    {
      $this->view->trans = $model->sqlQuery2('SELECT * FROM translators ORDER BY name_tran DESC;');
    }
  }
  
  /** method Agensys - view all, one*/
  public function agencysAction()
  {
    $model = new models_Model();
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      $this->view->ag = $model->sqlQuery2("SELECT * FROM agencys WHERE agency_id='$cod';", true);
    }
    else
    {
      $this->view->ag = $model->sqlQuery2('SELECT * FROM agencys ORDER BY agency_id DESC;');
    }
  }
  
  public function agensyClientsAction()
  {
    $cod = (int)$_GET['id'];
    $model = new models_Model();
    $ag = $model->sqlQuery2("SELECT * FROM agencys WHERE agency_id='$cod';", true);
    
    $objTrans = new models_Translator();
    $transData = $objTrans->getTrEmail($ag['email_r']);
    
    $objCust = new models_Customers();
    
    $custData = $objCust->getCust(null,0,$transData['id']);
      
    models_Model::createExel($custData);
    exit;
  }
  
  /** method Plans - view all, one, delete, add, update*/
  public function plansAction()
  {
    $objPl = new models_Plans();
    
    if(!empty($_POST))
    {
      $data['name_plan'] = $this->test_input($_POST['nam']);
      $data['desc_plan'] = $this->test_input($_POST['des']);
      $data['quan_plan'] = $this->test_input($_POST['com']);
      $data['price_plan'] = $this->test_input($_POST['pri']);
      $data['expe_plan'] = $this->test_input($_POST['exp']);
      

      if(isset($errorName)){
          header("location: /admin/plans?rsp=re_er_translator&msj=$errorName");
          exit;
      }
      
      if(isset($_POST['btg']))
      {
        
        $cod = $objPl->insertPlans($data);
        header("location: /admin/plans?form&ver=$cod&rsp=re_ok_plans");
      }
      else if(isset($_POST['btc']))
      {
        $id = (int)$_POST['id'];
        
        if($id != 0)
        {
          $objPl->updatePlans($id, $data);
          
          header("location: /admin/plans&?form&ver=$id&rsp=ed_ok_translator");
          
        }
        
        header("location: /admin/plans&?form&ver=$id&rsp=no_ed_translator");
      }
    }
    
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->plf = $objPl->getPlans($cod);
    }
    else if(isset($_GET['ele']))
    {
      $cod = (int)$_GET['ele'];
      
      $objPl->deletePlans($cod);
      
      header('Location: /admin/plans');
    }
    else
    {
      $this->view->pl = $objPl->getPlans();
    }
    
  }
  
  /** method expertise - view all, one, delete, add, update*/
  public function expertisesAction()
  {
    $model = new models_Expertises();
    
    if(!empty($_POST))
    {
      $data['desc_expe'] = $this->test_input($_POST['nam']);
      

      if(isset($errorName)){
          header("location: /admin/expertises?rsp=re_er_translator&msj=$errorName");
          exit;
      }
      
      if(isset($_POST['btg']))
      {
        
        $cod = $model->insertExp($data);
        header("location: /admin/expertises?form&ver=$cod&rsp=re_ok_plans");
      }
      else if(isset($_POST['btc']))
      {
        $id = (int)$_POST['id'];
        
        if($id != 0)
        {
          $model->updateExp($id, $data);
          
          header("location: /admin/expertises&?form&ver=$id&rsp=ed_ok_translator");
          
        }
        
        header("location: /admin/expertises&?form&ver=$id&rsp=no_ed_translator");
      }
    }
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->exp = $model->getExpert($cod);
      
    }
    else if(isset($_GET['ele']))
    {
      $cod = (int)$_GET['ele'];
      
      $model->deleteExp($cod);
      
      header('Location: /admin/expertises');
    }
    else if(!isset($_GET['form']))
    {
      $this->view->exp = $model->getExpert();
    }
  }
  
  /** method Domain - view all, one, delete, add, update*/
  public function domainsAction()
  {
    $model = new models_Domains();
    
    if(!empty($_POST))
    {
      if(isset($_POST['set_lang']))
      {
        //print_r($_POST);exit;
        
        if(isset($_POST['id']))
        {
          $ids = '';
          foreach($_POST['id'] as $k=>$v)
          {
            $ids .= (int)$v;
            $ids .= ',';
          }
          
          $ids = trim($ids,',');
          
          $data['lang_id'] = $_POST['lang'];
          
          $model->updateDomLang($ids,$data);
        }
        
        header("location: /admin/domains");
        exit;
      }
      
      $data['name_dom'] = $this->test_input($_POST['nam']);
      $data['price_dom'] = $this->test_input($_POST['pri']);
      $data['stat_dom'] = $this->test_input($_POST['sta']);
      

      if(isset($errorName)){
          header("location: /admin/domains?rsp=re_er_translator&msj=$errorName");
          exit;
      }
      
      if(isset($_POST['btg']))
      {
        if(!$model->chekDomain($data['name_dom']))
        {
          $cod = $model->insertDom($data);
        }
        
        header("location: /admin/domains?form&ver=$cod&rsp=re_ok_plans");
      }
      else if(isset($_POST['btc']))
      {
        $id = (int)$_POST['id'];
        
        if($id != 0)
        {
          $model->updateDom($id, $data);
          
          header("location: /admin/domains&?form&ver=$id&rsp=ed_ok_translator");
          
        }
        
        header("location: /admin/domains&?form&ver=$id&rsp=no_ed_translator");
      }
    }
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->dom = $model->getDomain($cod);
    }
    else if(isset($_GET['bt_set_price']))
    {
      $price = (int)$_GET['price'];
      $type = $_GET['type'];
      
      $this->view->dom = $model->getDomainType($type, $price);
    }
    else if(isset($_GET['setlang']))
    {
      $modLang = new models_Languages();
      
      $this->view->langs =  $modLang->getLang2();
      
      $this->view->doms = $model->getDomain();
      
      $this->_helper->viewRenderer('domains-setlang');
    }
    else if(isset($_GET['ele']))
    {
      $cod = (int)$_GET['ele'];
      
      if($cod != 0)
      {
        $model->deleteDom($cod);
      }
      
      header('Location: /admin/domains');
    }
    else if(!isset($_GET['form']))
    {
      $this->view->dom = $model->getDomain();
    }
  }
  
  /** method domainsImport - insert new domain*/
  public function domainsImportAction()
  {
    if(!empty($_FILES))
    {
      $model = new models_Domains();
      $model->importDomain();
      header('Location: /admin/domains');
    }
  }
  
  /** method extension - view all, one, delete, add, update*/
  public function extensionAction()
  {
    $model = new models_Model();
    
    if(!empty($_POST))
    {
      $data['extension'] = $this->getRequest()->getPost('extension');
      $data['price'] = $this->getRequest()->getPost('price');
      $data['ext_desc'] = $this->getRequest()->getPost('ext_desc');
      
      $objExtDom = new models_ExtensionDomain();
      
      if(isset($_POST['update']))
      {
        $id = (int)$this->getRequest()->getPost('id');

        $objExtDom->updateExtDom($id, $data);
      }
      else
      {
        $objExtDom->insertExtDom($data);
      }
      
      header('Location: /admin/extension');exit;
    }
    
    if(isset($_GET['ele']))
    {
      $id = (int)$_GET['ele'];
      
      if($id != 0)
      {
        $model->sqlQuery('DELETE FROM `minisites`.`extension_domain` WHERE  `id`='.$id.';');
      }
      
      header('Location: /admin/extension');exit;
    }
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->ext = $model->sqlQuery2('SELECT * FROM extension_domain WHERE id = '.$cod.';', true);
      
    }
    else if(!isset($_GET['form']))
    {
      $this->view->exts = $model->sqlQuery2('SELECT * FROM extension_domain ORDER BY id DESC;');
    }
  }
  
  
  /** method languages - view all, one, delete, add, update*/
  public function languagesAction()
  {
    $model = new models_Languages();
    
    if(!empty($_POST))
    {
      $data['name_leng'] = $this->test_input($_POST['nam']);
      $data['group_id'] = (int)($_POST['idg']);
      

      if(isset($errorName)){
          header("location: /admin/domains?rsp=re_er_translator&msj=$errorName");
          exit;
      }
      
      if(isset($_POST['btg']))
      {
        
        $cod = $model->insertLang($data);
        header("location: /admin/languages?form&ver=$cod&rsp=re_ok_plans");
      }
      else if(isset($_POST['btc']))
      {
        $id = (int)$_POST['id'];
        
        if($id != 0)
        {
          $model->updateLang($id, $data);
          
          header("location: /admin/languages&?form&ver=$id&rsp=ed_ok_translator");
          
        }
        
        header("location: /admin/languages&?form&ver=$id&rsp=no_ed_translator");
      }
    }
    
    if(isset($_GET['form']))
    {
      $modelGr = new models_Model();
      
      $this->view->group = $modelGr->sqlQuery2('SELECT * FROM group_languages ORDER BY name_group;');
    }
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->lang = $model->getLang($cod);
      
    }
    else if(isset($_GET['ele']))
    {
      $id = (int)$_GET['ele'];
      
      if($id != 0)
      {
        $model->deleteLang($id);
      }
      
      header('Location: /admin/languages');exit;
    }
    else if(!isset($_GET['form']))
    {
      $this->view->langs = $model->getLang();
    }
  }
  
  /** method groups - view all, one, delete, add, update*/
  public function groupsAction()
  {
    $model = new models_GroupLang();
    
    if(!empty($_POST))
    {
      if(isset($_POST['bt_set_price']))
      {
        $type = $this->test_input($_POST['type']);
        $price = $_POST['price'];
        
        $model->setPrice($type, $price);
        
        //print_r($_POST);exit;
        
        header('Location: /admin/groups');exit;
      }
      
      $data['name_group'] = $this->test_input($_POST['nam']);
      $data['price_group'] = ($_POST['pri']);
      

      if(isset($errorName)){
          header("location: /admin/groups?rsp=re_er_translator&msj=$errorName");
          exit;
      }
      
      if(isset($_POST['btg']))
      {
        
        $cod = $model->insertGr($data);
        header("location: /admin/groups?form&ver=$cod&rsp=re_ok_plans");
      }
      else if(isset($_POST['btc']))
      {
        $id = (int)$_POST['id'];
        
        if($id != 0)
        {
          $model->updateGr($id, $data);
          
          header("location: /admin/groups&?form&ver=$id&rsp=ed_ok_translator");
          
        }
        
        header("location: /admin/groups&?form&ver=$id&rsp=no_ed_translator");
      }
    }
    
    if(isset($_GET['ver']))
    {
      $cod = (int)$_GET['ver'];
      
      $this->view->grl = $model->getGr($cod);
    }
    else if(isset($_GET['ele']))
    {
      $id = (int)$_GET['ele'];
      
      if($id != 0)
      {
        $model->deleteGr($id);
      }
      
      header('Location: /admin/groups');exit;
    }
    else if(!isset($_GET['form']))
    {
      $this->view->gr_lang = $model->getGr();
    }
  }
  
  public function codeAction()
  {
    if(!empty($_POST))
    {
      $data['code'] = $this->getRequest()->getPost('code');
      $data['status'] = $this->getRequest()->getPost('status');
      
      $objCode = new models_Code();
      $objCode->insertCust($data);
      
      header('Location: /admin/code');exit;
    }
    
    if(isset($_GET['generate']))
    {
      $mtime = explode(" ", microtime());

      $this->view->code = md5($mtime[1] + $mtime[0]);
      
      $this->_helper->viewRenderer('code-generate');
    }
    else
    {
      $objCode = new models_Code();
    
      $this->view->codes = $objCode->getCode();
    }
  }
  
  public function macrosAction()
  {
    if(!empty($_POST))
    {
      $lang_id = (int)$_POST['lang_id'];
      $macrosArr = $_POST['macros'];
      
      $macros = json_encode($macrosArr);
      
      if($macros != '')
      {
        $objTO = new models_TemplOther();
        
        if(!$objTO->getTemp($lang_id))
        {
          $objTO->insertTempO(array('lang_id'=>$lang_id,'data'=>$macros));
        }
        else
        {
          $objTO->updateTempO($lang_id, array('data'=>$macros));
        }
      }
      
      header('Location: /admin/macros');
      
    }
    
    $id = (isset($_GET['id']))?(int)$_GET['id']:0;
    
    $objLang = new models_Languages();
    $this->view->langs = $objLang->getLangMacros($id);
    
    if($id != 0)
    {
      $this->view->macros = json_decode($this->view->langs['data'],true); 
          
      $default = $objLang->getLangMacros(902);
      $this->view->default = json_decode($default['data'],true);
      $this->_helper->viewRenderer('macros-edit');
    }
  }
}