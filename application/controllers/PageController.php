<?php

class PageController extends Zend_Controller_Action
{
  public function init()
  {
    $this->redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
  }

  public function indexAction()
  {
    $_SESSION['email'] = '';
    
    $subDomain = models_Model::getSubdomain();

    // let's try to search by host name first
    $objOrdDom = new models_OrderDomains();
    list($host,)=explode(':',$_SERVER['HTTP_HOST']);
    $data = $objOrdDom->getEmailOrdDomain($host);
    //print_r($host);exit;
    if ($data){
      $_SESSION['email'] = $data['email_r'];
    }else if($subDomain)
    {
      $data = $objOrdDom->getEmailOrdDomain($subDomain);
      //print_r($data);exit;
      $_SESSION['email'] = $data['email_r'];
    }
    
    $_GET['temp_id'] = 3;
    
    //print_r($data);exit;
    
    $_SESSION['email'] = $data['email_r'];

    $templates = new models_Templates();
    
    if($templates->checkTemplate())
    { 
      $this->_helper->layout()->setLayout('templates/page');
      
      $id = (int)$this->getRequest()->getParam('id');
      
      if($id != 0)
      {
        $objOrder = new models_Order();
      
        $orderData = $objOrder->getOrder($id);
        
        if($orderData)
        {
          $objCust = new models_Customers();
        
          $this->view->custData = $objCust->getCust(null, $orderData['customer_id']);
          $this->view->orderData = $orderData;
          
          $objAgensy = new models_Agencys();
          
          $this->view->transData = $objAgensy->getAgencys($data['email_r']);
          
          $objTrans = new models_Translator();
      
          $trans = $objTrans->getTrEmail($data['email_r']);
          
          $this->view->siteLang = $trans['site_lang_id'];
          
          $objLang = new models_Languages();
        
          $this->view->siteLangCode = $objLang->getLang($trans['site_lang_id']);
          
        }
        else
        {
          $this->view->error = 'not order';
        }
      }
      else
      {
        $this->view->error = 'not order';
      }
      
      
      
    }
    else
    {
      echo 'Site does not exist';
      exit;
    }
    
  }
}