<?php

class models_Domains extends Zend_Db_Table_Abstract
{
  protected $_name = 'domains';
  
  public function getDomain($domain_id=0)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

      $select->where('stat_dom =  ?', 'AVAILABLE');
      
      if($domain_id != 0)
      {
        $select->where('domain_id =  ?', $domain_id);
        
        return $this->getAdapter()->fetchRow($select); // if not row - result = false
      }
      
      $select->order(array('domain_id DESC'));
      
    return $this->getAdapter()->fetchAll($select);
  }
  
  public function getDomainType($type, $price=0)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
            
    $select->where('stat_dom =  ?', 'AVAILABLE');
    
    $select->where('name_dom LIKE ?', '%'.$type);
      
    if($price != 0)
    {
      $select->where('price_dom =  ?', $price);   
    }
    
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchAll($select); 
  }
  
  public function chekDomain($domain_name)
  {
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('name_dom =  ?', $domain_name);
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false

  }
  
  public function importDomain()
  {
    if(!empty($_FILES))
    {
      $tempFile = $_FILES['file']['tmp_name'];
      
      $storeFolder = realpath(APPLICATION_PATH . "/../public/uploads/");

      if (!is_dir($storeFolder)){ mkdir($storeFolder,0777); }
      
      //print_r($storeFolder);exit;
      
      $pathFile = $storeFolder .'/temp.txt';
      
      if (move_uploaded_file($tempFile, $pathFile))
      {
        //print_r($pathFile);exit;
        $this->import($pathFile);
        
        unlink($pathFile);
      }
    }
  }
  
  private function import($pathFile)
  {
    $lines = file($pathFile);
    $i = 1;
    $value = '';
    $domains = $this->chekDomain();
    
    $chek = true;
    $arrDom = array();
    
    //print_r($lines);exit();
    
    foreach($lines as $line)
    {
      $line = trim($line);
      foreach($domains as $dom)
      {
        if($dom['name_dom'] ==  $line)
        {
          $chek = false;
          break;
        }
      }
      
      if(!empty($arrDom))
      {
        foreach($arrDom as $adom)
        {
          if($adom ==  $line)
          {
            $chek = false;
            break;
          }
        }
      }
      
      if($chek)
      {
        $value .= '("'.$line.'", "AVAILABLE"),';
        $arrDom[] = $line;
      }

      if($i==100 && $value != '')
      {
        $value = trim($value,',');
        $sql = "INSERT INTO domains (`name_dom`,`stat_dom`) VALUES $value ";
        $this->getAdapter()->query($sql);
        $value = '';
      }
      
      $i++;
      $chek = true;
    }
    
    if($value != '')
    {
      $value = trim($value,',');
    
      $sql = "INSERT INTO domains (`name_dom`,`stat_dom`) VALUES $value ";
      
      //print_r($sql);exit;
      
      $this->getAdapter()->query($sql);
    }

    //print_r($lines);exit;
  }
  
  public function insertDom($data)
  {
    return $this->insert($data);
  }
  
  public function updateDomLang($id, $data)
  {
    $where[] = "domain_id in ($id)";
    $this->update($data, $where);
  }
  
  public function updateDom($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('domain_id = ?', $id);
    $this->update($data, $where);
  }
  
  public function deleteDom($id)
  {
    $where = $this->getAdapter()->quoteInto('domain_id = ?', $id);
    $this->delete($where);
  }
}