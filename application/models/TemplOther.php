<?php

class models_TemplOther extends Zend_Db_Table_Abstract
{
  protected $_name = 'templates_other';
  
  public function getTemp($lang_id)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('lang_id =  ?', $lang_id);
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function insertTempO($data)
  {
    return $this->insert($data);
  }
  
  public function updateTempO($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('lang_id = ?', $id);
    $this->update($data, $where);
  }
}