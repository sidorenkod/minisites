<?php

class models_Tranzila
{
    public static function charge($sum, $ccno, $expmonth, $expyear, $mycvv, $first_name, $last_name, $email)
    {
        if (session_id() == '') session_start();
        $sess_id = session_id();

        // credit card no
        $ccno = trim(str_replace("-", "", $ccno));

        $host = 'secure5.tranzila.com'; // gateway host
        $path = '/cgi-bin/tranzila71u.cgi'; // gateway uri
        $formdata['supplier']='smartlation'; // supplier
        $formdata['sum']=$sum; // total amount to process
        $formdata['currency']= 2; // 2- Dollar
        $formdata['ccno']=$ccno; // credit card
        $formdata['expmonth']=$expmonth; // expiration month
        $formdata['expyear']=$expyear; // expiration year
        $formdata['mycvv']=$mycvv; // CVV
        $formdata['company']=$email;
        $formdata['first_name']=$first_name;
        $formdata['last_name']=$last_name;
        $formdata['TranzilaPW']='P65f4QA';
        $poststring = '';

        // formatting the request string
        foreach($formdata AS $key => $val) {
            $poststring .= $key . "=" . $val . "&";
        }

        // strip off trailing ampersand
        $poststring = substr($poststring, 0, -1);

        // init curl connection
        $CR = curl_init();
        curl_setopt($CR, CURLOPT_URL, "https://".$host.$path);
        curl_setopt($CR, CURLOPT_POST, 1);
        curl_setopt($CR, CURLOPT_FAILONERROR, true);
        curl_setopt($CR, CURLOPT_POSTFIELDS, $poststring);
        curl_setopt($CR, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($CR, CURLOPT_SSL_VERIFYPEER, 0);

        // actual curl execution perform
        $result = curl_exec( $CR );
        $error = curl_error( $CR );
        curl_close( $CR );

        if( !empty( $error )) {
            return array(0, $error);
        }

        // re-format the string into array
        $response =explode('&', $result);
        foreach($response as $key=>$value) {
            unset($tmparr);
            $tmparr=explode("=",$value);
            $answer[$tmparr[0]]=$tmparr[1];
        }

        if(array_key_exists("Response", $answer))
        {
            if($answer['Response']=="000") {
                //AUTHORIZED
                return array (1, 0);
            }else{
                //NOT AUTHORIZED
                return array(0, $answer['Response']);
            }
        }else{
            return array(0, $result);
        }
    }

}

?>