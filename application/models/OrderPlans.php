<?php

class models_OrderPlans extends Zend_Db_Table_Abstract
{
  protected $_name = 'order_plans';
  
  public function getPlans($email)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

      $select->where('email =  ?', $email);

    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function getPrice($email, $lang_1, $lang_2)
  {    
    $select = $this->getAdapter()->select()
            ->from(array('op'=>$this->_name), '');
    $select->joinLeft('details_language_order_plan as dl','dl.order_plan_id = op.order_plan_id',array('dl.price'));

    $select->where('email = ?', $email);
    $select->where('leng_id_1 = ?', $lang_1);
    $select->where('leng_id_2 = ?', $lang_2);
      
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
}