<?php

class models_Code extends Zend_Db_Table_Abstract
{
  protected $_name = 'code';
  
  public function getCode($id=0, $code=null)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
      
      if(!is_null($code))
      {
        $select->where('code =  ?', $code);
        //echo (string)$select;exit;
        return $this->getAdapter()->fetchRow($select); // if not row - result = false
      }
      else if($id != 0)
      {
        $select->where('id =  ?', $id);
        return $this->getAdapter()->fetchRow($select); // if not row - result = false
      }
      
    return $this->getAdapter()->fetchAll($select);
  }
  
  
  public function insertCust($data)
  {
    return $this->insert($data);
  }
  
  public function updateCode($code, $data)
  {
    $where = $this->getAdapter()->quoteInto('code = ?', $code);
    $this->update($data, $where);
  }
}