<?php

class models_Templates extends Zend_Db_Table_Abstract
{
  
  protected $_name = 'templ_translator';
  
  private $data = array();
  
  private $meta = array();
  
  //private $path_def = '/templates/templates_users/';
  private $path_def = '/templates/templates_users/';
  
  private $user_id = 1;
  
  private $temp_id = null;
  
  private $transData = null;
  
  public function __construct()
  {
    
    parent::__construct();
    
    $this->transData =  $this->getTranslData();
    
    //print_r($this->transData);exit;

    $this->temp_id = $this->transData['templ_id'];


    $this->getJson($this->transData['meta_data']); 
    
  }
  
  /** get default templates */
  private function getDataTemplate($id)
  {
    $select = $this->getAdapter()->select()
              ->from('templates');

    $select->where('id =  ?', $id);   

    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  /** update template translator */
  public function updateTempl($trans_id, $data, $templ_id=null, $status = null)
  {
    
    $where[] = 'trans_id = '.$trans_id;
    
    if(!is_null($templ_id))
    {
      $where[] = 'templ_id = '.$templ_id;
    }
    else if(!is_null($status))
    {
      $where[] = 'status = 1';
    }
    
    $this->update($data, $where);
  }
  
  /** insert template translator (default data) */
  public function insertTempl($data)
  {
    return $this->insert($data);
  }
  
  /** change template */
  public function changeTemplate()
  {
    if(isset($_POST['temp']))
    {
      $templ_id = (int)$_POST['temp'];
      
      //print_r($this->transData);exit;
      
      $this->temp_id = $templ_id;
      
      $this->updateTempl($this->transData['id'], array('status'=>0));
        
      if($this->getTemplate($this->transData['id'], $templ_id))
      { 
        $this->updateTempl($this->transData['id'], array('status'=>1), $templ_id);
      }
      else
      {
        $data_templ = $this->getDataTemplate($templ_id);
        
        if(!is_null($this->transData['logo']))
        {
          $json_data = json_decode($data_templ['data'],true);
          
          $json_data['logo'] = '/templates/templates_users/'.$this->transData['id'].'/'.$this->transData['logo'];
          
          $data_templ['data'] = json_encode($json_data);
        }
        
        $data = array(
            'trans_id'=>$this->transData['id'],
            'status'=>1,
            'templ_id'=>$templ_id,
            'json_data' => $data_templ['data']
          );
        $this->insertTempl($data);
      }
      
      if(!$this->getTemplate($this->transData['id'], 3) && $templ_id != 3)
      {
        $data_templ = $this->getDataTemplate(3);
        
        if(!is_null($this->transData['logo']))
        {
          $json_data = json_decode($data_templ['data'],true);
          
          $json_data['logo'] = '/templates/templates_users/'.$this->transData['id'].'/'.$this->transData['logo'];
          
          $data_templ['data'] = json_encode($json_data);
        }
        
        $data = array(
            'trans_id'=>$this->transData['id'],
            'status'=>0,
            'templ_id'=>3,
            'json_data' => $data_templ['data']
          );
        $this->insertTempl($data);
      }
      
      
      $model = new models_Model();
      
      $this->createFolder();
    }
  }
  
  private function getMetaData()
  {
    $objTrans = new models_Translator();
    
    $result = $objTrans->getTrEmail(models_Model::getTransEmail());
    
    $this->meta = $result['meta_data'];
  }
  
  
  
  public function getTemplate($trans_id, $templ_id=null, $all=false)
  {
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('trans_id =  ?', $trans_id);
    
    if(isset($_GET['upload']))
    {
      $select->where('templ_id = 3');
    }
    else if(isset($_GET['temp_id']))
    {
      $select->where('templ_id = ?',(int)$_GET['temp_id']);
    }
    else if(!is_null($templ_id))
    {
      $select->where('templ_id = ?', $templ_id);
    }
    else if(!$all)
    {
      $select->where('status = 1');
    }
    
    //echo (string)$select;exit;
    if($all)
    {
      return $this->getAdapter()->fetchAll($select);
    }
    
    return $this->getAdapter()->fetchRow($select);
  }
  
  
  public function checkTemplate()
  {
    if(is_null($this->temp_id))
    {
      return false;
    }
    
    return true;
  }
  
  public function getPath($upload=null)
  {
    if(is_null($upload))
    {
        return '/templates/template_'.$this->temp_id;
    }
    else
    {
        return '/templates/template_'.$upload;
    }
    
    //return '/templates/template_'.$this->temp_id;
  }
  
  public function checkTempId()
  {
    return (is_null($this->temp_id)) ? false : true;
  }
  
  /** get json */
  private function getJson($json)
  {
    $this->data = json_decode($json, true);
  
    //print_r($this->data);exit;
  }
  
  private function setJson()
  {    
    $data['meta_data'] = json_encode($this->data);

    
    $objTrans = new models_Translator();
    
    $objTrans->updateTrans($this->transData['id'], $data);
    //print_r($data);exit;
    //$this->updateTempl($this->transData['id'], $data, $templ_id, 1);
  }
  
  private function getTranslData()
  {
    $objTrans = new models_Translator();
    
    return $objTrans->getTrEmail(models_Model::getTransEmail());
  }
  
  public function getData($key, $default=null)
  {
    if(isset($this->data[$key]))
    {
      return $this->data[$key];
    }
    
    return $default;
  }
  
  public function getMeta($key, $default=null)
  {
    if(isset($this->data[$key]))
    {
      return $this->data[$key];
    }
    
    return $default;
  }
  
  public function chekData($key)
  {
    return (isset($this->data[$key]) && $this->data[$key]==true) ? true : false;
  }
  
  public function getDataImages($key)
  {
    if(!empty($this->data[$key]))
    {
      $res = '';
      foreach($this->data[$key] as $k)
      {
        $res .= '<img src="'.$k.'" width="100"><br>';
      }
      
      return $res;
    }
    
    return '';
  }
  
  public function savePost()
  {
    //print_r($_POST);exit;
    foreach($_POST as $k=>$v)
    {
      $this->data[$k]=$v;
    }
    //print_r($this->data[$k]);exit;
    
    $this->setJson();
  }
  
  
  private function createFolder()
  {
    //$storeFolder = $this->path_def . $this->user_id;
    $storeFolder = $_SERVER['DOCUMENT_ROOT'] . $this->path_def . $this->transData['id'];
    
    if (!is_dir($storeFolder))
    {
        //print_r($storeFolder);exit;
        mkdir($storeFolder, 0777); 
    }
    
    return $storeFolder;
  }
  
  public function uploadFile()
  {
    if (!empty($_FILES))
    {
      $storeFolder = $this->createFolder().'/img/';
      
      //$publicPath = 'templates/templates_users/'.$this->user_id.'/img/';
      $publicPath = '/templates/templates_users/'.$this->transData['id'].'/img/';
      //$storeFolder = $publicPath;
      //print_r($storeFolder);exit;
      //$storeFolder = $_SERVER['DOCUMENT_ROOT']. $publicPath;
      if (!is_dir($storeFolder)){ mkdir($storeFolder,0777); }
      
      $tempFile = $_FILES['fuser_file']['tmp_name'];
          
      $file_ext = trim(strtolower(strrchr($_FILES['fuser_file']['name'], '.')),'.');
      
      $mtime = explode(" ", microtime());
      $name_file = md5($mtime[1] + $mtime[0]);
      $nameFile = $name_file . '.' . $file_ext;
      $pathFile = $storeFolder . $nameFile;
      if (move_uploaded_file($tempFile, $pathFile))
      {
        $key = (isset($_GET['bg'])) ? 'user_files_bg' : 'user_files';
        
        $this->data[$key][] = $publicPath.$nameFile;
        
        $result = array('result'=>true,'file'=>$publicPath.$nameFile);
        
        $this->setJson();
        
        echo json_encode($result);exit;
      }
      else
      {
        echo json_encode(array('result'=>false));exit;
      }
    }
  }
  
  /** reset templates_data */
  public function resetTemp()
  {
    $templ_id = (int)$_POST['temp_id'];
    
    $result = false;

    if($templ_id !=0 )
    {
      $objTrans = new models_Translator();
      
      $transData = $objTrans->getTrEmail(models_Model::getTransEmail());
      
      $res = json_decode($transData['meta_data'], true);
      
      $dataArr = models_Model::templateData($templ_id);
      
      foreach($dataArr as $k=>$v)
      {
        $res[$k]=$v;
      }
      
      $data['meta_data'] = json_encode($res);
    
      $objTrans->updateTrans($this->transData['id'], $data);
      
      $result = true;
    }
    
    echo json_encode(array('result'=>$result));
  }
  
  public function getTemId()
  {
    return $this->temp_id;
  }
  
  /** check edit templates */
  public function edit()
  {
    if(isset($_GET['edit']))
    {
      return true;
    }
    return false;
  }
}