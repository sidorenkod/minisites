<?php

class models_TranzilaResponse extends Zend_Db_Table_Abstract
{
  protected $_name = 'tranzila_response';
  
  public function getCode($code=0)
  {
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('code =  ?', $code);   

    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
}