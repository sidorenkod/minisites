<?php

include_once 'models/class.smtp.php';
include_once 'models/class.phpmailer.php';


class models_Mailer{

  //protected $mailtemplate = "<html><div style='text-align: center'><img src=\"cid:logoimg\" /></div><hr>{message}</html>";

  protected $mailtemplate = '';

  public function getMessageTemplate($header, $body, $customer_email, $footer = 'Sincerely,<br />IGMI Team')
  {
    $cust_email = '';
    $send_mail = '';
    if(is_array($customer_email))
    {
      foreach($customer_email as $mail)
      {
        $cust_email .=', '.$mail;
        $send_mail .= $mail.',';
      }

      $cust_email = trim($cust_email,',');
      $send_mail = trim($send_mail,',');
    }
    else
    {
      $cust_email = $send_mail = $customer_email;
    }


    $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <meta http-equiv="content-type" content="text/html; charset=utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=720px, initial-scale=1">
          <meta name="robots" content="noindex, nofollow">
          <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,700" rel="stylesheet" type="text/css">
          <style>
                  * {
                      margin:0;
                      padding:0;
                      font-family: "Open Sans", arial;
                      font-size:100%;
                      line-height:normal;
                  }
                  img {
                      max-width:100%;
                  }
          </style>
        </head>
        <body style="-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%!important;height:100%;background:#f2f2f2;">
          <div style="margin:auto;width:720px;min-width:720px;">
            <div style="margin:auto;width:620px;padding-top:20px;padding-bottom:35px;">
              <div style="float:left;">
                <p style="color:#999;font-size:11px;">'. date("F j, Y") . '</p>
              </div>
              <div style="float:right;">
                <p style="color:#999;font-size:11px;">Secured e-mail</p>
              </div>
            </div>
            <div style="margin:auto;width:620px;border:1px solid #ddd;background-color:#fff;position:relative;border-radius:20px;">
              <div style="padding:0 15px;">
                <div style="text-align:center;padding-top:30px;">
                  <a style="text-decoration:none;" href="https://www.igmi.co/" title="IGMI - The smart way to find a translator" target="_blank">
                    <img style="border:0;" src="https://www.igmi.co/images/igmi.png" alt="IGMI">
                    <br />
                  </a>
                </div>
                <div style="border-radius:18px;background:#fff;padding:30px 20px;">
                  <h1 style="margin:0;font-weight:bold;font-size:16px;color:#000;">' . $header . '</h1>
                  <br />
                  <span style="font-size:14px;color:#4e4e4e;">
                    ' . $body . '
                    <br />
                    <br />
                    ' . $footer . '
                  </span>
                </div>
              </div>
            </div>
            <div style="margin:auto;width:620px;color:#999;">
              <div style="padding:20px 0;font-size:11px;text-align:center;">
                <div style="border-top:1px solid #ddd;padding:20px 0;margin:20px 0;border-bottom:1px solid #ddd;">
                  <p>This message was sent to <a style="color:#999;" href="mailto:' . $send_mail . '">' . $cust_email . '</a> by IGMI.</p>

                </div>
                <p style="margin:5px;">IGMI LTD</p>
                <p>
                  <a style="text-decoration:underline;color:#999;" href="https://www.igmi.co/" target="_blank" title="Home">Home</a>
                  <span> | </span>
                  <a style="text-decoration:underline;color:#999;" href="https://www.igmi.co/about/" target="_blank" title="About us of IGMI">About us</a>
                </p>
                <p style="padding-top: 5px;">
                  <a style="text-decoration:underline;color:#999;" href="https://www.igmi.co/legal/terms/" target="_blank" title="Terms and Conditions of IGMI">Terms and Conditions</a>
                  <span> | </span>
                  <a style="text-decoration:underline;color:#999;" href="https://www.igmi.co/legal/privacy/" target="_blank" title="Privacy Policy of IGMI">Privacy Policy</a>
                </p>
                <p style="margin-top:15px;font-weight:bold;color:#4e4e4e;">IGMI &copy; '.date('Y').'</p>
              </div>
            </div>
          </div>
        </body>
      </html>';

    return $message;
  }


  /** Selecting the method sending mail production/developer */
  public function getMail($mail_to, $subject, $message, $mail_from = null, $file=null)
  {
    $mail_from = (is_null($mail_from)) ? (models_Settings::email()) : $mail_from;

    if(APPLICATION_ENV == 'production')
    {
      if(is_array($mail_to))
      {
        $res = null;
        foreach($mail_to as $mail_t)
        {
          $res = $this->doMail($mail_t, $subject, $message, $mail_from, $file);
        }
        return $res;
      }
      else
      {
        return $this->doMail($mail_to, $subject, $message, $mail_from, $file);
      }
    }
    else
    {
      //$this->smtpmail('sidorenkod@gmail.com', $subject, $message, $mail_from);
      //return $this->smtpmail('evgeniy.starcev.88@gmail.com', $subject, $message, $mail_from);
      return $this->doMail('evgeniy.starcev.88@gmail.com', $subject, $message, $mail_from);
    }
  }

  /** common function for our mails
  * returns true in success and false otherwise */
  public function doMail($mail_to, $subject, $message, $mail_from=null, $file=null,$agensyName=null)
  {
    $htmlMessage = $message;

    $email = new PHPMailer();
      try {
        $email->CharSet = 'UTF-8';
        $email->From      = (is_null($mail_from))?"admin@translated.co":$mail_from;;
        $email->FromName  = (is_null($agensyName))?"Translated.co":$agensyName;
        $email->Subject   = $subject;
        $email->MsgHTML($htmlMessage);
        $email->AltBody = strip_tags($htmlMessage);

        if(!is_null($file))
            $email->AddAttachment($file);

        $email->IsSMTP();                        // tell the class to use SMTP
        $email->SMTPAuth = true;                 // enable SMTP authentication
        $email->IsHTML(true);

        $email->Port       = 465;                 // set the SMTP server port
        $email->SMTPSecure = 'ssl';
        $email->Host       = "smtp.gmail.com";        // SMTP server
        $email->Username   = "admin@translated.co";     // SMTP server username
        $email->Password   = "Ho@e|l3$";         // SMTP server password

        // must be changed to true when released!!
        if (false)
        {
          if(is_array($mail_to))
          {
              if(count($mail_to)>=0)
              {
                  foreach($mail_to as $mto)
                      $email->AddAddress($mto);
              }else
                  return 0;
          }else{
              $email->AddAddress($mail_to);
          }
        }else{
          // this case is to avoid wrong messages while the site is in testing stage
          //$email->AddAddress("evgeniy.starcev.88@gmail.com");
          $email->AddAddress("sidorenkod@gmail.com");
          //$email->AddAddress("ivan@smartlation.com");
        }

        if ($email->Send())
            return array (1, $mail_to);
        else
            return array (0, $email->ErrorInfo);

      } catch(phpmailerException $e)
      {
          return array(0, $e->errorMessage()); //Pretty error messages from PHPMailer
      } catch (Exception $e) {
          return array(0, $e->getMessage()); //Boring error messages from anything else!
      }
  }

  public function smtpmail($mail_to, $subject, $message, $mail_from = null )
  {
    $htmlMessage = $this->getMessageTemplate($subject, $message, $mail_to);

    if(is_null($mail_from)) { $mail_from = models_Settings::email(); }

    $config['smtp_charset']  = 'windows-1251';
    //кодировка сообщений. (или UTF-8, итд)
    $config['smtp_from']     = 'IGMI';
    //Ваше имя - или имя Вашего сайта. Будет показывать при прочтении в поле "От кого"
    $config['smtp_debug']   = false;
    //Если Вы хотите видеть сообщения ошибок, укажите true вместо false
    $config['smtp_port']     = '465';
    // Порт работы. Не меняйте, если не уверены.

    $config['smtp_searcher'] = 'yandex.ru';
    $config['smtp_email'] = 'Jey-Val-Star@yandex.ru';
    $config['smtp_username'] = 'SmV5LVZhbC1TdGFyQHlhbmRleC5ydQ==';
    //Смените на имя своего почтового ящика.
    $config['smtp_host']     = 'ssl://smtp.yandex.ru';
    //сервер для отправки почты
    $config['smtp_password'] = 'dmlwaTc0MjY5NXZpcGk=';

    $header="Date: ".date("D, j M Y G:i:s")." +0700\r\n";
    $header.="From: =?utf-8?Q?".str_replace("+","_",str_replace("%","=",urlencode(''.$config['smtp_from'].'')))."?= <".$mail_from.">\r\n";
    $header.="X-Mailer: The Bat! (v3.99.3) Professional\r\n";
    $header.="Reply-To: =?windows-1251?Q?".str_replace("+","_",str_replace("%","=",urlencode(''.$config['smtp_from'].'')))."?= <".$config['smtp_email'].">\r\n";
    $header.="X-Priority: 3 (Normal)\r\n";
    $header.="Message-ID: <172562218.".date("YmjHis")."@".$config['smtp_searcher'].">\r\n";
    $header.="To: =?windows-1251?Q?".str_replace("+","_",str_replace("%","=",urlencode('')))."?= <$mail_to>\r\n";
    $header.="Subject: =?windows-1251?Q?".str_replace("+","_",str_replace("%","=",urlencode(''.$subject.'')))."?=\r\n";
    $header.="MIME-Version: 1.0\r\n";
    $header.="Content-Type: text/html; charset=windows-1251\r\n";
    $header.="Content-Transfer-Encoding: 8bit\r\n";


    $smtp_conn = fsockopen("".$config['smtp_host']."", $config['smtp_port'],$errno, $errstr, 10);
    if(!$smtp_conn) {print "соединение с серверов не прошло"; fclose($smtp_conn); exit;}
    $data = $this->get_data($smtp_conn);

    fputs($smtp_conn,"EHLO yandex.ru \r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 250)
    {
        //print "ошибка приветсвия EHLO"; fclose($smtp_conn); exit;
        return 0;
    }

    fputs($smtp_conn,"AUTH LOGIN\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 334)
    {
        //print "сервер не разрешил начать авторизацию"; fclose($smtp_conn); exit;
        return 0;
    }

    fputs($smtp_conn,$config['smtp_username']."\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 334)
    {
        //print "ошибка доступа к такому юзеру"; fclose($smtp_conn); exit;
        return 0;
    }


    fputs($smtp_conn,$config['smtp_password']."\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 235)
    {
        //print "не правильный пароль"; fclose($smtp_conn); exit;
        return 0;
    }

    fputs($smtp_conn,"MAIL FROM:".$config['smtp_email']."\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 250)
    {
        //print "сервер отказал в команде MAIL FROM"; fclose($smtp_conn); exit;
        return 0;
    }

    fputs($smtp_conn,"RCPT TO:".$mail_to."\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 250 AND $code != 251)
    {
        //print "Сервер не принял команду RCPT TO"; fclose($smtp_conn); exit;
        return 0;
    }

    fputs($smtp_conn,"DATA\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);
    if($code != 354)
    {
        //print "сервер не принял DATA"; fclose($smtp_conn); exit;
        return 0;
    }

    fputs($smtp_conn,$header."\r\n".$htmlMessage."\r\n.\r\n");
    $code = substr($this->get_data($smtp_conn),0,3000);

    if($code != 250)
    {
      //print "ошибка отправки письма"; fclose($smtp_conn); exit;
      return 0;
    }

    fputs($smtp_conn,"QUIT\r\n");
    fclose($smtp_conn);

    return array (1, $mail_to);
  }

  public function get_data($smtp_conn)
  {
    $data="";
    while($str = fgets($smtp_conn,515))
    {
      $data .= $str;
      if(substr($str,3,1) == " ") { break; }
    }
  return $data;
  }
}