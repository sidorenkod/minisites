<?php

class models_File extends Zend_Db_Table_Abstract
{
  protected $_name = 'order_files';
  
  public function insertFile($data)
  {
    return $this->insert($data);
  }
  
  public function updateFile($id, $data)
  {
    if($id != 0)
    {
      $where = $this->getAdapter()->quoteInto('id = ?', $id);
      $this->update($data, $where);
    }
  }
  
  public function deleteFile($id)
  {
    if($id != 0)
    {
      $where = $this->getAdapter()->quoteInto('id = ?', $id);
      $this->delete($where);
    }
  }
  
  public function getFiles($order_id=0, $file_id=0)
  {
    $select = $this->getAdapter()->select()->from($this->_name);
            
    if($file_id != 0)
    {
      $select->where('id = ?', $file_id);
      
      return $this->getAdapter()->fetchRow($select);// if not row - result = false
    }
    
    $select->where('order_id = ?', $order_id);
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchAll($select);
  }
  
  public function uploadFile($email)
  {
    $objTrans = new models_Translator();
    
    $trans = $objTrans->getTrEmail($email);
    
    $storeFolder = realpath(APPLICATION_PATH . "/../public/uploads/").'/'.$trans['id'].'/';
    
    if (!is_dir($storeFolder)){ mkdir($storeFolder,0777); }
    
    $tempFile = $_FILES['fileupload']['tmp_name'];
    
    $file_ext = trim(strtolower(strrchr($_FILES['fileupload']['name'], '.')),'.');
    
    $file_id = $this->insertFile(array('order_id'=>$_SESSION['order_id'],'last_name'=>$_FILES['fileupload']['name'],'file_ext'=>$file_ext));
    
    $nameFile = $_SESSION['order_id'] .'_' . $file_id . '.'.$file_ext;
    
    $pathFile = $storeFolder . $nameFile;
    
    $res = '';
    
    if (move_uploaded_file($tempFile, $pathFile))
    {
      $objOrder = new models_Order();
      $orderData = $objOrder->getOrder($_SESSION['order_id']);
      
      $url = 'https://www.smartlation.com/api/getwordcount?flang='.$orderData['lang_id'].'&files[]=http://translated.co/uploads/'.$trans['id'].'/'.$nameFile;
      
      $dataFile = json_decode($this->file_get_contents_curl($url), true);
      //print_r($url);
      //print_r($dataFile);
      
      exec("nohup /usr/bin/php -f ../utils/async_count_words.php '".$dataFile['id']."' '$nameFile' '$file_id' > /dev/null 2>&1 &");
      
      /*sleep(5);
      
      $url = 'https://www.smartlation.com/api/getstatus?id='.$dataFile['id'];//585bc8c231278
      
      $dataFile2 = json_decode($this->file_get_contents_curl($url), true);
      
      $this->updateFile($file_id, array('len'=>$dataFile2[0]['words'],'name_file'=>$nameFile));
      */
      //print_r($dataFile);exit;
      $res = true;
    }
    else
    {
      
      $res = false;
    }
    
    return $res;
  }
  
  function file_get_contents_curl($url) 
  {
  	$ch = curl_init();
  
  	curl_setopt($ch, CURLOPT_HEADER, 0);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //������������� ��������, ����� curl ��������� ������, ������ ����, ����� �������� �� � �������.
  	curl_setopt($ch, CURLOPT_URL, $url);
  
  	$data = curl_exec($ch);
  	curl_close($ch);
  
  	return $data;
  }
}