<?php

class models_Model extends Zend_Db_Table_Abstract
{
  protected $_name = '';
  
  public function getData($order_plan_id)
  {    
    $sql = "SELECT D.*, L1.*, L2.lenguage_id, L2.name_leng as name_leng2 FROM details_language_order_plan D, languages L1, languages L2 WHERE D.order_plan_id=".$order_plan_id." AND D.leng_id_1=L1.lenguage_id AND D.leng_id_2=L2.lenguage_id;";

    //print_r($sql);exit;
    
    return $this->getAdapter()->fetchAll($sql); // if not row - result = false
  }
  
  public function getData2($order_plan_id)
  {    
    $sql = "select * from details_expertise_order_plan D, expertises E WHERE order_plan_id=".$order_plan_id." AND D.expe_id=E.expertise_id;";

    return $this->getAdapter()->fetchAll($sql); // if not row - result = false
  }
  
  public function sqlQuery($sql, $table)
  {
    //$this->_name = $table;
    //print_r($data);
    $this->getAdapter()->query($sql);
    
    return $this->getAdapter()->lastInsertId($table);
  }
  
  public function sqlQueryCount($sql)
  {
    $result = $this->getAdapter()->fetchAll($sql);
    
    return count($result);
  }
  
  public function sqlQuery2($sql, $row = null)
  {
    if(!is_null($row))
    {
      return $this->getAdapter()->fetchRow($sql);
    }
    return $this->getAdapter()->fetchAll($sql);
  }
  
  /** method get email translator */
  static function getTransEmail()
  {
    if(Zend_Auth::getInstance()->hasIdentity() && isset(Zend_Auth::getInstance()->getIdentity()->email))
    {
      $email = Zend_Auth::getInstance()->getIdentity()->email;
    }
    else
    {
      $email = $_SESSION['email'];
    }
    //print_r($email);exit;
    return $email;
    
    //print_r(models_Model::getTransData());exit;
  }
  
  static public function convert($price)
  {
    if($price != 0)
    {
      $url = "http://www.google.com/finance/converter?a=$price&from=USD&to=SEK";
      $request = curl_init();
      $timeOut = 0;
      curl_setopt ($request, CURLOPT_URL, $url);
      curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
      curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
      $response = curl_exec($request);
      curl_close($request);

      $regularExpression = '#\<span class=bld\>(.+?)\<\/span\>#s';
      preg_match($regularExpression, $response, $finalData);
      $str = preg_replace("/[^0-9.]/", '', $finalData[0]);
      //print_r($finalData[0]); exit;
      return intval($str);;
    }

      return 0;
  }
  
  static public function getString($text, $arr = null)
  {
    if(!is_null($arr) && count($arr)>0) 
    {  
      foreach($arr as $key => $val)
      {
        $text = str_replace('%'.$key.'%', $val, $text);
      }
    }
    
    return $text;
    
  }
  
  /** Generate password */
  static public function passwGen()
  {
    $chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

    $max = 8;

    $size = strlen($chars) - 1;

    $password = null;

    // Create password
    while ($max--){ $password .= $chars[rand(0, $size)]; }

    return $password;
  }
  
  /** method to return translator data (id, email) */
  static function getTransData()
  {
    $user_data = null;
    
    $auth = Zend_Auth::getInstance();

    if ($auth->hasIdentity())
    {
      $user_data = $auth->getStorage()->read();
    }
    
    return $user_data;
  }
  
  /** get subdomen */
  static function getSubdomain()
  {
    $subdomain = false;
    
    if (isset($_SERVER['HTTP_HOST']))
    {
      $host =  $_SERVER['HTTP_HOST'];
      $hostParts = preg_split("/\./", $host);

      if (count($hostParts)==3){
        $subdomain = $hostParts[0];
      }

    }
    
    return $subdomain;
  }
  
  static public function parseExel($sInFile)
  {
    $sPath = dirname(__FILE__);
    require_once $sPath.'/PHPExcel.php';
    //$oExcel = PHPExcel_IOFactory::load($sInFile);
    
    $result = array();

  	// получаем тип файла (xls, xlsx), чтобы правильно его обработать
  	$file_type = PHPExcel_IOFactory::identify( $sInFile );
  	// создаем объект для чтения
  	$objReader = PHPExcel_IOFactory::createReader( $file_type );
  	$objPHPExcel = $objReader->load( $sInFile ); // загружаем данные файла в объект
  	$result = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив
  
  	return $result;
  }
  
  static public function createExel($arrData)
  {
    $fileName = $_SERVER['DOCUMENT_ROOT'].'/uploads/data.xlsx';
    $sPath = dirname(__FILE__);
    require_once $sPath.'/PHPExcel.php';
    //$oExcel = PHPExcel_IOFactory::load($sInFile);
    $phpexcel = new PHPExcel();
    /* Каждый раз делаем активной 1-ю страницу и получаем её, потом записываем в неё данные */
      $page = $phpexcel->setActiveSheetIndex(0); // Делаем активной первую страницу и получаем её
      $i=1;
      foreach($arrData as $row)
      {
        $page->setCellValue("A".$i, $row['name_cus']); // Добавляем в ячейку A1 слово "Hello"
        $page->setCellValue("B".$i, $row['email_cus']); // Добавляем в ячейку A2 слово "World!"
        $page->setCellValue("C".$i, $row['email_cus_sec']); // Добавляем в ячейку B1 слово "MyRusakov.ru"
        $i++;
      }
      
      $page->setTitle("Customers"); // Ставим заголовок "Test" на странице
      /* Начинаем готовиться к записи информации в xlsx-файл */
      $objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
      /* Записываем в файл */
      $objWriter->save($fileName);
      
      if (ob_get_level()) {
        ob_end_clean();
        }
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($fileName));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($fileName));
        readfile($fileName);
  }
  
  static function templateData($temp_id = 0)
  {
    $data = array();
    
    $data['meta'] = array(
      // meta data
      "title"=>"title",
      "meta_desc"=>"description",
      "meta_key"=>"keywords",
    );
    
    $data[1] = array(
       //template data
        "1_txt1"=>"Professional Translation",
        "1_txt1_style"=>"font-size:40px;",
        
        "1_txt2"=>"Martketplace 24/7",
        "1_txt2_style"=>"",
        
        "1_txt3"=>"UPLOAD...COMPARE...DONE!",
        "1_txt3_style"=>"",
        
        "1_txt4"=>"SMARTLATION.COM works in very easy steps. Now you do not need to get frustrated over translation tasks. All you have to do is get your content through SMARTLATION.COM system and get instant results.",
  
        "1_bg1"=>"",
        "1_bg2"=>"",
        
        "1_txt5"=>"Our Advantages",
        "1_txt5_style"=>"color:white !important; text-align:center;",
        
        "1_txt6"=>"Professionalism",
        "1_txt6_style"=>"",
        
        "1_txt7"=>"SMARTLATION.COM categorizes service providers by field of expertise and localization ability, thus ensuring that the translator of your choice is the best one for the job.",
        "1_txt7_style"=>"",
        
        "1_txt8"=>"Quality",
        "1_txt8_style"=>"",
        
        "1_txt9"=>"Our algorithm provides a list of service providers with a proven track record. The vendor list includes a rating chart. The client can sort vendors from “excellent” to “good enough” before selecting a given translator.",
        "1_txt9_style"=>"",
        
        "1_txt10"=>"Competitive Pricing",
        "1_txt10_style"=>"",
        
        "1_txt11"=>"Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits. Dramatically visualize customer directed convergence without revolutionary ROI.",
        "1_txt11_style"=>"",
        
        "1_txt12"=>"Cost Effectiveness",
        "1_txt12_style"=>"",
        
        "1_txt13"=>"All prices quoted through SMARTLATION.COM are based on the source language text. When a project is uploaded, our system provides an accurate word count and generates a quote. By providing the quote upon upload, the client knows the final price in advance.",
        "1_txt13_style"=>"",
        
        "1_txt14"=>"Speed",
        "1_txt14_style"=>"",
        
        "1_txt15"=>"The list displays the estimated time of delivery for each vendor. The system also tracks the translator’s progress. If there is a delay, the client is notified immediately. This ensures that the job is finished on time.",
        "1_txt15_style"=>"",
        
        "1_txt16"=>"Ease of Mind",
        "1_txt16_style"=>"",
        
        "1_txt17"=>"All stages of a project are done through SMARTLATION.COM. This saves both client and vendor from having to use different methods of communication for each stage of the job. SMARTLATION.COM makes the whole translation process fast, easy, and simple.",
        "1_txt17_style"=>"",
        
        "1_txt18" => "Our platform is the ONLY system in existence today that gives the client complete control over the project at all stages.",
        "1_txt18_style"=>"color:white; text-align:center; margin-top:20px; margin-bottom:-30px; z-index:99999;",

        "1_txt19"=>"Join Us",
        "1_txt19_style"=>"margin:0px 5px;",
        
        "1_txt20"=>"Translate Now",
        "1_txt20_style"=>"margin:0px 5px;",
        
        "1_txt21"=>"Contact",
        "1_txt21_style"=>"margin:0px 5px;",
        
        "1_txt22"=>"Our Advantages",
        "1_txt22_style"=>"",
        
        "1_txt23"=>"How does it works?",
        "1_txt23_style"=>"",
        
        "1_txt24"=>"What we do?",
        "1_txt24_style"=>"",
        
        "1_txt27"=>"Professional Translation",
        "1_txt27_style"=>"",
        
        "1_txt28"=>"Proof Reading",
        "1_txt28_style"=>"",
        
        "1_txt29"=>"Project Manager",
        "1_txt29_style"=>"",
        
        "1_txt31"=>"Customers FAQs",
        "1_txt31_style"=>"",
        
        "1_txt32"=>"Contact Us",
        "1_txt32_style"=>"",
        
        "1_txt33"=>"Supported Formats",
        "1_txt33_style"=>"",
        
        "1_txt37"=>"About",
        "1_txt37_style"=>"margin-top:50px;",
        
        "1_txt38"=>"Services",
        "1_txt38_style"=>"margin-top:50px;",
        
        "1_txt39"=>"Support",
        "1_txt39_style"=>"margin-top:50px;",
        
        "1_txt40"=>"Help",
        "1_txt40_style"=>"margin-top:50px;",
        
        "1_txt333"=>"Compare the best offers for you.",
        "1_txt333_style"=>"",
        
        "1_txt41"=>"CONTACT  US",
        "1_txt41_style"=>"",
        
        "1_btn1"=>"TRANSLATE NOW!",
        "1_btn1_style"=>"",
        
        "1_btnnn2"=>"TRANSLATE NOW!",
        "1_btnnn2_style"=>"",
        
        "1_btnnn3"=>"LEARN IN SIMPLE STEPS",
        "1_btnnn3_style"=>"",
      
        "1_btn4"=>"CONTACT ME",
        "1_btn4_style"=>"",
        
        "1_blc1"=>"1",
        "1_blc2"=>"1",
        "block3"=>"1",
        "block4"=>"1",
        
        "block5"=>"1",
        "block6"=>"1",
        "block7"=>"1",
        "block8"=>"1",
        "block9"=>"1",
        "block10"=>"1",
        
        "link1"=>"Home", 
        "link1_block"=>"1",
        "link1_style"=>"",
        
        "link10"=>"Contact Us",
        "link10_block"=>"1",
        "link10_style"=>"",
        
        "link11"=>"About",
        "link11_block"=>"1",
        "link11_style"=>"",
        
        "link13"=>"Services",
        "link13_block"=>"1",
        "link13_style"=>"",
        
        "logo"=>"/templates/template_1/img/logo.png",
        "1_img2"=>"/templates/template_1/img/about.png",
        
        "1_soc1"=>"1",
        "1_soc1_link"=>"https://www.facebook.com/smartlation",
        "1_soc2"=>"1",
        "1_soc2_link"=>"https://twitter.com/smartlation",
        "1_soc3"=>"1",
        "1_soc3_link"=>"https://plus.google.com/+SmartlationMarketplace",
        "1_soc4"=>"1",
        "1_soc4_link"=>"https://www.linkedin.com/company/smartlation",
        "1_soc5"=>"1",
        "1_soc5_link"=>"https://www.pinterest.com/smartlation/",
    );
    
    $data[3] = array(
        "3_txt1"=>"EASY TRANSLATION JOB",
        "3_txt1_style"=>"font-size:40px;margin-top:30px;",
        
        "3_txt2"=>"Choose Language",
        "3_txt2_style"=>"",
        
        "3_txt3"=>"Upload Your Files",
        
        "3_txt4"=>"Get Instant Offers",
        

        "3_txt41"=>"Translate From",
        "3_txt42"=>"Translate To",
        "3_txt43"=>"Expertise",

        "3_img2"=>"/templates/template_3/img/top.png",
        "3_bg2nav"=>"background-color:#eef3f7",
        "3_about"=>"",
        "3_btn1"=>"Order Now",
        "3_btn1_style"=>"",
        "footer"=>"",
        "3_bg23"=>"",
    );
    
    $data[2] = array(
        
        "css-preset"=>"color_1",
        "logo"=>'/templates/template_2/images/logo.png',
        "logobot"=>"/templates/template_2/images/logo.png",
        "slogan_p"=>"The tagline for agency can go here",
        "slogan_p_color"=>"",
        "id_h1"=>'Welcome to <span>Singular</span>',
        "id_h1_color"=>"",
        "text_h2_1"=>"Why Choose Us dwa",
        "text_h2_1_color"=>"",
        "text_p_1"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam",
        "text_p_1_color"=>"",
        "text_h3_1"=>"Reason-1",
        "text_h3_1_color"=>"",
        "text_p_2"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
        "text_p_2_color"=>"",
        "text_h3_2"=>"Reason-2",
        "text_h3_2_color"=>"",
        "text_p_3"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
        "text_p_3__color"=>"",
        "text_h3_3"=>"Reason-3",
        "text_h3_3_color"=>"",
        "text_p_4"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore",
        "text_p_4_color"=>"",
        "bg_1"=>'background-image: url(/templates/template_2/images/header.jpg)',
        "text_h2_2"=>"About Us",
        "text_h2_2_color"=>"",
        "text_p_5"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.Ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        "text_p_5_color"=>"",
        "text_p_6"=>"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "text_p_6_color"=>"",
        "text_h2_3"=>"Professional Credibility & Experience",
        "text_h2_3_color"=>"",
        "text_p_7"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam",
        "text_p_7_color"=>"",
        "text_p_8"=>"Experience Title-1",
        "text_p_8_color"=>"",
        "text_p_9"=>"2011 - 2013",
        "text_p_9_color"=>"",
        "text_p_10"=>"Name of Company",
        "text_p_10_color"=>"",
        "text_p_11"=>"Lorem ipsum dolor sit amet, consectetur adipiscingVivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.",
        "text_p_11_color"=>"",
        "text_p_12"=>"Experience Title-2",
        "text_p_12_color"=>"",
        "text_p_13"=>"2010 - 2010",
        "text_p_13_color"=>"",
        
        "text_p_14"=>"Name of Company",
        "text_p_15"=>"Lorem ipsum dolor sit amet, consectetur adipiscingVivamus sit amet ligula non lectus cursus egestas. Cras erat lorem, fringilla quis sagittis in, sagittis inNam leo tortor Nam leo tortor Vivamus.",
        "text_h3_4"=>"Cerification-1",
        "text_h4_1"=>"Name of Institute",
        "text_p_16"=>"Consectetur adipisicing elit, sed do eiusmod tempor incididunt",
        "text_h3_5"=>"Cerification-2",
        "text_h4_2"=>"Name of Institute",
        "text_p_17"=>"Consectetur adipisicing elit, sed do eiusmod tempor incididunt",
        "text_h3_6"=>"Affiliation-1",
        "text_h4_3"=>"Name of Affiliate",
        "text_p_18"=>"Consectetur adipisicing elit, sed do eiusmod tempor incididunt",
        "text_h3_7"=>"Affiliation-2",
        "text_h4_4"=>"Name of Affiliate",
        "text_p_19"=>"Consectetur adipisicing elit, sed do eiusmod tempor incididunt",
        "text_h3_8"=>"150",
        "text_p_20"=>"Completed Order",
        "text_h3_9"=>"65",
        "text_p_21"=>"Available Languages",
        "text_h3_10"=>"93%",
        "text_p_22"=>"Positive Ratings",
        "text_h3_11"=>"44",
        "text_p_23"=>"Clients",
        "text_h2_4"=>"Hire Us",
        "text_p_24"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam",
        "text_h4_5"=>"Client References",
        "text_p_25"=>"Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
        "text_p_26"=>"Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
        "text_p_27"=>"Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.",
        "text_h4_6"=>"Client One",
        "text_h4_7"=>"Client Two",
        "text_h4_8"=>"Client Three",
        "text_h2_5"=>"Blog Posts",
        "text_p_28"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam",
        "text_h3_12"=>"Lorem ipsum dolor sit amet consectetur adipisicing elit",
        "text_p_29"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.","text_h3_13"=>"Lorem ipsum dolor sit amet consectetur adipisicing elit",
        "text_p_30"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "text_h3_14"=>"Lorem ipsum dolor sit amet consectetur adipisicing elit",
        "text_p_31"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "text_p_32"=>"June 26, 2016",
        "text_p_33"=>"June 26, 2016",
        "text_p_34"=>"June 26, 2016",
        "text_h2_6"=>"Contact Us",
        "text_p_35"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam",
        "text_p_36"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.",
        "text_p_37"=>"Test Location",
        "text_p_38"=>"+123 456 7890",
        "text_p_39"=>"+123 456 7890",
        "text_p_40"=>"Skype\/test-user",
        "text_p_41"=>"&copy;2016 | All Rights Reserved",
        "text_p_42"=>"www.test-user.com",
        
        "text_p_14_color"=>"",
        "text_p_15_color"=>"",
        "text_h3_4_color"=>"",
        "text_h4_1_color"=>"",
        "text_p_16_color"=>"",
        "text_h3_5_color"=>"",
        "text_h4_2_color"=>"",
        "text_p_17_color"=>"",
        "text_h3_6_color"=>"",
        "text_h4_3_color"=>"",
        "text_p_18_color"=>"",
        "text_h3_7_color"=>"",
        "text_h4_4_color"=>"",
        "text_p_19_color"=>"",
        "text_h3_8_color"=>"",
        "text_p_20_color"=>"",
        "text_h3_9_color"=>"",
        "text_p_21_color"=>"",
        "text_h3_10_color"=>"",
        "text_p_22_color"=>"",
        "text_h3_11_color"=>"",
        "text_p_23_color"=>"",
        "text_h2_4_color"=>"",
        "text_p_24_color"=>"",
        "text_h4_5_color"=>"",
        "text_p_25_color"=>"",
        "text_p_26_color"=>"",
        "text_p_27_color"=>"",
        "text_h4_6_color"=>"",
        "text_h4_7_color"=>"",
        "text_h4_8_color"=>"",
        "text_h2_5_color"=>"",
        "text_p_28_color"=>"",
        "text_h3_12_color"=>"",
        "text_p_29_color"=>"",
        "text_p_30_color"=>"",
        "text_h3_14_color"=>"",
        "text_p_31_color"=>"",
        "text_p_32_color"=>"",
        "text_p_33_color"=>"",
        "text_p_34_color"=>"",
        "text_h2_6_color"=>"",
        "text_p_35_color"=>"",
        "text_p_36_color"=>"",
        "text_p_37_color"=>"",
        "text_p_38_color"=>"",
        "text_p_39_color"=>"",
        "text_p_40_color"=>"",
        "text_p_41_color"=>"",
        "text_p_42_color"=>"",
        
        "features"=>"",
        "about-us"=>"",
        "twitter"=>"",
        "contact-us"=>"",
        "lang1"=>"Language-1",
        "lang2"=>"Language-2",
        "lang3"=>"Language-3",
        "lang4"=>"Language-4",
        "langp1"=>"95",
        "langp2"=>"75",
        "langp3"=>"60",
        "langp4"=>"85",
        
        "lang1_color"=>"",
        "lang2_color"=>"",
        "lang3_color"=>"",
        "lang4_color"=>"",
        "langp1_color"=>"",
        "langp2_color"=>"",
        "langp3_color"=>"",
        "langp4_color"=>"",
        
        "user_files"=>array(),
        "user_files_bg"=>array(),
        "block1"=>1,
        "block2"=>1,
        "block3"=>1,
        "block4"=>1,
        "comt1"=>1,
        "comt2"=>1,
        "comt3"=>1,
        "img1"=>"/templates/template_2/images/blog/1.jpg",
        "img2"=>"/templates/template_2/images/blog/2.jpg",
        "img3"=>"/templates/template_2/images/blog/3.jpg",
        "img4"=>"/templates/template_2/images/affiliation/certifications.png",
        "img5"=>"/templates/template_2/images/affiliation/certifications.png",
        "img6"=>"/templates/template_2/images/affiliation/affiliations.png",
        "img7"=>"/templates/template_2/images/affiliation/affiliations.png",
        "nav"=>"",
        "services"=>"",
        "team"=>"",
        "hire"=>"",
        "blog"=>"",
        "footer"=>"",
        "footer_bot"=>"",
        "text9"=>"ember Since: March 18,2016",
        "text10"=>"Available Now"
      );
      
    if($temp_id != 0)
    {
      return $data[$temp_id];
    }
    
    $res = $data['meta']+$data[1]+$data[2]+$data[3];
    
    return $res;
  }
}