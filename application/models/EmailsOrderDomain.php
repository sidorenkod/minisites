<?php

class models_EmailsOrderDomain extends Zend_Db_Table_Abstract
{
  protected $_name = 'emails_order_domain';
  
  public function getEmOrdDomain($id)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

      $select->where('order_domain_id =  ?', $id);

    return $this->getAdapter()->fetchAll($select); // if not row - result = false
  }
  
  public function getCorporateEmail($id)
  {
    $select = $this->getAdapter()->select()
            ->from($this->_name);

      $select->where('email_order_id =  ?', $id);

    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function updateCEmail($id, $data)
  {
    if($id != 0)
    {
      $where = $this->getAdapter()->quoteInto('email_order_id = ?', $id);
      $this->update($data, $where);
    }
  }
}
//emails_order_domain