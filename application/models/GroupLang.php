<?php

class models_GroupLang extends Zend_Db_Table_Abstract
{
  protected $_name = 'group_languages';
  
  public function getGr($id=0)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
            
    if($id != 0)
    {   
      $select->where('group_id =  ?', $id);
      
      return $this->getAdapter()->fetchRow($select); // if not row - result = false
    }
    
    return $this->getAdapter()->fetchAll($select); // if not row - result = false
  }
  
  public function getPrice($lang_1, $lang_2)
  {    
    $select = $this->getAdapter()->select()
            ->from(array('gr'=>$this->_name), 'max(price_group) as price');
    $select->joinLeft('languages as l','l.group_id = gr.group_id',array(''));

    $select->where('l.lenguage_id = ?', $lang_1);
    $select->orWhere('l.lenguage_id = ?', $lang_2);
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function insertGr($data)
  {
    return $this->insert($data);
  }
  
  public function updateGr($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('group_id = ?', $id);
    $this->update($data, $where);
  }
  
  public function deleteGr($id)
  {
    $where = $this->getAdapter()->quoteInto('group_id = ?', $id);
    $this->delete($where);
  }
  
  public function setPrice($type, $price)
  {
    if($type == 'plus')
    {
      $price = ($price/100)+1;
    }
    else if($type == 'minus')
    {
      $price = (100-$price)/100;
    }
    else{
      return;
    }
    
    $sql = 'UPDATE `minisites`.`group_languages` SET `price_group`=`price_group`*'.$price;
    
    //print_r($sql);exit;
    
    $this->getAdapter()->query($sql);
  }
}