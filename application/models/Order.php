<?php

class models_Order extends Zend_Db_Table_Abstract
{
  protected $_name = 'orders';
  
  public function insertOrder($data)
  {
    return $this->insert($data);
  }

  public function uploadTranslatedFiles($order_num, $file_urls)
  {
    $dbTable = $this->getAdapter();
    $select = $dbTable->select();
    $select = $select->from(array('o' => 'orders'))
                     ->where("o.smartlation_order_num = ? ", $order_num);

    $row  = $dbTable->fetchAll($select);
    if (count($row)==1)
    {
      $order_id = $row[0]['id'];

      $upload_dir = getcwd()."/translated/$order_id";
      if (!is_dir($upload_dir))
        mkdir($upload_dir, 0777);

      foreach($file_urls as $url){
        $upload_file_name = basename($url);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        $data = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpCode==200)
        {
          $ch = curl_init($url);
          $exploded_target = explode('.', $upload_file_name);
          $extension = (count($exploded_target)>1)?end($exploded_target):false;

          if ($extension && $extension != 'php' )
          {
            $upload_file_path = $upload_dir."/".$upload_file_name;

            // download file
            $fp = fopen($upload_file_path, "w");
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
          }
        }else{
          return false;
        }
      }
    }else{
      return false;
    }
  }

  public function updatePercent($order_num, $percent)
  {
    $dbTable = $this->getAdapter();
    $select = $dbTable->select();
    $select = $select->from(array('o' => 'orders'))
                     ->where("o.smartlation_order_num = ? ", $order_num);

    $row  = $dbTable->fetchAll($select);
    if (count($row)==1)
    {
      $order_id = $row[0]['id'];
      $this->updateOrder($order_id, array('work_status'=>$percent));
      return true;
    }else{
      return false;
    }
  }
  
  public function updateOrder($id, $data)
  {
    if($id != 0)
    {
      $where = $this->getAdapter()->quoteInto('id = ?', $id);
      $this->update($data, $where);
    }
  }
  
  public function getOrder($order_id=0, $trans_id=0)
  {
    $select = $this->getAdapter()->select()->from(array('o'=>$this->_name));
            
    if($order_id != 0)
    {
      $select->joinLeft('languages as sl', 'sl.lenguage_id = o.lang_id', array('name_leng as sourse'));
      $select->joinLeft('languages as dl', 'dl.lenguage_id = o.dest_lang_id', array('name_leng as dest'));
      
      $select->where('o.id = ?', $order_id);
      
      return $this->getAdapter()->fetchRow($select);// if not row - result = false
    }
    else if($trans_id != 0)
    {
      $select->joinLeft('customers as cust', 'cust.cus_id = o.customer_id', array('email_cus','name_cus'));
      $select->where('trans_id = ?', $trans_id);
      
      //echo (string)$select;exit;
      
      return $this->getAdapter()->fetchAll($select);
    }
    
    return $this->getAdapter()->fetchAll($select);
  }
  
  public function getOrderCust($custID, $order_id=0)
  {
    $select = $this->getAdapter()->select()->from(array('o'=>$this->_name));
    
    $select->joinLeft('languages as sl', 'sl.lenguage_id = o.lang_id', array('name_leng as sourse'));
    $select->joinLeft('languages as dl', 'dl.lenguage_id = o.dest_lang_id', array('name_leng as dest'));  
    
    $select->where('o.customer_id = ?', $custID);
    
    if($order_id != 0)
    {
      $select->where('o.id = ?', $order_id);
      
      return $this->getAdapter()->fetchRow($select);// if not row - result = false
    }
    
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchAll($select);
  }
  
  public function getCountOrder($custID=0)
  {
    $select = $this->getAdapter()->select()->from($this->_name,array('count(id) as count'));
            
    if($custID != 0)
    { 
      $select->where('customer_id = ?', $custID);
    }
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select);
  }
  
}