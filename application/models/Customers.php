<?php

class models_Customers extends Zend_Db_Table_Abstract
{
  protected $_name = 'customers';
  
  public function getCust($email=null, $id=0, $trans_id=null)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
            
      if(!is_null($email))
      {
        $select->where('email_cus =  ?', $email);
        
        if(!is_null($trans_id))
        {
          $select->where('agency_id_r =  ?', $trans_id);
        }
        
        return $this->getAdapter()->fetchRow($select); // if not row - result = false
      }
      else if($id != 0)
      {
        $select->where('cus_id =  ?', $id);
        return $this->getAdapter()->fetchRow($select); // if not row - result = false
      }
      else if(!is_null($trans_id))
      {
        $select->where('agency_id_r =  ?', $trans_id);
      }
      
    return $this->getAdapter()->fetchAll($select);
  }
  
  public function getCustLogin($email, $password, $agensyID)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
            
    $select->where('email_cus =  ?', $email)
            ->where('password =  ?', md5($password))
            ->where('agency_id_r =  ?', $agensyID);
            
    //$this->getAdapter()->setFetchMode(5);       
    //echo (string)$select;exit;
    
    $res = $this->getAdapter()->fetchRow($select, array(), 5); // if not row - result = false
    
    //var_dump($res);exit;
    
    return $res;
  }
  
  
  public function insertCust($data)
  {
    return $this->insert($data);
  }
  
  public function deleteCust($id)
  {
    $where = $this->getAdapter()->quoteInto('domain_id = ?', $id);
    $this->delete($where);
  }
  
  public function getFileExel()
  {
    $email = models_Model::getTransEmail();
    
    $objTrans = new models_Translator();
    
    $trans = $objTrans->getTrEmail($email);
    
    $storeFolder = realpath(APPLICATION_PATH . "/../public/uploads/").'/'.$trans['id'].'/';
    
    if (!is_dir($storeFolder)){ mkdir($storeFolder,0777); }
    
    $tempFile = $_FILES['fileexcel']['tmp_name'];
    
    $file_ext = trim(strtolower(strrchr($_FILES['fileexcel']['name'], '.')),'.');
    
    $mtime = explode(" ", microtime());

    $tempName = $mtime[1] + $mtime[0];
    
    $nameFile = $tempName . '.'.$file_ext;
    
    $pathFile = $storeFolder . $nameFile;
    
    $res = '';
    
    //print_r($pathFile);exit;
    
    if (move_uploaded_file($tempFile, $pathFile))
    {
      $custArr = models_Model::parseExel($pathFile);
      
      return $this->addCust($custArr,$trans['id'],$pathFile);
    }
    else
    {
      return false;
    }
    
  }
  
  public function addCust($custArr, $transID, $pathFile)
  {
    $issetCust = $this->getCust(null,0,$transID);
    
    $arrCustMail = array();
    
    foreach($issetCust as $custRow)
    {
      $arrCustMail[] = $custRow['email_cus'];
    }
  
    $sql = 'INSERT INTO `customers` (`name_cus`,`email_cus`,`email_cus_sec`,`agency_id_r`) VALUES ';
    
    $i=1;
    
    $values = '';
    
    $notAdded = array();
    
    //print_r($custArr);exit;
    
    foreach($custArr as $cust)
    {
      if(!in_array($cust[1], $arrCustMail))
      {
        $values .= '("'.$cust[0].'","'.$cust[1].'","'.$cust[2].'","'.$transID.'"),';
        $i++;
        $arrCustMail[] = $cust[1];
      }
      else
      {
        $notAdded[] = $cust[1];
      }
      
      if($i==10)
      {
        $sqlQuery = $sql . trim($values,',');
        $this->getAdapter()->query($sqlQuery);
        
        $i = 1;
      }
    }
    
    if($i!==1)
    {
      $sqlQuery = $sql . trim($values,',');
      $this->getAdapter()->query($sqlQuery);
    }
    
    unlink ($pathFile);
    
    return $notAdded;
    
  }
  
}