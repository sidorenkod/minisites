<?php

class models_Country extends Zend_Db_Table_Abstract
{
  protected $_name = 'country';
  
  public function getCountry($id=0)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
      
     if($id != 0)
      {
        $select->where('id =  ?', $id);
        return $this->getAdapter()->fetchRow($select); // if not row - result = false
      }
      
    return $this->getAdapter()->fetchAll($select);
  }
  
  
  public function insertCount($data)
  {
    return $this->insert($data);
  }
  
  public function updateCount($id, $data)
  {
    $where[] = "id = $id";
    $this->update($data, $where);
  }
}