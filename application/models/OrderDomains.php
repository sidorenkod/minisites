<?php

class models_OrderDomains extends Zend_Db_Table_Abstract
{
  protected $_name = 'order_domains';
  
  public function getOrdDomain($email)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

      $select->where('email_r =  ?', $email);

    return $this->getAdapter()->fetchAll($select); // if not row - result = false
  }
  
  public function getEmailOrdDomain($domain)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

      $select->where('domain =  ?', $domain);

    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function insertOrDom($data)
  {
    return $this->insert($data);
  }
}