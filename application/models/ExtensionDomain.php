<?php

class models_ExtensionDomain extends Zend_Db_Table_Abstract
{
  protected $_name = 'extension_domain';
  
  public function getAgencys()
  {
    $email = Zend_Auth::getInstance()->getIdentity()->email;
    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('email_r =  ?', $email); 
      
        
      
    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function insertExtDom($data)
  {
    $this->insert($data);
  }
  
  public function updateExtDom($id, $data)
  {
    if($id != 0)
    {
      $where = $this->getAdapter()->quoteInto('id = ?', $id);
      $this->update($data, $where);
    }
  }
}