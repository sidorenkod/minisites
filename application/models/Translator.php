<?php

class models_Translator extends Zend_Db_Table_Abstract
{
  protected $_name = 'translators';
  
  public function iniciarSesionTranslators($u, $p)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('email =  ?', $u);
    $select->where('pass_tran =  ?', $p);    

    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function transIdPass($id, $pass)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('id =  ?', $id);
    $select->where('pass_tran =  ?', $pass);    

    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function getTrEmail($email=null,$id=null)
  {    
    $select = $this->getAdapter()->select()->from($this->_name);
    
    if(!is_null($email))
    {
      $select->where('email =  ?', $email);
    }
    else if(!is_null($id))
    {
      $select->where('id = ?', $id);
    }
    

    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function insertTrans($data)
  {
    return $this->insert($data);
  }
  
  public function updateTrans($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('id = ?', $id);
    $this->update($data, $where);
  }
  
}