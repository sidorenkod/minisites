<?php

class models_Agencys extends Zend_Db_Table_Abstract
{
  protected $_name = 'agencys';
  
  public function getAgencys($email=null)
  {
    if(!is_null($email))
    {
      $email = $email;
    }
    else if(isset(Zend_Auth::getInstance()->getIdentity()->email))
    {
      $email = Zend_Auth::getInstance()->getIdentity()->email;
    }
    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    $select->where('email_r =  ?', $email); 
      

    //echo (string)$select;exit;
    return $this->getAdapter()->fetchRow($select); // if not row - result = false
  }
  
  public function insertAgen($data)
  {
    $this->insert($data);
  }
  
  public function updateAgen($email, $data)
  {
    $where = $this->getAdapter()->quoteInto('email_r = ?', $email);
    $this->update($data, $where);
  }
}