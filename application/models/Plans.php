<?php

class models_Plans extends Zend_Db_Table_Abstract
{
  protected $_name = 'plans';
  
  public function getPlans($id=0)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);

    if($id!=0)
    {
      $select->where('plan_id = ?', $id);
      
      return $this->getAdapter()->fetchRow($select); // if not row - result = false
    }
    
    return $this->getAdapter()->fetchAll($select); // if not row - result = false
  }
  
  public function insertPlans($data)
  {
    return $this->insert($data);
  }
  
  public function updatePlans($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('plan_id = ?', $id);
    $this->update($data, $where);
  }
  
  public function deletePlans($id)
  {
    $where = $this->getAdapter()->quoteInto('plan_id = ?', $id);
    $this->delete($where);
  }
}