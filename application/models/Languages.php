<?php

class models_Languages extends Zend_Db_Table_Abstract
{
  protected $_name = 'languages';
  
  public function getLang($id=0)
  {    
    if($id != 0)
    {
      $select = $this->getAdapter()->select()
            ->from($this->_name);
            
      $select->where('lenguage_id =  ?', $id);
      
      return $this->getAdapter()->fetchRow($select); // if not row - result = false
    }
    $sql = 'SELECT * FROM languages L, group_languages G WHERE G.group_id=L.group_id ORDER BY L.name_leng;';
    return $this->getAdapter()->fetchAll($sql); // if not row - result = false
  }
  
  public function getLang2()
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
    return $this->getAdapter()->fetchAll($select); // if not row - result = false
  }
  
  public function getLangMacros($id=0)
  {
    $select = $this->getAdapter()->select()
            ->from(array('l'=>$this->_name));
    $select->joinLeft(array('ot'=>'templates_other'),'ot.lang_id=l.lenguage_id',array('data'));
    
    if($id != 0)
    {
      $select->where('l.lenguage_id = ?', $id);
      return $this->getAdapter()->fetchRow($select);
    }
    
    return $this->getAdapter()->fetchAll($select);
  }
  
  public function insertLang($data)
  {
    return $this->insert($data);
  }
  
  public function updateLang($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('lenguage_id = ?', $id);
    $this->update($data, $where);
  }
  
  public function deleteLang($id)
  {
    $where = $this->getAdapter()->quoteInto('lenguage_id = ?', $id);
    $this->delete($where);
  }
}