<?php

class models_Expertises extends Zend_Db_Table_Abstract
{
  protected $_name = 'expertises';
  
  public function getExpert($id=0,$lang_id=0)
  {    
    $select = $this->getAdapter()->select()
            ->from($this->_name);
            
    if($id != 0)
    {
      $select->where('expertise_id = ?',$id);
      
      return $this->getAdapter()->fetchRow($select); // if not row - result = false
    }
    else if($lang_id !==0)
    {
      $select->where('lang_id = ?',$lang_id);
      $select->order(array('exp_id ASC'));
    }

    return $this->getAdapter()->fetchAll($select);
  }
  
  public function insertExp($data)
  {
    return $this->insert($data);
  }
  
  public function updateExp($id, $data)
  {
    $where = $this->getAdapter()->quoteInto('expertise_id = ?', $id);
    $this->update($data, $where);
  }
  
  public function deleteExp($id)
  {
    $where = $this->getAdapter()->quoteInto('expertise_id = ?', $id);
    $this->delete($where);
  }
}