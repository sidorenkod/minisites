<?php
class Zend_View_Helper_GetFavicontempl extends Zend_View_Helper_Abstract
{
  public function getFavicontempl()
  {
    $email = models_Model::getTransEmail();
    
    $trans = new models_Translator();
    
    $data = $trans->getTrEmail($email);
    
    
    //print_r($data);exit;
    
    
    $file = $_SERVER['DOCUMENT_ROOT'].'/templates/templates_users/'.$data['id'].'/favicon.ico';
    
    if(file_exists($file))
    {
      return '/templates/templates_users/'.$data['id'].'/favicon.ico';
    }
    
    return null;
  }
}