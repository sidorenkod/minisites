<?php
class Zend_View_Helper_CheckData extends Zend_View_Helper_Abstract
{
  public function checkData($data, $key, $default = null)
  {
    
    //print_r($data);exit;
    if(isset($data[$key]) && $data[$key] != '')
    {
      return $data[$key];
    }
    
    return $default;
  }
}