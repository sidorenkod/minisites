<?php
class Zend_View_Helper_EmailDomains extends Zend_View_Helper_Abstract
{
  public function emailDomains($id)
  {
    $objEmOrdDomains = new models_EmailsOrderDomain();
    
    return $objEmOrdDomains->getEmOrdDomain($id);
  }
}