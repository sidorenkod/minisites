<?php
class Zend_View_Helper_CheckLangSite extends Zend_View_Helper_Abstract
{
  public function checkLangSite($langId)
  {
    //print_r($langId);
    $langRight = array(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,74); // id languages
    
    if(in_array($langId, $langRight))
    {
      return true;
    }    
    
    return false;
  }
}