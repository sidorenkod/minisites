<?php
class Zend_View_Helper_GetLogo extends Zend_View_Helper_Abstract
{
  public function getLogo()
  {
    $email = Zend_Auth::getInstance()->getIdentity()->email;
    $id = Zend_Auth::getInstance()->getIdentity()->id;
    
    $trans = new models_Translator();
    
    $data = $trans->getTrEmail($email);
    
    if(!is_null($data['logo']) && $data['logo'] != '')
    {
      return 'style="background-image:url(/templates/templates_users/'.$id.'/'.$data['logo'].')"';
    }
    
    return null;
  }
}