<?php
class Zend_View_Helper_Templates extends Zend_View_Helper_Abstract
{
  public function templates($data, $key, $default=null)
  {
    $objTempl = new models_Templates();
    if($data == 'getMeta')
    {
      return $objTempl->getMeta($key, $default);
    }
    else
    {
      return $objTempl->getData($key, $default);
    }
  }
}