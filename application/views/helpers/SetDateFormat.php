<?php
class Zend_View_Helper_SetDateFormat extends Zend_View_Helper_Abstract
{
  public function setDateFormat($date, $format='d, m, Y')
  {
    if ($date == '0000-00-00 00:00:00')
      return 'N/A';
    else{
      $newDate = new DateTime($date);
      return $newDate->format($format);
    }
  }
}