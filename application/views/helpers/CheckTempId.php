<?php
class Zend_View_Helper_CheckTempId extends Zend_View_Helper_Abstract
{
  public function checkTempId()
  {
    $objTempl = new models_Templates();
    
    return $objTempl->checkTempId();
  }
}