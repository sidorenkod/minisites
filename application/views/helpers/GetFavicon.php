<?php
class Zend_View_Helper_GetFavicon extends Zend_View_Helper_Abstract
{
  public function getFavicon()
  {
    $id = Zend_Auth::getInstance()->getIdentity()->id;
    
    $file = $_SERVER['DOCUMENT_ROOT'].'/templates/templates_users/'.$id.'/favicon.ico';
    
    if(file_exists($file))
    {
      return 'style="background-image:url(/templates/templates_users/'.$id.'/favicon.ico)"';
    }
    
    return null;
  }
}