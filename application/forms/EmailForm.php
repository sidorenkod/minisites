<?php
/**
 * Created by PhpStorm.
 * User: inxo
 * Date: 07/03/16
 * Time: 11:23
 */
class Application_Form_EmailForm extends Zend_Form{
    public function __construct()
    {
        // this is where i normally set up my decorators for the form and elements
        // additionally you can register prefix paths for custom validators, decorators, and elements

        parent::__construct();
        // parent::__construct must be called last because it calls $form->init()
        // and anything after it is not executed
    }

    public function removeDecorators()
    {
        foreach ($this->getElements() as $element) {
//            if($element->getDecorator('Label'))
//                $element->getDecorator('Label')->setTag(null);
            $element->removeDecorator('HtmlTag');
            $element->removeDecorator('Label');
            $element->removeDecorator('DtDdWrapper');
        }

        return $this;
    }

    public function highlightErrorElements()
    {
        foreach($this->getElements() as $element) {
            if($element->hasErrors()) {
                $element->setAttrib('class', 'form-control error_form');
            }
        }
    }
}